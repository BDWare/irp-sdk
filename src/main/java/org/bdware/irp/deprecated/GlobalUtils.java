package org.bdware.irp.deprecated;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.ECDSASigner;
import com.nimbusds.jose.crypto.ECDSAVerifier;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.*;
import org.bdware.irp.irplib.exception.IrpMessageCredentialException;
import org.bdware.irp.irplib.util.IrpCommon;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;

public class GlobalUtils {

    //encode string to bytes by specified coding type
    public static final byte[] encodeString(String str) {
        try {
            return str.getBytes(IrpCommon.MESSAGE_ENCODING_TYPE);
        } catch (Exception e) {
            System.out.println(str + " encode to bytes failed!");
        }
        return null;
    }

    //encode bytes to String by specified coding type
    public static final String decodeString(byte[] bytes) {
        if (bytes == null || bytes.length == 0)
            return "";
        try {
            return new String(bytes, IrpCommon.MESSAGE_ENCODING_TYPE);
        } catch (Exception e) {
            System.out.println(new String(bytes) + " decode to string failed!");
        }
        return null;
    }


    public static final byte[] getDigestAlgFromSignature(String sigAlgorithm) throws IrpMessageCredentialException {
        if (sigAlgorithm.startsWith("SHA1"))
            return IrpCommon.CREDENTIAL_DIGEST_ALG_SHA1;
        else if (sigAlgorithm.startsWith("MD5"))
            return IrpCommon.CREDENTIAL_DIGEST_ALG_MD5;
        else
            throw new IrpMessageCredentialException("Unsupported digest algorithm for signature: " + sigAlgorithm);
    }

    public static final String signByteArrayByJWK(byte[] data, JWK jwk) throws JOSEException {
        JWSSigner jwsSigner;
        JWSObject jwsObject;

        if (jwk.getKeyType() == KeyType.RSA) {
            jwsSigner = new RSASSASigner(jwk.toRSAKey());
            jwsObject = new JWSObject(
                    new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(jwk.getKeyID()).build(),
                    new Payload(data));
        } else if (jwk.getKeyType() == KeyType.EC) {
            jwsSigner = new ECDSASigner(jwk.toECKey());
            jwsObject = new JWSObject(
                    new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(jwk.getKeyID()).build(),
                    new Payload(data));
        } else {
            System.out.println("unsupported jwk Algorithm");
            return null;
        }
        //Compute the EC signature
        jwsObject.sign(jwsSigner);

        return jwsObject.serialize(true);
    }

    public static final boolean verifySigByJWK(byte[] messageBody, String signature, String pkInfo) {
        if (messageBody == null || signature == null || pkInfo == null) {
            System.out.println("SignatureInfo missing!");
            return false;
        }
        try {
            JWSObject verify = JWSObject.parse(signature, new Payload(messageBody));
            //parse public key string
            JWK pkToVerify = JWK.parse(pkInfo);
            JWSVerifier verifier;
            if (pkToVerify.getKeyType() == KeyType.RSA) {
                verifier = new RSASSAVerifier(pkToVerify.toRSAKey().toRSAPublicKey());
            } else if (pkToVerify.getKeyType() == KeyType.EC) {
                verifier = new ECDSAVerifier(pkToVerify.toECKey().toECPublicKey());
            } else {
                System.out.println("unsupported Algorithm");
                return false;
            }
            if (verify.verify(verifier)) {
                return true;
            } else {
                System.out.println("verify the message failed!");
                return false;
            }
        } catch (ParseException e) {
            System.out.println("parse the pk error");
            return false;
        } catch (JOSEException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String createPublicKeyString(PublicKey publicKey) {
        if (publicKey == null) return null;

        String pubKeyStr = null;
        if (publicKey instanceof RSAPublicKey) {
            RSAKey rsaKey = new RSAKey.Builder((RSAPublicKey) publicKey).build();
            pubKeyStr = rsaKey.toJSONString();
        } else if (publicKey instanceof ECPublicKey) {
            ECKey ecKey = new ECKey.Builder(Curve.forECParameterSpec(((ECPublicKey) publicKey).getParams()), (ECPublicKey) publicKey).build();
            pubKeyStr = ecKey.toJSONString();
        } else {
            // currently only RSA algo is supported
            System.out.println("algo not support");
        }
        return pubKeyStr;
    }
    public static JWK loadKeysFromJWKFile(String jwkFilePath) throws IOException, ParseException {
        FileInputStream fi = new FileInputStream(jwkFilePath);
        byte[] jwkB = new byte[fi.available()];
        fi.read(jwkB);
        String jwkS = new String(jwkB);
        return JWK.parse(jwkS);
    }

    public static JWK loadKeysFromJWKStr(String jwkStr) throws ParseException {
        return JWK.parse(jwkStr);
    }
}
