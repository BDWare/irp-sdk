package org.bdware.irp.deprecated;

import com.nimbusds.jose.jwk.JWK;
import org.bdware.irp.irplib.core.IrpMessage;
import org.bdware.irp.irplib.core.IrpMessageSigner;
import org.bdware.irp.irplib.util.EncoderUtils;
import org.bdware.irp.irplib.util.IrpCommon;

public class JWKSigner implements IrpMessageSigner {
    JWK jwk;


    public JWKSigner(JWK jwk) {
        this.jwk = jwk;
    }

    //sign the massage by Signature, need init first
    public final boolean verifyMessage(IrpMessage irpMessage) {
        try {
            //generate the message signature data first
            byte[] digestData = irpMessage.getEncodedMessageHeaderBody();
            String signature = EncoderUtils.decodeString(irpMessage.credential.signature);
            String pubkey = EncoderUtils.decodeString(irpMessage.credential.signerDoid);
            return GlobalUtils.verifySigByJWK(digestData, signature, pubkey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //sign the massage by JWK
    public static final void signMessage(IrpMessage irpMessage, JWK jwk) {
        try {
            if (jwk == null) return;
            irpMessage.header.setCertifiedFlag(true);
            //generate the message signature data first
            byte[] digestData = irpMessage.getEncodedMessageHeaderBody();
            //not digest the body and header, sign immediately
            String signature = GlobalUtils.signByteArrayByJWK(digestData, jwk);
            irpMessage.credential.signerDoid = EncoderUtils.encodeString(jwk.getKeyID());
            irpMessage.credential.signedInfoType = IrpCommon.CREDENTIAL_SIGNEDINFO_TYPE_JWK;
            irpMessage.credential.signedInfoDigestAlgorithm = IrpCommon.CREDENTIAL_DIGEST_ALG_JWK;
            irpMessage.credential.signature = EncoderUtils.encodeString(signature);
            irpMessage.credential.signedInfoLength = irpMessage.credential.signedInfoDigestAlgorithm.length + irpMessage.credential.signature.length;
            irpMessage.encodedMessage = null;  //update the encode message
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void signMessage(IrpMessage irpMessage) {
        signMessage(irpMessage, jwk);
    }

}