package org.bdware.irp.stateinfo;


public class DoipServiceStateInfo extends StateInfoBase {

    public DoipServiceStateInfo(
            String serviceName,
            String serviceDescription,
            String owner,
            String listeners,
            String publicKey,
            String type
    ) {
        super();
        handleValues.addProperty("serviceName", serviceName);
        handleValues.addProperty("serviceDescription", serviceDescription);
        handleValues.addProperty("owner", owner);
        handleValues.addProperty("listeners", listeners);
        handleValues.addProperty("publicKey", publicKey);
        handleValues.addProperty("type", type);
    }


    public DoipServiceStateInfo(
    ) {
        super();
    }

    public DoipServiceStateInfo(StateInfoBase hrBase) {
        this.identifier = hrBase.identifier;
        this.handleValues = hrBase.handleValues;
    }

    public static DoipServiceStateInfo fromStateInfoBase(StateInfoBase hrBase) {
        if(hrBase == null)
            return null;
        return new DoipServiceStateInfo(hrBase);
    }

    public String getListeners() {
        return getValues("listeners");
    }

    public String getServiceDescription() {
        return getValues("serviceDescription");
    }

    public String getPublicKey() {
        return getValues("publicKey");
    }

    public String getOwner() {
        return getValues("owner");
    }

    public String getType() {
        return getValues("type");
    }

    public String getServiceName() {
        return getValues("serviceName");
    }

    public void setListeners(String listeners) {
        handleValues.addProperty("listeners", listeners);
    }

    public void setOwner(String owner) {
        handleValues.addProperty("owner", owner);
    }

    public void setType(String type) {
        handleValues.addProperty("type", type);
    }


    public void setServiceName(String serviceName) {
        handleValues.addProperty("serviceName", serviceName);
    }

    public void setServiceDescription(String serviceDescription) {
        handleValues.addProperty("serviceDescription", serviceDescription);
    }

    public void setPublicKey(String publicKey) {
        handleValues.addProperty("publicKey", publicKey);
    }

}
