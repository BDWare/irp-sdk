package org.bdware.irp.stateinfo;

public class DoStateInfo extends StateInfoBase {

    public DoStateInfo(String owner, String repository){
        super();
        setOwner(owner);
        setRepository(repository);
    }


    public DoStateInfo(String owner, String repository, String registry){
        super();
        setOwner(owner);
        setRepository(repository);
        setRegistry(registry);
    }




    public DoStateInfo(StateInfoBase hrBase){
        if(hrBase == null)
            return;
        identifier = hrBase.identifier;
        handleValues = hrBase.handleValues;
    }

    public static DoStateInfo fromStateInfoBase(StateInfoBase base){
        if(base == null)
            return null;
        return new DoStateInfo(base);
    }

    public String getOwner(){
        return getValues("owner");
    }
    public String getRepository(){
        return getValues("repository");
    }

    public String getRegistry(){
        return getValues("registry");
    }


    public void setOwner(String owner){
        handleValues.addProperty("owner",owner);
    }

    public void setRepository(String repository){
        handleValues.addProperty("repository",repository);
    }

    public void setRegistry(String registry){
        handleValues.addProperty("registry",registry);
    }

}
