package org.bdware.irp.stateinfo;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class StateInfoBase {


    public final static byte PUBLIC_WRITE = 0x01;
    public final static byte PUBLIC_READ = 0x02;
    public final static byte ADMIN_WRITE = 0x04;
    public final static byte ADMIN_READ = 0x08;


    public final static String ADMIN_WRITE_DOI = "ADMIN_WRITE";
    public final static String ADMIN_READ_DOI = "ADMIN_READ";
    public final static String ELEMENT_WRITE_DOI = "ELEMENT_WRITE";
    public final static String ELEMENT_READ_DOI = "ELEMENT_READ";

    public String identifier;
    public JsonObject handleValues;
    static final String UUID_KEY = "UUID";
    static final String TIMESTAMP = "__timestamp__";
    //@liuyi TODO need to verify if the ttl is second or millisecond
    public static final String TTL = "__ttl__";
    public static final String SESSION = "__session_id__";

    public byte permission;

    public static String ADMIN = "HS_ADMIN";

    public StateInfoBase() {
        handleValues = new JsonObject();
        permission = PUBLIC_READ & ADMIN_WRITE & ADMIN_READ;
    }


    public byte getPermission() {
        return permission;
    }

    public void setPermission(byte permission) {
        this.permission = permission;
    }

    public void setSessionId(String sessionId) {
        this.handleValues.addProperty(SESSION, sessionId);
    }

    public String getSessionId(){
        return handleValues.has(SESSION) ? handleValues.get(SESSION).getAsString() : null;
    }


    public StateInfoBase(JsonObject handleValues) {
        if (handleValues == null)
            this.handleValues = new JsonObject();
        else
            this.handleValues = handleValues;
    }

    public StateInfoBase(String identifier, JsonObject handleValues) {
        this.identifier = identifier;
        if (handleValues == null)
            this.handleValues = new JsonObject();
        else
            this.handleValues = handleValues;
    }


    public UUID getUUID() {
        return UUID.fromString(getValues(UUID_KEY));
    }

    public void setUUID(UUID uuid) {
        this.handleValues.addProperty(UUID_KEY, uuid.toString());
    }

    public static StateInfoBase fromJson(String json) {
        return new Gson().fromJson(json, StateInfoBase.class);
    }

    public String getIdentifier() {
        return identifier;
    }

    public JsonObject getHandleValues() {
        return handleValues;
    }

    public void setHandleValues(JsonObject handleValues) {
        this.handleValues = handleValues;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getValues(String key) {
        return handleValues.has(key) ? handleValues.get(key).getAsString() : null;
    }


    public JsonObject getAdmins(){
        if(!handleValues.has(ADMIN)){
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(ADMIN_WRITE_DOI, new JsonArray());
            jsonObject.add(ADMIN_READ_DOI, new JsonArray());
            jsonObject.add(ELEMENT_WRITE_DOI, new JsonArray());
            jsonObject.add(ELEMENT_READ_DOI, new JsonArray());
            handleValues.add(ADMIN, jsonObject);
        }
        return handleValues.get(ADMIN).getAsJsonObject();
    }

    public boolean canReadElement(String doi){
        if((permission & PUBLIC_READ) > 0)
            return true;
        if((permission & ADMIN_READ) == 0)
            return false;
        JsonObject admins = getAdmins();
        JsonArray readElements = admins.get(ELEMENT_READ_DOI).getAsJsonArray();
        if(readElements.isEmpty())
            return false;
        for(JsonElement jsonElement : readElements){
            if(doi.equals(jsonElement.getAsString()))
                return true;
        }
        return false;
    }

    public boolean canModifyElement(String doi){
        if((permission & PUBLIC_WRITE) > 0)
            return true;
        if((permission & ADMIN_WRITE) == 0)
            return false;
        JsonObject admins = getAdmins();
        JsonArray modifyElements = admins.get(ELEMENT_WRITE_DOI).getAsJsonArray();
        if(modifyElements.isEmpty())
            return false;
        for(JsonElement jsonElement : modifyElements){
            if(doi.equals(jsonElement.getAsString()))
                return true;
        }
        return false;
    }

    public boolean canModifyAdmin(String doi){
        if((permission & PUBLIC_WRITE) > 0)
            return true;
        if((permission & ADMIN_WRITE) == 0)
            return false;
        JsonObject admins = getAdmins();
        JsonArray modifyAdmins = admins.get(ADMIN_WRITE_DOI).getAsJsonArray();
        if(modifyAdmins.isEmpty())
            return false;
        for(JsonElement jsonElement : modifyAdmins){
            if(doi.equals(jsonElement.getAsString()))
                return true;
        }
        return false;
    }

    public JsonArray updateElement(String doi, int type, JsonArray jsonArray){
        if(type == 0){
            for(JsonElement jsonElement :  jsonArray){
                if(doi.equals(jsonElement.getAsString())){
                    jsonArray.remove(jsonElement);
                }
            }
        }else{
            jsonArray.add(doi);
        }
        return jsonArray;
    }

    public void setAdmin(String doi, String adminType, int type){
        JsonObject admins = getAdmins();
        if(ADMIN_WRITE_DOI.equals(adminType)){
            updateElement(doi, type, admins.get(ADMIN_WRITE_DOI).getAsJsonArray());
        }else if(ADMIN_READ_DOI.equals(adminType)){
            updateElement(doi, type, admins.get(ADMIN_READ_DOI).getAsJsonArray());
        }else if(ELEMENT_READ_DOI.equals(adminType)){
            updateElement(doi, type, admins.get(ELEMENT_READ_DOI).getAsJsonArray());
        }else if(ELEMENT_WRITE_DOI.equals(adminType)){
            updateElement(doi, type, admins.get(ELEMENT_WRITE_DOI).getAsJsonArray());
        }
    }

    public void setTimestamp(long timestamp) {
        this.handleValues.addProperty(TIMESTAMP, timestamp);
    }
    public void setTTL(long ttl) {
        this.handleValues.addProperty(TTL, ttl);
    }

    public long getTimestamp() {
        if(this.handleValues.get(TIMESTAMP) == null){
            return -1;
        }
        return this.handleValues.get(TIMESTAMP).getAsLong();
    }

    public long getTTL() {
        if(this.handleValues.get(TTL) == null){
            return -1;
        }
        return this.handleValues.get(TTL).getAsLong();
    }

    public boolean cacheValidate(){
        long ttl = getTTL();
        if(ttl < 0)
            return false;
        long now = System.currentTimeMillis();
        return ttl > now ? true : false;

    }


    public Map<String, String> getHandleValuesAsMap() {
        Map<String, String>
                ret = new HashMap<>();
        if (handleValues != null) for (String key : handleValues.keySet())
            ret.put(key, handleValues.get(key).getAsString());
        return ret;
    }
}
