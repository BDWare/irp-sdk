package org.bdware.irp.stateinfo;

public class UserStateInfo extends StateInfoBase {

    public UserStateInfo(String pubkey, String desc){
        super();
        handleValues.addProperty("pubkey",pubkey);
        handleValues.addProperty("desc",desc);
    }

    public UserStateInfo(String pubkey, String desc, String displayName, String email, String oldPassword, String password, String phone, String userName){
        super();
        handleValues.addProperty("pubkey",pubkey);
        handleValues.addProperty("desc",desc);
        handleValues.addProperty("displayName",displayName);
        handleValues.addProperty("email",email);
        handleValues.addProperty("oldPassword",oldPassword);
        handleValues.addProperty("password",password);
        handleValues.addProperty("phone",phone);
        handleValues.addProperty("userName",userName);
    }

    public UserStateInfo(StateInfoBase hrBase){
        if(hrBase == null)
            return;
        identifier = hrBase.identifier;
        handleValues = hrBase.handleValues;
    }

    public static UserStateInfo fromStateInfoBase(StateInfoBase hrBase){
        return new UserStateInfo(hrBase);
    }

    public void setPubkey(String pubkey){
        handleValues.addProperty("pubkey",pubkey);
    }

    public void setDesc(String desc){
        handleValues.addProperty("desc",desc);
    }

    public String getPubkey(){
        return getValues("pubkey");
    }

    public String getDesc(){
        return getValues("desc");
    }
}
