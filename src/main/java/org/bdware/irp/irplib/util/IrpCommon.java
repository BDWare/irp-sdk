package org.bdware.irp.irplib.util;

/**
 * Description:
 * <p>
 * List all of the standard identifiers for the irp
 * </p>
 * Time:2021-07<br>
 *
 * @author: pku<br>
 * @version: $Revision: 1.0 $<br>
 * @since 1.0
 */
public class IrpCommon {
    //identify the version of the irp
    public static final byte MAJOR_VERSION = 1;
    public static final byte MINOR_VERSION = 0;

    public static final byte DEFAULT_MAJOR_VERSION = 1;
    public static final byte DEFAULT_MINOR_VERSION = 0;

    public static final String MESSAGE_ENCODING_TYPE = "UTF8";

    // identifies the algorithm used to generate the message digest(digest types)
    public static final byte DIGEST_TYPE_MD5 = (byte) 1;
    public static final byte DIGEST_TYPE_SHA1 = (byte) 2;

    //the length of message digest for different digest type
    public static final int DIGEST_SIZE_MD5 = 16;
    public static final int DIGEST_SIZE_SHA1 = 20;

    //the version of credential
    public static final byte CREDENTIAL_VERSION = 1;
    public static final byte DEFAULT_CREDENTIAL_VERSION = 1;

    //signed info identifies
    public static final byte[] CREDENTIAL_SIGNEDINFO_TYPE_RSA = EncoderUtils.encodeString("HS_SIGNED_RSA");
    public static final byte[] CREDENTIAL_SIGNEDINFO_TYPE_DSAPUBKEY = EncoderUtils.encodeString("HS_SIGNED_DSAPUBKEY");
    public static final byte[] CREDENTIAL_SIGNEDINFO_TYPE_MAC = EncoderUtils.encodeString("HS_MAC");
    public static final byte[] CREDENTIAL_SIGNEDINFO_TYPE_JWK = EncoderUtils.encodeString("HS_SIGNED_JWK");
    public static final byte[] CREDENTIAL_SIGNEDINFO_TYPE_SM2 = EncoderUtils.encodeString("HS_SIGNED_SM2");

    public static final byte[] CREDENTIAL_DIGEST_ALG_MD5 = EncoderUtils.encodeString("MD5");
    public static final byte[] CREDENTIAL_DIGEST_ALG_SHA1 = EncoderUtils.encodeString("SHA-1");
    public static final byte[] CREDENTIAL_DIGEST_ALG_JWK = EncoderUtils.encodeString("JWK");

    public static final byte[] DEFAULT_CREDENTIAL_SIGNEDINFO_TYPE = EncoderUtils.encodeString("HS_SIGNED_RSA");
    public static final byte[] DEFAULT_CREDENTIAL_SIGNEDINFO_DIGEST_ALG = EncoderUtils.encodeString("SHA-1");

    // static size of irp message envelope (in bytes)
    public static final int MESSAGE_ENVELOPE_SIZE = 20;

    // static size of irp message header (in bytes)
    public static final int MESSAGE_HEADER_SIZE = 24;

    public static final int MAX_STRING_ARRAY_SIZE = 1048576;
    public static final int MAX_ARRAY_LENGTH_SIZE = 128;

    //configs for irp server
    public static final String DEAULT_HOST_ADDRESS = "127.0.0.1";
    public static final int DEAULT_HOST_PORT = 2641;
    public static final int DEFAULT_WORKER_GROUP_THREAD_SIZE = -1;

    public static final String IRP_SERVER_LISTENER_TCP = "TCP";
    public static final String IRP_SERVER_LISTENER_UDP = "UDP";

    // maximum allowable size (in bytes) of a message
    public static final int MAX_MESSAGE_PACKET_LENGTH = 262144;

    // maximum size of udp packets.  packets in multi-packet
    // messages must be as large as possible equal to or below
    // this limit.
    public static final int MAX_UDP_PACKET_SIZE = 512;
}

