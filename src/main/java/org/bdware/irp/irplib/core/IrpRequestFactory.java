package org.bdware.irp.irplib.core;

import com.google.gson.JsonObject;
import org.bdware.irp.irplib.util.EncoderUtils;

import java.nio.charset.StandardCharsets;

public class IrpRequestFactory {
    public static final String DOID_DEFAULT = "doidForCreate";
    public IrpMessageSigner signer;

    public IrpRequestFactory(IrpMessageSigner signer) {
        this.signer = signer;
    }

    public IrpRequest newIrsResolveRequest(String doid, String[] keys) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_RESOLUTION_DOID, doid);
        if (keys != null) {
            req.requestedKeys = new byte[keys.length][];
            for (int i = 0; i < keys.length; i++) {
                req.requestedKeys[i] = EncoderUtils.encodeString(keys[i]);
            }
        }
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newIrsResolveRecursiveRequest(String doid, String[] keys) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_RESOLUTION_DOID_RECURSIVE, doid);
        if (keys != null) {
            req.requestedKeys = new byte[keys.length][];
            for (int i = 0; i < keys.length; i++) {
                req.requestedKeys[i] = EncoderUtils.encodeString(keys[i]);
            }
        }
        JsonObject values = new JsonObject();
        values.addProperty("recursiveFlag", "true");
        req.doidValues = IrpMessage.fromJson2Byte(values);
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newGrsResolveRequest(String doid, String[] keys) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_RESOLVE_GRS, doid);
        if (keys != null) {
            req.requestedKeys = new byte[keys.length][];
            for (int i = 0; i < keys.length; i++) {
                req.requestedKeys[i] = EncoderUtils.encodeString(keys[i]);
            }
        }
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newGrsCreateOrgRequest(String doi, JsonObject values) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_CREATE_ORG_GRS, doi);
        if (values != null) {
            req.doidValues = IrpMessage.fromJson2Byte(values);
        }
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newIrsSynOrgToParentRequest(String doi, JsonObject values) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_CREATE_ORG_GRS, doi);
        if (values != null) {
            req.doidValues = IrpMessage.fromJson2Byte(values);
        }
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newIrsResolveRequest(byte[] doid, byte[][] keys, byte[][] doidValues) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_RESOLUTION_DOID, doid);
        req.requestedKeys = keys;
        req.doidValues = doidValues;
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newIrsResolveRequest(byte[] doid, byte[][] keys) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_RESOLUTION_DOID, doid);
        req.requestedKeys = keys;
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newIrsCreateDoidRequest(JsonObject values) {
        return newIrsCreateDoidRequest(DOID_DEFAULT, values);
    }

    public IrpRequest newIrsCreateDoidRequest(String doid, JsonObject values) {
        if (doid == null) doid = DOID_DEFAULT;
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_CREATE_DOID, doid);
        if (values != null) {
            req.doidValues = IrpMessage.fromJson2Byte(values);
        }
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newIrsCreateDoidRequest(byte[] doid, byte[][] values) {
        if (doid == null) doid = DOID_DEFAULT.getBytes(StandardCharsets.UTF_8);
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_CREATE_DOID, doid);
        req.doidValues = values;
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newIrsUpdateDoidRequest(String doid, JsonObject values) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_UPDATE_DOID, doid);
        if (values != null) {
            req.doidValues = IrpMessage.fromJson2Byte(values);
        }
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newIrsUpdateDoidRequest(byte[] doid, byte[][] values) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_UPDATE_DOID, doid);
        req.doidValues = values;
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newIrsDeleteDoidRequest(String doid) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_DELETE_DOID, doid);
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newIrsDeleteDoidRequest(byte[] doid) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_DELETE_DOID, doid);
        return req;
    }

    public IrpRequest newIrsBatchCreateDoidRequest(JsonObject values, int count) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_BATCH_CREATE_DOID, DOID_DEFAULT);
        if (values != null) {
            req.doidValues = IrpMessage.fromJson2Byte(values);
        }
        req.createNumber = count;
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newIrsBatchCreateDoidRequest(int count, byte[][] values) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_BATCH_CREATE_DOID, DOID_DEFAULT);
        req.doidValues = values;
        req.createNumber = count;
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newVerityIrsServerRequest(String doid, String address) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_VERIFY_IRS, doid);
        req.address = EncoderUtils.encodeString(address);
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newVerityIrsServerRequest(byte[] doid, byte[] address) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_VERIFY_IRS, doid);
        req.address = address;
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newQueryRouterInfo() {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_QUERY_ROUTER, DOID_DEFAULT);
        JsonObject values = new JsonObject();
        values.addProperty("routerInfo", "true");
        req.doidValues = IrpMessage.fromJson2Byte(values);
        if (signer != null) signer.signMessage(req);
        return req;
    }

    public IrpRequest newQueryDOByOffsetRequest(int offset, int count, boolean queryByCreateTime) {
        IrpRequest req = new IrpRequest(IrpMessageCode.OC_QUERY_ROUTER, DOID_DEFAULT);
        JsonObject values = new JsonObject();
        values.addProperty("offset", offset + "");
        values.addProperty("count", count + "");
        if (queryByCreateTime) values.addProperty("queryByCreateTime", "true");
        req.doidValues = IrpMessage.fromJson2Byte(values);
        if (signer != null) signer.signMessage(req);
        return req;
    }

}
