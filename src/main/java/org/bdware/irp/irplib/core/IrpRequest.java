package org.bdware.irp.irplib.core;

import com.google.gson.JsonObject;
import org.bdware.irp.irplib.exception.IrpMessageDecodeException;
import org.bdware.irp.irplib.util.EncoderUtils;

public class IrpRequest extends IrpMessage {
    public byte[] doid;
    public byte[][] doidValues = null;

    public byte[][] requestedKeys = null;
    public byte[] address;
    public int createNumber = 0;

    //Creates an IrpRequest.
    public IrpRequest(int opCode, String doid) {
        super(opCode, IrpMessageCode.RC_RESERVED);
        //this.authInfo = authInfo;
        this.doid = EncoderUtils.encodeString(doid);
    }

    public IrpRequest(int opCode, byte[] doid) {
        super(opCode, IrpMessageCode.RC_RESERVED);
        //this.authInfo = authInfo;
        this.doid = doid;
    }


    public JsonObject getDoidValues() {
        JsonObject result = null;
        try {
            result = fromByte2Json(this.doidValues);
        } catch (IrpMessageDecodeException e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean isAuthNeeded() {
        if (this.header != null)
            return this.header.getCertifiedFlag();
        return false;
    }

    public String getDoid() {
        return EncoderUtils.decodeString(this.doid);
    }

    public String getAddress() {
        return EncoderUtils.decodeString(this.address);
    }
}
