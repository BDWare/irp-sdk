package org.bdware.irp.irplib.core;

public interface IrpMessageSigner {
    public boolean verifyMessage(IrpMessage irpMessage);

    public void signMessage(IrpMessage irpMessage);
}