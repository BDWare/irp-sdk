package org.bdware.irp.irplib.core;

import org.bdware.irp.irplib.util.EncoderUtils;

public class HandleRequest extends IrpMessage {
    public byte[] handle;
    public byte[][] handleTypes = null;
    public int[] handleIndexes = null;


    //Creates an HandleRequest.
    public HandleRequest(int opCode, String handle) {
        super(opCode, IrpMessageCode.RC_RESERVED);
        //this.authInfo = authInfo;
        this.handle = EncoderUtils.encodeString(handle);
    }

    public static HandleRequest newResolveHandleRequest(String handle, int[] indexs, String[] types) {
        HandleRequest req = new HandleRequest(IrpMessageCode.OC_RESOLUTION, handle);
        if (types != null) {
            req.handleTypes = new byte[types.length][];
            for (int i = 0; i < types.length; i++) {
                req.handleTypes[i] = EncoderUtils.encodeString(types[i]);
            }
        }
        req.handleIndexes = indexs;
        return req;
    }

    @Override
    public String getDoid() {
        return EncoderUtils.decodeString(handle);
    }
}
