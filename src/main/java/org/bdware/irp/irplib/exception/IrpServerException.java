package org.bdware.irp.irplib.exception;

public class IrpServerException extends Exception{

    public IrpServerException(){
        super();
    }
    public IrpServerException(String message){
        super(message);
    }

    @Override
    public String toString() {
        String msg = getMessage();
        if (msg == null) msg = "";
        return "IrpServerException: " + msg;
    }

}
