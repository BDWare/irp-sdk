package org.bdware.irp.irplib.exception;

public class IrpMessageEncodeException extends Exception{

    public IrpMessageEncodeException(){
        super();
    }
    public IrpMessageEncodeException(String message){
        super(message);
    }

    @Override
    public String toString() {
        String msg = getMessage();
        if (msg == null) msg = "";
        return "IrpMessageEncodeException: " + msg;
    }

}
