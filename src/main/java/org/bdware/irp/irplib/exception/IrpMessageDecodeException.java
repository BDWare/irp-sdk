package org.bdware.irp.irplib.exception;

public class IrpMessageDecodeException extends Exception{

    public IrpMessageDecodeException(){
        super();
    }
    public IrpMessageDecodeException(String message){
        super(message);
    }

    @Override
    public String toString() {
        String msg = getMessage();
        if (msg == null) msg = "";
        return "IrpMessageDecodeException: " + msg;
    }

}
