package org.bdware.irp.irplib.crypto;

import org.bdware.irp.irplib.exception.EnvelopeResendException;

import java.util.ArrayList;

public abstract class AbstractPacketReceiveBuffer {
    public int requestId;


    public AbstractPacketReceiveBuffer(int requestId){
        this.requestId = requestId;
    }


    public abstract boolean isCompleted();

    public abstract void addMessagePacket(IrpMessagePacket messagePacket) throws EnvelopeResendException;

    public abstract ArrayList<IrpMessagePacket> getSortedIrpMessagePackets();

}
