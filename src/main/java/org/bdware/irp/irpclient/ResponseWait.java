package org.bdware.irp.irpclient;

import org.bdware.irp.irplib.core.IrpMessage;

import java.util.HashMap;
import java.util.Map;

public class ResponseWait {
    Map<Integer, IrpMessageCallback> waitObj = new HashMap<>();

    public void wakeUpAndRemove(int requestID, IrpMessage result) {
        wakeup(requestID,result);
        waitObj.remove(requestID);
    }

    public boolean waitResponse(final int requestID, IrpMessageCallback cb) {
        if (!waitObj.containsKey(requestID)) {
            waitObj.put(requestID, cb);
            return true;
        }
        return false;
    }

    public void wakeup(int requestID, IrpMessage result) {
        IrpMessageCallback ob = waitObj.get(requestID);
        if (ob != null) ob.onResult(result);
    }
}
