package org.bdware.irp.irpclient;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.netty.incubator.codec.quic.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp.irplib.core.IrpMessage;
import org.bdware.irp.irplib.crypto.NettyTCPCodeC;
import org.bdware.irp.irplib.util.IrpCommon;

import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

public class NettyIrpQuicClientChannel  extends NettyIrpClientChannel {
    static Logger logger = LogManager.getLogger(NettyIrpQuicClientChannel.class);
    final Bootstrap b = new Bootstrap();
    static EventLoopGroup group = new NioEventLoopGroup();
    QuicChannelBootstrap qcb;

    public NettyIrpQuicClientChannel() {
        QuicSslContext context = QuicSslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).
                applicationProtocols("irp/3.0").build();
        ChannelHandler codec = new QuicClientCodecBuilder()
                .sslContext(context)
                .maxIdleTimeout(5000, TimeUnit.MILLISECONDS)
                .initialMaxData(10000000)
                // As we don't want to support remote initiated streams just setup the limit for local initiated
                // streams in this example.
                .initialMaxStreamDataBidirectionalLocal(1000000)
                .build();

        Channel channel = b.group(group)
                .channel(NioDatagramChannel.class)
                .handler(codec)
                .bind(0).channel();
        qcb = QuicChannel.newBootstrap(channel)
                .handler(new LengthFieldBasedFrameDecoder(
                        IrpCommon.MAX_MESSAGE_PACKET_LENGTH,
                        16,
                        4,
                        0,
                        0))
                .handler(new NettyTCPCodeC())
                .handler(handler);

    }

    @Override
    public void sendMessage(IrpMessage request, IrpMessageCallback callback) {

    }

    @Override
    public void close() {
        if (handler != null) handler.close();
        isConnected = false;
    }

    @Override
    public void connect(String targetUrl) throws URISyntaxException {
        URI uri = new URI(targetUrl);
        logger.debug("[URI Parse]scheme:" + uri.getScheme() + "  host: " + uri.getHost() + "  port: " + uri.getPort());
        try {
            channel = qcb
                    .remoteAddress(new InetSocketAddress(uri.getHost(), uri.getPort()))
                    .connect()
                    .get();
            handler.setChannel(channel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        isConnected = true;
    }

    @Override
    public boolean isConnected() {
        return false;
    }

}
