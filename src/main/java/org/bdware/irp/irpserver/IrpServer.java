package org.bdware.irp.irpserver;

public interface IrpServer {
    void start();
    void stop();
    void setIrpServerHandler(IrpHandler handler);
}
