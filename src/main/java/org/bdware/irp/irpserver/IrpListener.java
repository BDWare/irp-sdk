package org.bdware.irp.irpserver;

public interface IrpListener {
    void start();
    void stop();
    void setRequestHandler(IrpRequestHandler handler);
}