package org.bdware.irp.irpserver;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import io.netty.incubator.codec.quic.QuicServerCodecBuilder;
import io.netty.incubator.codec.quic.QuicSslContext;
import io.netty.incubator.codec.quic.QuicSslContextBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp.irplib.crypto.NettyTCPCodeC;
import org.bdware.irp.irplib.util.IrpCommon;

import java.util.concurrent.TimeUnit;

public class NettyIrpQuicListener implements IrpListener {

    static Logger logger = LogManager.getLogger(NettyIrpQuicListener.class);
    NettyServerHandler handler;
    private Channel ch;
    private final int port;
    IrpListenerInfo info;

    protected NettyIrpQuicListener(IrpListenerInfo listenerInfo) {
        info = listenerInfo;
        this.port = listenerInfo.port;
    }

    public static NettyIrpQuicListener CreateIrpListener(IrpListenerInfo listenerInfo) {
        return new NettyIrpQuicListener(listenerInfo);
    }

    @Override
    public void setRequestHandler(IrpRequestHandler handler) {
        this.handler = new NettyServerHandler(handler);
    }

    @Override
    public void start() {
        if (handler == null) {
            logger.error("Handler not init yet! set handler first");
            return;
        }
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup;
        if (info.workerGroupThreadSize <= 0)
            workerGroup = new NioEventLoopGroup();
        else workerGroup = new NioEventLoopGroup(info.workerGroupThreadSize);
        try {
            SelfSignedCertificate selfSignedCertificate = new SelfSignedCertificate();

            Bootstrap b = new Bootstrap();
            QuicSslContext context = QuicSslContextBuilder.forServer(
                    selfSignedCertificate.privateKey(), null, selfSignedCertificate.certificate())
                    .applicationProtocols("irp/3.0").build();
            b.group(workerGroup)
                    .channel(NioDatagramChannel.class)
                    .localAddress(port);
            ChannelHandler codec = new QuicServerCodecBuilder().sslContext(context)
                    .maxIdleTimeout(5000, TimeUnit.MILLISECONDS)
                    // Configure some limits for the maximal number of streams (and the data) that we want to handle.
                    .initialMaxData(10000000)
                    .initialMaxStreamDataBidirectionalLocal(1000000)
                    .initialMaxStreamDataBidirectionalRemote(1000000)
                    .initialMaxStreamsBidirectional(100)
                    .initialMaxStreamsUnidirectional(100)
                    .handler(new LengthFieldBasedFrameDecoder(
                                    IrpCommon.MAX_MESSAGE_PACKET_LENGTH,
                                    16,
                                    4,
                                    0,
                                    0))
                    .handler(new NettyTCPCodeC())
                    .handler(handler).build();
            b.handler(codec);
            ch = b.bind().syncUninterruptibly().channel();
            logger.info("UDP IRP listener start at:" + port);
            ch.closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    @Override
    public void stop() {
        if (ch != null)
            ch.close();
    }

}
