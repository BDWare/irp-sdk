package org.bdware.irp.irpserver;

import org.bdware.irp.irplib.core.IrpMessage;

//prs服务接口，支持irs节点验证、标识解析、机构信息管理
public interface PrsHandler {
    IrpMessage verifyIrs(IrpMessage request);
    IrpMessage resolvePrefix(IrpMessage request);
    IrpMessage createPrefix(IrpMessage request);
    IrpMessage updatePrefix(IrpMessage request);
    IrpMessage deletePrefix(IrpMessage request);
}
