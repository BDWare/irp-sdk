package org.bdware.irp.irpserver;

import org.bdware.irp.irplib.core.IrpMessage;

public interface RouterHandler {
    //before server start
    IrpMessage verifyChildRouter(IrpMessage request);
    IrpMessage queryIdentifierByOffset(IrpMessage request);
}
