package org.bdware.irp.irpserver;

import com.google.gson.Gson;

import java.util.List;

public class IrpServerInfo {
    public String serverId;
    public String serverDescription;
    public List<IrpListenerInfo> listenerInfos;


    public IrpServerInfo(String serverId, String serverDes, List<IrpListenerInfo> listenerInfos) {
        this.serverId = serverId;
        this.serverDescription = serverDes;
        //     this.admin = admin;
        this.listenerInfos = listenerInfos;
        //    this.ipAddress = ipAddress;
        //   this.port = port;
        //this.publicKey = GlobalCertifications.getGlobalJWK().toPublicJWK().toString();
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public byte[] toBytes() {
        return this.toJson().getBytes();
    }

}
