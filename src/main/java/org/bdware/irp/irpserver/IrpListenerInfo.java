package org.bdware.irp.irpserver;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import org.bdware.irp.irplib.crypto.NettyTCPCodeC;
import org.bdware.irp.irplib.util.IrpCommon;

public class IrpListenerInfo {
    public String host;
    public int port;
    public String protocolType;
    public int workerGroupThreadSize;
    
    private int queueSize = 1000;

    public IrpListenerInfo(String protocolType) {
        this(IrpCommon.DEAULT_HOST_PORT, protocolType);
    }

    public IrpListenerInfo(int port, String protocolType) {
        this(IrpCommon.DEAULT_HOST_ADDRESS, port, protocolType);
    }

    public IrpListenerInfo(String host, int port, String protocolType) {
        this(host, port, protocolType, IrpCommon.DEFAULT_WORKER_GROUP_THREAD_SIZE);
    }

    public IrpListenerInfo(String host, int port, String protocolType, int workerGroupThreadSize) {
        this.host = host;
        this.port = port;
        this.protocolType = protocolType;
        this.workerGroupThreadSize = workerGroupThreadSize;
    }
    
    public IrpListenerInfo(String host, int port, String protocolType, int workerGroupThreadSize, int queueSize) {
        this.host = host;
        this.port = port;
        this.protocolType = protocolType;
        this.workerGroupThreadSize = workerGroupThreadSize;
        this.queueSize = queueSize;
    }

    public ChannelInitializer<SocketChannel> createChannelInitializer(NettyServerHandler serverHandler) {
        return new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                ch.pipeline()
                        .addLast(new LengthFieldBasedFrameDecoder(IrpCommon.MAX_MESSAGE_PACKET_LENGTH, 16, 4, 0, 0))
                        .addLast(new NettyTCPCodeC())
                        .addLast(serverHandler);
            }
        };
    }

	public int getWorkerGroupThreadSize() {
		return workerGroupThreadSize;
	}

	public int getQueueSize() {
		return queueSize;
	}
    
}
