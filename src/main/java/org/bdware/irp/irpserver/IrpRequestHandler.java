package org.bdware.irp.irpserver;

import io.netty.channel.ChannelHandlerContext;
import org.bdware.irp.irplib.core.IrpMessage;

public interface IrpRequestHandler {
    IrpMessage onRequest(ChannelHandlerContext ctx, IrpMessage msg);
}
