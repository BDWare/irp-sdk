package org.bdware.irp.irpserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NettyIrpListener implements IrpListener {

    static Logger logger = LogManager.getLogger(NettyIrpListener.class);
    NettyServerHandler handler;
    private Channel ch;
    private final int port;
    IrpListenerInfo info;

    protected NettyIrpListener(IrpListenerInfo listenerInfo) {
        info = listenerInfo;
        this.port = listenerInfo.port;
    }

    public static IrpListener CreateIrpListener(IrpListenerInfo listenerInfo) {
        switch (listenerInfo.protocolType) {
            case "QUIC":
            case "quic":
                return new NettyIrpQuicListener(listenerInfo);
            default:
                return new NettyIrpListener(listenerInfo);
        }

    }

    @Override
    public void setRequestHandler(IrpRequestHandler handler) {
        this.handler = new NettyServerHandler(handler);
    }

    @Override
    public void start() {
        if (handler == null) {
            logger.error("Handler not init yet! set handler first");
            return;
        }


        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup;
        if (info.workerGroupThreadSize <= 0)
            workerGroup = new NioEventLoopGroup();
        else workerGroup = new NioEventLoopGroup(info.getWorkerGroupThreadSize());
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(port);
            b.option(ChannelOption.SO_BACKLOG, info.getQueueSize())
                    .childOption(ChannelOption.SO_KEEPALIVE, Boolean.TRUE);
            b.childHandler(info.createChannelInitializer(handler));
            ch = b.bind().syncUninterruptibly().channel();
            logger.info("TCP IRP listener start at:" + port);
            ch.closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    @Override
    public void stop() {
        if (ch != null)
            ch.close();
    }

}
