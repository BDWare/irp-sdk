package org.bdware.irp.irpserver;

import io.netty.channel.ChannelHandlerContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp.irplib.core.IrpMessage;
import org.bdware.irp.irplib.core.IrpMessageCode;
import org.bdware.irp.irplib.core.IrpResponse;

public class IrpRequestHandlerImpl implements IrpRequestHandler {
    static Logger logger = LogManager.getLogger(NettyIrpListener.class);
    IrpHandler irpHandler;

    public IrpRequestHandlerImpl() {
    }

    public void setIrpHandler(IrpHandler irsHandler) {
        this.irpHandler = irsHandler;
    }


    @Override
    public IrpMessage onRequest(ChannelHandlerContext ctx, IrpMessage msg) {
        int opCode = msg.header.opCode;
        IrpMessage res;
        logger.debug("[Call operation] name: " + opCode);
        try {
            switch (opCode) {
                case IrpMessageCode.OC_RESOLUTION_DOID:
                case IrpMessageCode.OC_RESOLUTION_DOID_RECURSIVE:
                    res = irpHandler.resolveDOID(msg);
                    break;
                case IrpMessageCode.OC_CREATE_DOID:
                    res = irpHandler.createDOID(msg);
                    break;
                case IrpMessageCode.OC_DELETE_DOID:
                    res = irpHandler.deleteDOID(msg);
                    break;
                case IrpMessageCode.OC_UPDATE_DOID:
                    res = irpHandler.updateDOID(msg);
                    break;
                case IrpMessageCode.OC_BATCH_CREATE_DOID:
                    res = irpHandler.batchCreateDOID(msg);
                    break;
                case IrpMessageCode.OC_VERIFY_IRS:
                    res = irpHandler.verifyIrs(msg);
                    break;
                case IrpMessageCode.OC_CREATE_ORG_GRS:
                    res = irpHandler.createPrefix(msg);
                    break;
                case IrpMessageCode.OC_UPDATE_ORG_GRS:
                    res = irpHandler.updatePrefix(msg);
                    break;
                case IrpMessageCode.OC_DELETE_ORG_GRS:
                    res = irpHandler.deletePrefix(msg);
                    break;
                case IrpMessageCode.OC_RESOLVE_GRS:
                    res = irpHandler.resolvePrefix(msg);
                    break;
                case IrpMessageCode.OC_VERIFY_ROUTER:
                    res = irpHandler.verifyChildRouter(msg);
                    break;
                case IrpMessageCode.OC_QUERY_ROUTER:
                    res = irpHandler.queryIdentifierByOffset(msg);
                    break;
                default:
                    logger.debug("Unknown opcode to handler");
                    res = IrpResponse.newErrorResponse(opCode, IrpMessageCode.RC_ERROR, "Unknown opcode to handler");
            }
        } catch (Exception e) {
            logger.error("Unknown opcode to handler",e);
            e.printStackTrace();
            res = IrpResponse.newErrorResponse(opCode, IrpMessageCode.RC_ERROR, "Unknown opcode to handler");
        }
        return res;
    }
}
