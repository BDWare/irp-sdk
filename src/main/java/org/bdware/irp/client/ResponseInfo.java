package org.bdware.irp.client;

public class ResponseInfo {
    public String prefix;
    public int port;
    public String routerInfo;

    public ResponseInfo(String prefix, int port, String routerInfo) {
        this.prefix = prefix;
        this.port = port;
        this.routerInfo = routerInfo;
    }
}
