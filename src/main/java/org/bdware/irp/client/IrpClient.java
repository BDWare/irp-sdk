package org.bdware.irp.client;

import org.bdware.irp.exception.IrpClientException;
import org.bdware.irp.irplib.core.IrpMessageSigner;
import org.bdware.irp.irplib.exception.IrpConnectException;
import org.bdware.irp.stateinfo.StateInfoBase;

import java.util.List;

public interface IrpClient {
    /**
     * @param handle 为string类型，表示需要解析的DOID
     * @return StateInfoBase类型，表示返回的解析结果
     * @exception IrpClientException 当解析出错时，抛出异常
     */
    StateInfoBase resolve(String handle) throws IrpClientException;

    String register(StateInfoBase hr) throws IrpClientException;

    String reRegister(StateInfoBase hr) throws IrpClientException;

    String unRegister(String handle) throws IrpClientException;

    List<String> batchRegister(StateInfoBase hr, int count) throws IrpClientException;

    void close();

    void connect(String url);

    void connect(String clientID, String LHSUrl, IrpMessageSigner signer);

    void reconnect() throws IrpConnectException;

    public boolean isConnected();

}
