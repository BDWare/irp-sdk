package org.bdware.irp.client;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nimbusds.jose.jwk.JWK;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp.deprecated.JWKSigner;
import org.bdware.irp.exception.IrpClientException;
import org.bdware.irp.irpclient.IrpClientChannel;
import org.bdware.irp.irpclient.IrpClientChannelGenerator;
import org.bdware.irp.irpclient.IrpMessageCallback;
import org.bdware.irp.irplib.core.*;
import org.bdware.irp.irplib.exception.IrpConnectException;
import org.bdware.irp.stateinfo.StateInfoBase;

import java.net.URISyntaxException;
import java.util.List;

public class IrpClientImpl implements IrpClient, IrsClient {

    static Logger LOGGER = LogManager.getLogger(IrpClientImpl.class);

    protected IrpClientChannel irpChannel;
    String serverURL = null;
    //PrintCallback pcb;
    String clientID;
    IrpRequestFactory factory;
    //String pre_doid = null;
    String address = null;

    public  IrpClientImpl(){

    }

    public IrpClientImpl(JWK jwk, String clientID, String LHSUrl) {
        this.clientID = clientID;
        this.serverURL = LHSUrl;
        factory = new IrpRequestFactory(new JWKSigner(jwk));
    }

    public IrpClientImpl(String clientID, String LHSUrl, IrpMessageSigner signer) {
        this.clientID = clientID;
        this.serverURL = LHSUrl;
        factory = new IrpRequestFactory(signer);
    }

    public String resolveHandle(String handle) throws IrpClientException {
        IrpMessage req = HandleRequest.newResolveHandleRequest(handle, null, null);
        ResponseCallback rcb = new ResponseCallback();
        sendMessage(req, rcb);
        synchronized (rcb) {
            try {
                rcb.wait(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        HandleResponse res = (HandleResponse) rcb.response;
        JsonArray values = JsonParser.parseString(res.getHandleValuesAsJson()).getAsJsonArray();
        JsonObject result = new JsonObject();
        result.addProperty("handle", res.getHandle());
        result.add("values", values);
        return result.toString();
    }

    public IrpResponse sendMessageSync(IrpMessage irpMessage) {
        return sendMessageSync(irpMessage, IRPConfig.IRPClientDefaultTimeout);
    }

    public IrpResponse sendMessageSync(IrpMessage irpMessage, long timeout) {
        ResponseCallback rcb = new ResponseCallback();
        sendMessage(irpMessage, rcb);
        synchronized (rcb) {
            try {
                rcb.wait(timeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (rcb.response == null)
            rcb.response = IrpResponse.newErrorResponse(
                    IrpMessageCode.OC_RESERVED,
                    IrpMessageCode.RC_ERROR,
                    "Server response timeout!");
        return rcb.response;
    }

    @Override
    public StateInfoBase resolve(String doid) throws IrpClientException {
        IrpMessage req = factory.newIrsResolveRequest(doid, null);
        IrpResponse res = sendMessageSync(req);
        LOGGER.info(res.toString());
        if (res.result == "success") {
            JsonObject resp = res.getDoidValues();
            StateInfoBase hr = new StateInfoBase();
            if (resp == null) {
                throw new IrpClientException("resolve failed!");
            }
            hr.identifier = res.getDoid();
            hr.handleValues = resp;
            return hr;
        } else {
            return null;
        }
    }

    public StateInfoBase resolveRecursive(String doid) throws IrpClientException {
        IrpMessage req = factory.newIrsResolveRecursiveRequest(doid, null);
        IrpResponse res = sendMessageSync(req);
        LOGGER.info(res.toString());
        if (res.result == "success") {
            JsonObject resp = res.getDoidValues();
            StateInfoBase hr = new StateInfoBase();
            if (resp == null) {
                throw new IrpClientException("resolve failed!");
            }
            hr.identifier = res.getDoid();
            hr.handleValues = resp;
            return hr;
        } else {
            return null;
        }
    }

    @Override
    public String register(StateInfoBase hr) throws IrpClientException {
        JsonObject map = hrToMap(hr);
        IrpMessage req;
        LOGGER.info("identifier: " + map.get("identifier") );
        if(hr.getIdentifier() != null){
            req = factory.newIrsCreateDoidRequest(hr.getIdentifier(), map);
        }else{
            map.remove("identifier");
            req = factory.newIrsCreateDoidRequest(map);
        }

        IrpResponse res = sendMessageSync(req);
        if (res.result != "success") {
            LOGGER.error("Register doid failed: " + res.getResponseMessage());
            throw new IrpClientException(res.getResponseMessage());
        }
        return res.getDoid();
    }

    @Override
    public String reRegister(StateInfoBase hr) throws IrpClientException {
        if (hr.identifier == null) {
            LOGGER.warn("handle need to be set.");
        }
        JsonObject map = hrToMap(hr);
        IrpMessage req = factory.newIrsUpdateDoidRequest(hr.identifier, map);
        IrpResponse res = sendMessageSync(req);
        if (res.result != "success") {
            LOGGER.error("Reregister doid failed: " + res.getResponseMessage());
            throw new IrpClientException(res.getResponseMessage());
        }
        return res.getDoid();
    }

    @Override
    public String unRegister(String handle) {
        if (handle == null) {
            LOGGER.warn("handle need to be set.");
        }
        IrpMessage req = factory.newIrsDeleteDoidRequest(handle);
        IrpResponse res = sendMessageSync(req);
        if (res.result != "success") {
            LOGGER.error("Delete doid failed: " + res.getResponseMessage());
        }
        return res.result;
    }

    @Override
    public List<String> batchRegister(StateInfoBase hr, int count) throws IrpClientException {
        JsonObject map = hrToMap(hr);
        map.remove("identifier");
        IrpMessage req = factory.newIrsBatchCreateDoidRequest(map, count);
        IrpResponse res = sendMessageSync(req);
        if (res.result != "success") {
            LOGGER.error("Register doid failed: " + res.getResponseMessage());
            throw new IrpClientException("register failed!");
        }
        return res.getDoidList();
    }

    @Override
    public boolean verifyIrsServer() {
        if (clientID == null) {
            LOGGER.warn("pre_doid need to be set in the jwk sk string to verify the irs server!");
            return false;
        }
        IrpMessage req = factory.newVerityIrsServerRequest(clientID, address);
        IrpResponse res = sendMessageSync(req);
        if (res.result != "success") {
            LOGGER.error("Verify the IRS Server from GRS failed: " + res.getResponseMessage());
            return false;
        }
        return true;
    }

    @Override
    public StateInfoBase resolvePrefix(String doid) throws IrpClientException {
        IrpMessage req = factory.newGrsResolveRequest(doid, null);
        IrpResponse res = sendMessageSync(req);
        LOGGER.info(res.toString());
        if (res.result == "success") {
            JsonObject resp = res.getDoidValues();
            StateInfoBase hr = new StateInfoBase();
            if (resp == null) {
                throw new IrpClientException("resolve failed!");
            }
            hr.identifier = res.getDoid();
            hr.handleValues = resp;
            return hr;
        } else {
            return null;
        }
    }

    @Override
    public boolean syncPrefixToGrs(StateInfoBase stateInfoBase) throws IrpClientException {
        LOGGER.info(new Gson().toJson(stateInfoBase));
        IrpMessage req = factory.newGrsCreateOrgRequest(stateInfoBase.getIdentifier(), hrToMap(stateInfoBase));
        IrpResponse res = sendMessageSync(req);
        LOGGER.info(res.toString());
        if (res.result == "success") {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean synPrefixToParent(StateInfoBase stateInfoBase) throws IrpClientException {
        LOGGER.info(new Gson().toJson(stateInfoBase));
        IrpMessage req = factory.newIrsSynOrgToParentRequest(stateInfoBase.getIdentifier(), hrToMap(stateInfoBase));
        IrpResponse res = sendMessageSync(req);
        LOGGER.info(res.toString());
        if (res.result == "success") {
            return true;
        } else {
            return false;
        }
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private JsonObject hrToMap(StateInfoBase handleRecord) {
        String className = handleRecord.getClass().getSimpleName();
        switch (className) {
            case "DoStateInfo":
                handleRecord.handleValues.addProperty("hrType", "do");
                break;
            case "DoipServiceStateInfo":
                handleRecord.handleValues.addProperty("hrType", "org/bdware/doip");
                break;
            case "UserStateInfo":
                handleRecord.handleValues.addProperty("hrType", "dou");
                break;
            default:
                LOGGER.error("use specific handle record class instead.");
                break;
        }
        return handleRecord.handleValues;
    }

    public void close() {
        irpChannel.close();
        irpChannel = null;
    }

    public void connect(String url) {
        try {
            irpChannel = IrpClientChannelGenerator.createIrpClientChannel(url);
            if (irpChannel == null) return;
            irpChannel.connect(url);
            serverURL = url;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connect(String clientID, String LHSUrl, IrpMessageSigner signer) {
        if(this.serverURL != LHSUrl){
            this.clientID = clientID;
            this.serverURL = LHSUrl;
            factory = new IrpRequestFactory(signer);
        }
        if(!isConnected())
            connect(LHSUrl);
    }

    public void reconnect() throws IrpConnectException {
        if (serverURL == null) throw (new IrpConnectException("target URL not set, use .connect(url) first"));
        if (irpChannel == null) irpChannel = IrpClientChannelGenerator.createIrpClientChannel(serverURL);
        if (irpChannel == null) return;
        try {
            irpChannel.connect(serverURL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return irpChannel != null && irpChannel.isConnected();
    }

    public void sendMessage(IrpMessage msg, IrpMessageCallback cb) {
        if (irpChannel == null || !irpChannel.isConnected()) {
            LOGGER.warn("channel not connect yet!");
            connect(serverURL);
            return;
        }
        irpChannel.sendMessage(msg, cb);
    }


    static class ResponseCallback implements IrpMessageCallback {
        IrpResponse response;

        public ResponseCallback() {
            response = null;
        }

        @Override
        public synchronized void onResult(IrpMessage msg) {
//            LOGGER.info("<callback>message header: " + msg.header.opCode);
            response = (IrpResponse) msg;
            synchronized (this) {
                this.notifyAll();
            }
        }
    }

    static class ResponseContainer {
        IrpResponse response;
    }
}
