package org.bdware.irp.client;

import org.bdware.irp.exception.IrpClientException;

public interface IrpRouterClient {
    //向上一级路由器进行验证
    String authInfoFromUpperRouter(String pubkey, String name, String signInfo) throws IrpClientException;
}
