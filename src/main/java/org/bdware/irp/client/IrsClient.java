package org.bdware.irp.client;

import org.bdware.irp.exception.IrpClientException;
import org.bdware.irp.stateinfo.StateInfoBase;

public interface IrsClient {
    //irs启动需要验证
    boolean verifyIrsServer();
    //获取前缀对应的机构信息
    StateInfoBase resolvePrefix(String handle) throws IrpClientException;
    //分配前缀，需要向grs上传信息
    boolean syncPrefixToGrs(StateInfoBase stateInfoBase) throws IrpClientException;
    //分配前缀，需要向grs上传信息
    boolean synPrefixToParent(StateInfoBase stateInfoBase) throws IrpClientException;
}
