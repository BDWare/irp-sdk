package org.bdware.irp.exception;

public class IrpClientException extends Exception{
    public IrpClientException(String s) {
        super(s);
    }
}
