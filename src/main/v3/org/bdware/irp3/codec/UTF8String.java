package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;

import java.nio.charset.StandardCharsets;

public class UTF8String {
    public static String fromByteBuf(ByteBuf byteBuf) {
        int length = byteBuf.readInt();
        if (length > 0) {
            byte[] data = new byte[length];
            byteBuf.readBytes(data);
            return new String(data, StandardCharsets.UTF_8);
        } else return "";
    }

    public static String[] listFromByteBuf(ByteBuf byteBuf) {
        int length = byteBuf.readInt();
        if (length < 0) length = 0;
        String[] data = new String[length];
        for (int i = 0; i < length; i++)
            data[i] = fromByteBuf(byteBuf);
        return data;
    }

    public static void toByteBuf(String[] strs, ByteBuf buf) {
        if (strs == null || strs.length == 0)
            buf.writeInt(0);
        else {
            buf.writeInt(strs.length);
            for (String str : strs)
                toByteBuf(str, buf);
        }
    }

    public static void toByteBuf(String str, ByteBuf buf) {
        if (str == null || str.length() == 0)
            buf.writeInt(0);
        else {
            buf.writeInt(str.length());
            buf.writeBytes(str.getBytes(StandardCharsets.UTF_8));
        }
    }

    public static int length(String digestAlgorithm) {
        if (digestAlgorithm == null)
            return 4;
        return 4 + digestAlgorithm.length();
    }
}
