package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;

public class RawBody extends MessageBody {

    public static RawBody fromByteBuf(ByteBuf byteBuf) {
        RawBody ret = new RawBody();
        ret.toRead = byteBuf.copy();
        return ret;
    }

    public void toByteBuf(ByteBuf byteBuf) {
        if (toRead != null)
            byteBuf.writeBytes(toRead);
        return;
    }
    public Opcode getOpcode() {
        return Opcode.OC_RESERVED;
    }
}
