package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.body.*;

import java.util.List;

public class RequestCodec extends MessageToMessageCodec<IrpMessage, MessageBody> {
    static Logger LOGGER = LogManager.getLogger(ResponseCodec.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, MessageBody msg, List<Object> out) throws Exception {
        //encode response and request uses the same method
        IrpMessage message = msg.wrapHeader();
        out.add(message);
        LOGGER.info("SEND:" + msg.toString());
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, IrpMessage msg, List<Object> out) throws Exception {
        try {
            // decode request
            MessageHeader header = msg.header;
            Opcode opcode = Opcode.fromInt(header.opCode);
            ByteBuf byteBuf = msg.body.toRead;
            if (opcode == null) out.add(GeneralMessage.fromByteBuf(byteBuf));
            switch (opcode) {
                case OC_RESOLUTION:
                    out.add(QueryRequest.fromByteBuf(byteBuf));
                    return;
                case OC_GET_SITE_INFO:
                    out.add(GetSiteInfoRequest.fromByteBuf(byteBuf));
                    return;
                case OC_CREATE_ID:
                    out.add(CreateIdentifierRequest.fromByteBuf(byteBuf));
                    return;
                case OC_DELETE_ID:
                    out.add(DeleteIdentifierRequest.fromByteBuf(byteBuf));
                    return;
                case OC_ADD_ELEMENT:
                    out.add(AddElementRequest.fromByteBuf(byteBuf));
                    return;
                case OC_REMOVE_ELEMENT:
                    out.add(RemoveElementRequest.fromByteBuf(byteBuf));
                    return;
                case OC_MODIFY_ELEMENT:
                    out.add(ModifyElementRequest.fromByteBuf(byteBuf));
                    return;
                case OC_HOME_PREFIX:
                    out.add(HomePrefixRequest.fromByteBuf(byteBuf));
                    return;
                case OC_UNHOME_PREFIX:
                    out.add(UnhomePrefixRequest.fromByteBuf(byteBuf));
                    return;
                case OC_LIST_IDS:
                    out.add(ListIdentifiersRequest.fromByteBuf(byteBuf));
                    return;
                case OC_LIST_DERIVED_PREFIXES:
                    out.add(ListDerivedPrefixsRequest.fromByteBuf(byteBuf));
                    return;
                case OC_LIST_HOMED_PREFIXES:
                    out.add(ListHomedPrefixRequest.fromByteBuf(byteBuf));
                    return;
                case OC_CHALLENGE_RESPONSE:
                    //challenge response request is sent from client to server
                    //VerificationRequest is from client to server
                    out.add(ChallengeResponseRequest.fromByteBuf(byteBuf));
                    return;
                case OC_VERIFY_RESPONSE:
                    //verify response is sent from key manager server.
                    //@TODO ack?
                    out.add(VerificationRequest.fromByteBuf(byteBuf));
                    return;
                case OC_SESSION_SETUP:
                    out.add(SessionSetupRequest.fromByteBuf(byteBuf));
                    return;
                case OC_SESSION_TERMINATE:
                    out.add(SessionTerminationRequest.fromByteBuf(byteBuf));
                    return;
                case OC_RESERVED:
                default:
                    out.add(GeneralMessage.fromByteBuf(byteBuf));
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out.size() > 0) {
                LOGGER.info("RECV:" + out.get(0).toString());
                ((MessageBody) out.get(0)).setRequestID(msg.envelope.requestId);
                ((MessageBody) out.get(0)).setSender(msg.getSender());
            }

            if (msg != null && msg.body != null && msg.body.toRead != null) msg.body.toRead.release();
        }
    }
}
