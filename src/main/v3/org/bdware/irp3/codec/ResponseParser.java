package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.body.*;

public class ResponseParser {

    public static MessageBody parse(IrpMessage msg) {
        try {
            // decode request
            MessageHeader header = msg.header;
            Opcode opcode = Opcode.fromInt(header.opCode);
            ByteBuf byteBuf = msg.body.toRead;
            if (opcode == null) return GeneralMessage.fromByteBuf(byteBuf);
            switch (opcode) {
                case OC_RESOLUTION:
                    return QueryRequest.fromByteBuf(byteBuf);
                case OC_GET_SITE_INFO:
                    return GetSiteInfoRequest.fromByteBuf(byteBuf);
                case OC_CREATE_ID:
                    return CreateIdentifierRequest.fromByteBuf(byteBuf);
                case OC_DELETE_ID:
                    return DeleteIdentifierRequest.fromByteBuf(byteBuf);
                case OC_ADD_ELEMENT:
                    return AddElementRequest.fromByteBuf(byteBuf);
                case OC_REMOVE_ELEMENT:
                    return RemoveElementRequest.fromByteBuf(byteBuf);
                case OC_MODIFY_ELEMENT:
                    return ModifyElementRequest.fromByteBuf(byteBuf);
                case OC_LIST_IDS:
                    return ListIdentifiersRequest.fromByteBuf(byteBuf);
                case OC_LIST_DERIVED_PREFIXES:
                    return ListDerivedPrefixsRequest.fromByteBuf(byteBuf);
                case OC_LIST_HOMED_PREFIXES:
                    return ListHomedPrefixRequest.fromByteBuf(byteBuf);
                case OC_CHALLENGE_RESPONSE:
                    //challenge response is sent from client to server
                    //challenge reponse from client to server is !VerificationRequest!
                    return ChallengeResponseRequest.fromByteBuf(byteBuf);
                case OC_VERIFY_RESPONSE:
                    //verify response is sent from key manager server.
                    return VerificationResponse.fromByteBuf(byteBuf);
                case OC_HOME_PREFIX:
                    return HomePrefixRequest.fromByteBuf(byteBuf);
                case OC_UNHOME_PREFIX:
                    return UnhomePrefixRequest.fromByteBuf(byteBuf);
                case OC_SESSION_SETUP:
                    return SessionSetupRequest.fromByteBuf(byteBuf);
                case OC_SESSION_TERMINATE:
                    return SessionTerminationRequest.fromByteBuf(byteBuf);
                case OC_RESERVED:
                default:
                    return GeneralMessage.fromByteBuf(byteBuf);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return GeneralMessage.fromByteBuf(msg.body.toRead);
        } finally {
            if (msg != null && msg.body != null && msg.body.toRead != null) msg.body.toRead.release();
        }
    }
}