package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;

public class IntegerList {
    public static int[] fromByteBuf(ByteBuf byteBuf) {
        int indexListLength = byteBuf.readInt();
        if (indexListLength > 0) {
            int[] indexList = new int[indexListLength];
            for (int i = 0; i < indexListLength; i++)
                indexList[i] = byteBuf.readInt();
            return indexList;
        } else return new int[0];
    }

    public static void toByteBuf(int[] data, ByteBuf buf) {
        if (data==null|| data.length==0)
            buf.writeInt(0);
        else {
            buf.writeInt(data.length);
            for (int i:data)
                buf.writeInt(i);
        }
    }
}
