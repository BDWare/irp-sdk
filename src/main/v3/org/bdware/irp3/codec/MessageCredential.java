package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;

public class MessageCredential {
    int length;
    final static int RESERVED1 = 0;
    final static int RESERVED2 = 0;
    int sessionCount;
    String type;//size=4bytes
    SignedInfo signedInfo;

    public static MessageCredential fromByteBuf(ByteBuf msg) {
        MessageCredential credential = new MessageCredential();
        credential.length = msg.readInt();
        msg.readInt();
        msg.readInt();
        credential.sessionCount = msg.readInt();
        credential.type = UTF8String.fromByteBuf(msg);
        credential.signedInfo = SignedInfo.fromByteBuf(msg);
        return credential;
    }

    public void toByteBuf(ByteBuf byteBuf) {
        //todo calculate length;
        int writerIndex = byteBuf.writerIndex();
        byteBuf.writeInt(length);
        byteBuf.writeInt(0);
        byteBuf.writeInt(0);
        byteBuf.writeInt(sessionCount);
        UTF8String.toByteBuf(type, byteBuf);
        signedInfo.toByteBuf(byteBuf);

    }
}
