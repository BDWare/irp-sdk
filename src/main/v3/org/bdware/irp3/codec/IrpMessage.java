package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.ResponseCode;

import java.net.InetSocketAddress;

public class IrpMessage implements Message {
    public MessageEnvelope envelope;
    public MessageHeader header;
    public MessageBody body;
    public MessageCredential credential;

    static Logger LOGGER = LogManager.getLogger(IrpMessage.class);
    private InetSocketAddress sender;


    public static IrpMessage fromByteBuf(ByteBuf msg) {
        IrpMessage message = new IrpMessage();
        message.envelope = MessageEnvelope.fromByteBuf(msg);
        message.header = MessageHeader.fromByteBuf(msg);
        int len = message.header.bodyLength;
        message.body = new RawBody();
        int readerInder = msg.readerIndex();
        // @TODO
        // check this
        message.body.toRead = msg.retainedSlice(readerInder, len);
        assert message.body.toRead != null;
        int readerIndex = msg.readerIndex();
        if (message.header.getCertifiedFlag()) {
            msg.readerIndex(len + readerIndex);
            message.credential = MessageCredential.fromByteBuf(msg);
        }
        return message;
    }

    public void toByteBuf(ByteBuf byteBuf) {
        int beginIndex = byteBuf.writerIndex();
        //fixed 20bytes
        envelope.toByteBuf(byteBuf);
        int headerIndex = byteBuf.writerIndex();
        //fixed 24bytes
        header.toByteBuf(byteBuf);
        body.toByteBuf(byteBuf);
        int bodyIndex = byteBuf.writerIndex();
        if (credential != null) credential.toByteBuf(byteBuf);
        int finalIndex = byteBuf.writerIndex();
        //totalLength = header+body+credential
        int totalLength = finalIndex - headerIndex;
        //bodyLength = body
        int bodyLength = bodyIndex - headerIndex - 24;
        byteBuf.writerIndex(beginIndex + 16);
        byteBuf.writeInt(totalLength);
        byteBuf.writerIndex(beginIndex + 40);
        byteBuf.writeInt(bodyLength);
        byteBuf.writerIndex(finalIndex);
        assert bodyLength >= 0;
        assert totalLength >= 0;
    }

    public String prettyPrint() {
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("envHead: 0x%x  sessionId: %d requestId: %d sequenceNumber: %d messageLength: %d",
                envelope.compoundHead,
                envelope.sessionId,
                envelope.requestId,
                envelope.sequenceNumber,
                envelope.messageLength));
        sb.append(String.format("\t\t\t\topCode:%s respCode:%s opFlag:0x%x siteInfoAndRecursionCount: 0x%x expirationTime: %d bodyLength:%d\n",
                Opcode.fromInt(header.opCode),
                ResponseCode.fromInt(header.responseCode),
                header.opFlag,
                header.siteInfoAndRecursionCount,
                header.expirationTime,
                header.bodyLength));
        return sb.toString();
    }

    public static String byteArrayToHexString(byte[] byteArray) {
        if (byteArray == null) {
            return null;
        }
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[byteArray.length * 3];
        for (int j = 0; j < byteArray.length; j++) {
            int v = byteArray[j] & 0xFF;
            hexChars[j * 3] = hexArray[v >>> 4];
            hexChars[j * 3 + 1] = hexArray[v & 0x0F];
            hexChars[j * 3 + 2] = ' ';
        }
        return new String(hexChars);
    }


    public static String byteArrayToBinaryString(byte[] byteArray) {
        if (byteArray == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (byte single : byteArray) {
            sb.append(Integer.toBinaryString(single));
        }
        return sb.toString();
    }

    public void setSender(InetSocketAddress sender) {
        this.sender = sender;
    }

    public InetSocketAddress getSender() {
        return this.sender;
    }
}
