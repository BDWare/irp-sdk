package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;

public class StringList {
    public static String[] fromByteBuf(ByteBuf byteBuf) {
        int indexListLength = byteBuf.readInt();
        if (indexListLength > 0) {
            String[] indexList = new String[indexListLength];
            for (int i = 0; i < indexListLength; i++)
                indexList[i] = UTF8String.fromByteBuf(byteBuf);
            return indexList;
        } else return new String[0];
    }

    public static void toByteBuf(String[] data, ByteBuf buf) {
        if (data == null || data.length == 0)
            buf.writeInt(0);
        else {
            buf.writeInt(data.length);
            for (String i : data)
                UTF8String.toByteBuf(i, buf);
        }
    }
}
