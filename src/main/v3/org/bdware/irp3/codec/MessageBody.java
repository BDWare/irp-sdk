package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.ResponseCode;

import java.net.InetSocketAddress;

public abstract class MessageBody implements Message {
    private int requestId;
    private ResponseCode responseCode = ResponseCode.RC_ERRORGeneral;
    public ByteBuf toRead;
    private InetSocketAddress sender;

    public abstract void toByteBuf(ByteBuf byteBuf);

    public abstract Opcode getOpcode();

    public IrpMessage wrapHeader() {
        IrpMessage message = new IrpMessage();
        message.envelope = MessageEnvelope.defaultEnvelop();
        message.envelope.requestId = requestId;
        message.header = new MessageHeader();
        message.header.opCode = getOpcode().code;
        message.header.responseCode = getResponseCode().code;
        message.header.opFlag = 0;
        message.header.siteInfoAndRecursionCount = 0;
        message.header.expirationTime = 0;
        message.body = this;
        message.credential = null;
        message.setSender(getSender());
        return message;
    }

    public ResponseCode getResponseCode() {
        return responseCode;
    }

    public MessageBody setResponseCode(ResponseCode responseCode) {
        this.responseCode = responseCode;
        return this;
    }

    public MessageBody setRequestID(int requestId) {
        this.requestId = requestId;
        return this;
    }

    public int getRequestID() {
        return requestId;
    }

    public void setSender(InetSocketAddress sender) {
        this.sender = sender;
    }

    public InetSocketAddress getSender() {
        return this.sender;
    }
}
