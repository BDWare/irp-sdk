package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;

public class SignedInfo {
    int length;
    String digestAlgorithm;
    int signedDataLength;
    byte[] signedData;

    public static SignedInfo fromByteBuf(ByteBuf byteBuf) {
        SignedInfo info = new SignedInfo();
        info.length = byteBuf.readInt();
        info.digestAlgorithm = UTF8String.fromByteBuf(byteBuf);
        info.signedDataLength = byteBuf.readInt();
        info.signedData = new byte[info.signedDataLength];
        byteBuf.readBytes(info.signedData);
        return info;
    }

    public void toByteBuf(ByteBuf byteBuf) {
        byteBuf.writeInt(length);
        length = UTF8String.length(digestAlgorithm) + ByteList.length(signedData);
        UTF8String.toByteBuf(digestAlgorithm, byteBuf);
        ByteList.toByteBuf(signedData, byteBuf);
    }
}
