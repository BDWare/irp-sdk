package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageCodec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetSocketAddress;
import java.util.List;

public class DatagramPacketToIrpMessageCodec extends MessageToMessageCodec<DatagramPacket, IrpMessage> {
    static Logger LOGGER = LogManager.getLogger(DatagramPacketToIrpMessageCodec.class);
    InetSocketAddress address = null;

    //use in server!
    public DatagramPacketToIrpMessageCodec() {

    }

    //use in Client!
    public DatagramPacketToIrpMessageCodec(String host, int port) {
        address = new InetSocketAddress(host, port);
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, IrpMessage msg, List<Object> out) throws Exception {
        try {
            ByteBuf buf = ctx.alloc().directBuffer();
            msg.toByteBuf(buf);
            DatagramPacket packet;
            if (address == null)
                packet = new DatagramPacket(buf, msg.getSender());
            else
                packet = new DatagramPacket(buf, address);
            out.add(packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, DatagramPacket msg, List<Object> out) throws Exception {
        ByteBuf buf = msg.content();
        IrpMessage irpMessage = IrpMessage.fromByteBuf(buf);
        irpMessage.setSender(msg.sender());
        out.add(irpMessage);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        LOGGER.debug("got exception: " + cause.getMessage());
        cause.printStackTrace();
        if (ctx.channel().isActive()) ctx.close();
    }
}
