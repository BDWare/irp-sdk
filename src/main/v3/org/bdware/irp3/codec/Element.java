package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.codec.predefined.ElementSerializable;

public class Element implements ElementSerializable {
    private int index;
    private String type;
    private byte[] value;
    private byte permission;
    private byte ttlType;
    private int ttl;
    private int timestamp;
    static final int references = 0;

    public Element() {
    }

    public Element(Element element) {
        this.setIndex(element.getIndex());
        this.setType(element.getType());
        this.setValue(element.getValue());
        this.setPermission(element.getPermission());
        this.setTtlType(element.getTtlType());
        this.setTtl(element.getTtl());
        this.setTimestamp(element.getTimestamp());
        fillFieldsFromValue();
    }

    public static Element[] listFromByteBuf(ByteBuf byteBuf) {
        int len = byteBuf.readInt();
        if (len <= 0) len = 0;
        Element[] ret = new Element[len];
        for (int i = 0; i < len; i++)
            ret[i] = Element.fromByteBuf(byteBuf);
        return ret;
    }

    public static void toByteBuf(Element[] elements, ByteBuf byteBuf) {
        if (elements == null || elements.length == 0) {
            byteBuf.writeInt(0);
            return;
        }
        byteBuf.writeInt(elements.length);
        for (int i = 0; i < elements.length; i++)
            elements[i].toByteBuf(byteBuf);
        return;
    }


    public static Element fromByteBuf(ByteBuf byteBuf) {
        Element element = new Element();
        element.setIndex(byteBuf.readInt());
        element.setType(UTF8String.fromByteBuf(byteBuf));
        element.setValue(ByteList.fromByteBuf(byteBuf));
        element.setPermission(byteBuf.readByte());
        element.setTtlType(byteBuf.readByte());
        element.setTtl(byteBuf.readInt());
        element.setTimestamp(byteBuf.readInt());
        //consume the deprecated references
        byteBuf.readInt();
        return element;
    }

    public void toByteBuf(ByteBuf byteBuf) {
        value = calculateValue();
        byteBuf.writeInt(index);
        UTF8String.toByteBuf(type, byteBuf);
        ByteList.toByteBuf(value, byteBuf);
        byteBuf.writeByte(permission);
        byteBuf.writeByte(ttlType);
        byteBuf.writeInt(ttl);
        byteBuf.writeInt(timestamp);
        // add deprecated references
        byteBuf.writeInt(0);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value = value;
    }

    public byte getPermission() {
        return permission;
    }

    public void setPermission(byte permission) {
        this.permission = permission;
    }

    public byte getTtlType() {
        return ttlType;
    }

    public void setTtlType(byte ttlType) {
        this.ttlType = ttlType;
    }

    public int getTtl() {
        return ttl;
    }

    public void setTtl(int ttl) {
        this.ttl = ttl;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public void fillFieldsFromValue() {
        return;
    }

    @Override
    public byte[] calculateValue() {
        return value;
    }
}
