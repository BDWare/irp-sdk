package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;

public class RequestDigest {
    byte digestAlgorithmIdentifier;
    byte[] messageDigest;

    public RequestDigest(byte identifier, byte[] digest) {
        this.digestAlgorithmIdentifier = identifier;
        this.messageDigest = digest;
    }

    public static RequestDigest fromByteBuf(ByteBuf buf) {
        RequestDigest requestDigest = new RequestDigest(buf.readByte(),null);
        switch (requestDigest.digestAlgorithmIdentifier) {
            case 1:
                requestDigest.messageDigest = new byte[16];
                break;
            case 2:
                requestDigest.messageDigest = new byte[20];
                break;
            case 3:
                requestDigest.messageDigest = new byte[32];
                break;
            default:
                requestDigest.messageDigest = null;
        }
        if (requestDigest.messageDigest != null)
            buf.readBytes(requestDigest.messageDigest);
        return requestDigest;
    }

    public void toByteBuf(ByteBuf byteBuf) {
        byteBuf.writeByte(digestAlgorithmIdentifier);
        switch (digestAlgorithmIdentifier) {
            case 1:
            case 2:
            case 3:
                byteBuf.writeBytes(messageDigest);
                break;
            default:
        }
    }
}
