package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.ResponseCode;
import org.bdware.irp3.body.*;

import java.util.List;

public class ResponseCodec extends MessageToMessageCodec<IrpMessage, MessageBody> {
    static Logger LOGGER = LogManager.getLogger(ResponseCodec.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, MessageBody msg, List<Object> out) throws Exception {
        //encode response and request uses the same method
        IrpMessage message = msg.wrapHeader();
        out.add(message);
        LOGGER.info("SEND:" + msg.toString() + " -> " + message.toString());
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, IrpMessage msg, List<Object> out) throws Exception {
        try {
            // decode request
            MessageHeader header = msg.header;
            Opcode opcode = Opcode.fromInt(header.opCode);
            ByteBuf byteBuf = msg.body.toRead;
            ResponseCode responseCode = ResponseCode.fromInt(header.responseCode);
            if (opcode == null) out.add(GeneralMessage.fromByteBuf(byteBuf));
            if (responseCode.equals(ResponseCode.RC_AUTHEN_NEEDED)) {
                out.add(ClientAuthResponse.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
            }
            switch (opcode) {
                case OC_RESOLUTION:
                    out.add(QueryResponse.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
                case OC_GET_SITE_INFO:
                    out.add(GetSiteInfoResponse.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
                case OC_CREATE_ID:
                    out.add(CreateIdentifierResponse.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
                case OC_DELETE_ID:
                case OC_ADD_ELEMENT:
                case OC_REMOVE_ELEMENT:
                case OC_MODIFY_ELEMENT:
                case OC_HOME_PREFIX:
                case OC_UNHOME_PREFIX:
                    out.add(RawBody.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
                case OC_LIST_IDS:
                    out.add(ListIdentifiersResponse.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
                case OC_LIST_DERIVED_PREFIXES:
                    out.add(ListDerivedPrefixsResponse.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
                case OC_LIST_HOMED_PREFIXES:
                    out.add(ListHomedPrefixResponse.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
                case OC_CHALLENGE_RESPONSE:
                    //challenge response request is sent from client to server
                    //challenge response from client to server is !VerificationRequest!
                    out.add(ChallengeResponseResponse.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
                case OC_VERIFY_RESPONSE:
                    //verify response is sent from key manager server.
                    //@TODO ack?
                    out.add(VerificationResponse.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
                case OC_SESSION_SETUP:
                    out.add(SessionSetupResponse.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
                case OC_SESSION_TERMINATE:
                    out.add(SessionTerminationRequest.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
                case OC_RESERVED:
                default:
                    if (responseCode == ResponseCode.RC_ERRORGeneral) {
                        out.add(ErrorResponse.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    } else
                        out.add(GeneralMessage.fromByteBuf(byteBuf).setRequestID(msg.envelope.requestId).setResponseCode(responseCode));
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (msg != null && msg.body != null && msg.body.toRead != null) msg.body.toRead.release();
            if (out.size() > 0)
                LOGGER.info("RECV:" + out.get(0).toString());

        }
    }
}
