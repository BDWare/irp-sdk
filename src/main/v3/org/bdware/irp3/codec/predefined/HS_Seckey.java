package org.bdware.irp3.codec.predefined;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import org.bdware.irp3.codec.ByteList;
import org.bdware.irp3.codec.Element;
import org.bdware.irp3.codec.UTF8String;

/**
 * The value of the element is the secret key.
 * In order to protect the secret key, the permission of the element should be set to forbid PUBLIC_READ.
 * This is the only common usage of any permission other than PUBLIC_READ, ADMIN_READ, and ADMIN_WRITE.
 *
 * Use of public keys is recommended over use of secret keys for DO-IRP authentication.
 */
public class HS_Seckey extends Element {
    public static String TYPE = "HS_SECKEY";
    byte[] secretKey;
    String keyType;
    short reserved;

    public HS_Seckey(Element element) {
        super(element);
        assert element.getType().equals(TYPE);
    }

    public HS_Seckey() {
        setType(TYPE);
    }

    @Override
    public void fillFieldsFromValue() {
        ByteBuf byteBuf = Unpooled.wrappedBuffer(getValue());
        keyType = UTF8String.fromByteBuf(byteBuf);
        reserved = byteBuf.readShort();
        secretKey = ByteList.fromByteBuf(byteBuf);
    }

    @Override
    public byte[] calculateValue() {
        ByteBuf byteBuf = Unpooled.directBuffer();
        UTF8String.toByteBuf(keyType, byteBuf);
        byteBuf.writeShort(reserved);
        ByteList.toByteBuf(secretKey, byteBuf);
        return ByteBufUtil.getBytes(byteBuf);
    }

}
