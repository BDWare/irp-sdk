package org.bdware.irp3.codec.predefined;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import org.bdware.irp3.codec.Element;

public class HS_Vlist extends Element {
    public static String TYPE = "HS_VLIST";
    ElementRef[] elementRefs;

    public HS_Vlist(Element element) {
        super(element);
        assert element.getType().equals(TYPE);
    }

    public HS_Vlist() {
        setType(TYPE);
    }

    @Override
    public void fillFieldsFromValue() {
        ByteBuf byteBuf = Unpooled.wrappedBuffer(getValue());
        elementRefs = ElementRef.listFromByteBuf(byteBuf);
    }

    @Override
    public byte[] calculateValue() {
        ByteBuf byteBuf = Unpooled.directBuffer();
        ElementRef.toByteBuf(elementRefs, byteBuf);
        return ByteBufUtil.getBytes(byteBuf);
    }
}
