package org.bdware.irp3.codec.predefined;

import org.bdware.irp3.codec.Element;

import java.nio.charset.StandardCharsets;

public class HS_Serv extends Element {
    public static class Prefix extends HS_Site {
        public static String PREFIX_TYPE = "HS_SERV.PREFIX";

        public Prefix(Element element) {
            //avoid call super(element), assertion error!
            super();
            //manually initialize fields in Element.
            this.setIndex(element.getIndex());
            this.setType(element.getType());
            this.setValue(element.getValue());
            this.setPermission(element.getPermission());
            this.setTtlType(element.getTtlType());
            this.setTtl(element.getTtl());
            this.setTimestamp(element.getTimestamp());
            fillFieldsFromValue();
            setType(PREFIX_TYPE);
        }

        public Prefix() {
            super();
            setType(PREFIX_TYPE);
        }
    }

    public static String TYPE = "HS_SERV";

    public HS_Serv(Element element) {
        super(element);
        assert element.getType().equals(TYPE);
    }

    public HS_Serv(String serverIdentifier) {
        setType(TYPE);
        setServIdentifier(serverIdentifier);
    }

    public void setServIdentifier(String identifier) {
        setValue(identifier.getBytes(StandardCharsets.UTF_8));
    }

    public String getServIdentifier() {
        return new String(getValue(), StandardCharsets.UTF_8);
    }

    @Override
    public void fillFieldsFromValue() {
        setType(TYPE);
    }

    @Override
    public byte[] calculateValue() {
        return getValue();
    }

}
