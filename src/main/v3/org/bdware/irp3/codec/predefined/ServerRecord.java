package org.bdware.irp3.codec.predefined;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.codec.ByteList;

public class ServerRecord {
    int serverID;
    byte[] address;
    byte[] publicKeyRecord;
    ServiceInterface[] serviceInterfaces;

    //publicKey=  keytype(string), 2byte reserved,
    public static ServerRecord fromByteBuf(ByteBuf byteBuf) {
        ServerRecord record = new ServerRecord();
        record.serverID = byteBuf.readInt();
        record.address = ByteList.fromByteBuf(byteBuf);
        record.publicKeyRecord = ByteList.fromByteBuf(byteBuf);
        record.serviceInterfaces = ServiceInterface.listFromByteBuf(byteBuf);
        return record;
    }

    public static ServerRecord[] listFromByteBuf(ByteBuf byteBuf) {
        int len = byteBuf.readInt();
        if (len <= 0) len = 0;
        ServerRecord[] ret = new ServerRecord[len];
        for (int i = 0; i < len; i++)
            ret[i] = fromByteBuf(byteBuf);
        return ret;
    }

    public void toByteBuf(ByteBuf byteBuf) {
        byteBuf.writeInt(serverID);
        ByteList.toByteBuf(address, byteBuf);
        ByteList.toByteBuf(publicKeyRecord, byteBuf);
        ServiceInterface.toByteBuf(serviceInterfaces, byteBuf);
    }

    public static void toByteBuf(ServerRecord[] serverRecords, ByteBuf byteBuf) {
        if (serverRecords == null || serverRecords.length == 0) {
            byteBuf.writeInt(0);
            return;
        }
        byteBuf.writeInt(serverRecords.length);
        for (int i = 0; i < serverRecords.length; i++)
            serverRecords[i].toByteBuf(byteBuf);
        return;
    }
}
