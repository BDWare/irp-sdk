package org.bdware.irp3.codec.predefined;

public class TransportProtocol {
    public final static short UDP = 0x00;
    public final static short TCP = 0x01;
    public final static short HTTP = 0x02;
    public final static short HTTPS = 0x03;
}
