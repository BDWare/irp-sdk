package org.bdware.irp3.codec.predefined;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.codec.UTF8String;

public class ElementRef {
    String identifier;
    int index;

    public static ElementRef fromByteBuf(ByteBuf byteBuf) {
        ElementRef adminRef = new ElementRef();
        adminRef.identifier = UTF8String.fromByteBuf(byteBuf);
        adminRef.index = byteBuf.readInt();
        return adminRef;
    }

    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(identifier, byteBuf);
        byteBuf.writeInt(index);
    }

    public static ElementRef[] listFromByteBuf(ByteBuf byteBuf) {
        int len = byteBuf.readInt();
        if (len <= 0) len = 0;
        ElementRef[] ret = new ElementRef[len];
        for (int i = 0; i < len; i++)
            ret[i] = ElementRef.fromByteBuf(byteBuf);
        return ret;
    }


    public static void toByteBuf(ElementRef[] adminRefs, ByteBuf byteBuf) {
        if (adminRefs == null || adminRefs.length == 0) {
            byteBuf.writeInt(0);
            return;
        }
        byteBuf.writeInt(adminRefs.length);
        for (int i = 0; i < adminRefs.length; i++)
            adminRefs[i].toByteBuf(byteBuf);
        return;
    }
}
