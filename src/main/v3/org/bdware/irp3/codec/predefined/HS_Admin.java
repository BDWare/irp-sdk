package org.bdware.irp3.codec.predefined;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import org.bdware.irp3.codec.Element;

public class HS_Admin extends Element {
    short adminPermission;
    ElementRef adminRef;
    public static String TYPE = "HS_ADMIN";

    public HS_Admin(Element element) {
        super(element);
        assert element.getType().equals(TYPE);
    }

    public HS_Admin() {
        setType(TYPE);
    }

    @Override
    public void fillFieldsFromValue() {
        ByteBuf byteBuf = Unpooled.wrappedBuffer(getValue());
        adminPermission = byteBuf.readShort();
        adminRef = ElementRef.fromByteBuf(byteBuf);
    }

    @Override
    public byte[] calculateValue() {
        ByteBuf byteBuf = Unpooled.directBuffer();
        byteBuf.writeShort(adminPermission);
        adminRef.toByteBuf(byteBuf);
        return ByteBufUtil.getBytes(byteBuf);
    }
}
