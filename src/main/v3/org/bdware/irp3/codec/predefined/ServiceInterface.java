package org.bdware.irp3.codec.predefined;

import io.netty.buffer.ByteBuf;

public class ServiceInterface {

    public static final int UDP_TCP_PORT = 2641;
    public static int HTTP_PORT = 8000;

    byte serviceType;
    byte transportProtocol;
    int portNumber;

    public static ServiceInterface[] listFromByteBuf(ByteBuf buf) {
        int len = buf.readInt();
        if (len < 0) len = 0;
        ServiceInterface[] ret = new ServiceInterface[len];
        for (int i = 0; i < len; i++) {
            ret[i] = fromByteBuf(buf);
        }
        return ret;
    }

    public static ServiceInterface fromByteBuf(ByteBuf buf) {
        ServiceInterface ret = new ServiceInterface();
        ret.serviceType = buf.readByte();
        ret.transportProtocol = buf.readByte();
        ret.portNumber = buf.readInt();
        return ret;
    }

    public static void toByteBuf(ServiceInterface[] serviceInterfaces, ByteBuf byteBuf) {
        if (serviceInterfaces == null || serviceInterfaces.length == 0) {
            byteBuf.writeInt(0);
            return;
        }
        byteBuf.writeInt(serviceInterfaces.length);
        for (int i = 0; i < serviceInterfaces.length; i++)
            serviceInterfaces[i].toByteBuf(byteBuf);
        return;
    }

    public void toByteBuf(ByteBuf buf) {
        buf.writeByte(serviceType);
        buf.writeByte(transportProtocol);
        buf.writeInt(portNumber);
    }
}
