package org.bdware.irp3.codec.predefined;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import org.bdware.irp3.codec.ByteList;
import org.bdware.irp3.codec.Element;
import org.bdware.irp3.codec.UTF8String;

/**
 * The value of the element is a binary encoding of the public key which for key types considered
 * in this specification is as follows. First, there is a UTF8-string that describes the key type;
 * then, a two- byte option field reserved for future use; and finally, a key-type-dependent number
 * of length- prefixed byte-arrays that contains the public key itself. The key types in current use
 * are “DSA_PUB_KEY”, where there are four byte-arrays after the two-byte option field for the four
 * DSA parameters q, p, g, and y; and “RSA_PUB_KEY”, where after the two-byte option field are two
 * byte- arrays for the exponent and modulus, followed by an empty byte-array (four zero bytes).
 */
public class HS_Pubkey extends Element {
    public static String TYPE = "HS_PUBKEY";
    String keyType;
    short reserved;
    byte[] publicKey;

    public HS_Pubkey(Element element) {
        super(element);
        assert element.getType().equals(TYPE);
    }

    public HS_Pubkey() {
        setType(TYPE);
    }

    @Override
    public void fillFieldsFromValue() {
        ByteBuf byteBuf = Unpooled.wrappedBuffer(getValue());
        keyType = UTF8String.fromByteBuf(byteBuf);
        reserved = byteBuf.readShort();
        publicKey = ByteList.fromByteBuf(byteBuf);
    }

    @Override
    public byte[] calculateValue() {
        ByteBuf byteBuf = Unpooled.directBuffer();
        UTF8String.toByteBuf(keyType, byteBuf);
        byteBuf.writeShort(reserved);
        ByteList.toByteBuf(publicKey, byteBuf);
        return ByteBufUtil.getBytes(byteBuf);
    }

}
