package org.bdware.irp3.codec.predefined;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import org.bdware.irp3.codec.Element;
import org.bdware.irp3.codec.UTF8String;

public class HS_Site extends Element {
    public static class Prefix extends HS_Site {
        public static String PREFIX_TYPE = "HS_SITE.PREFIX";
        public Prefix(Element element) {
            //avoid call super(element), assertion error!
            super();
            //manually initialize fields in Element.
            this.setIndex(element.getIndex());
            this.setType(element.getType());
            this.setValue(element.getValue());
            this.setPermission(element.getPermission());
            this.setTtlType(element.getTtlType());
            this.setTtl(element.getTtl());
            this.setTimestamp(element.getTimestamp());
            fillFieldsFromValue();
            setType(PREFIX_TYPE);
        }

        public Prefix() {
            super();
            setType(PREFIX_TYPE);
        }
    }

    private short version;
    private short protocolVersion;
    private short serialNumber;
    private byte primaryMask;
    private byte hashOption;
    private String hashFilter;
    private Attribute[] attributes;
    //desc, alt_addr, alt_addr.serverid, path.serverid
    private int numOfServer;
    private ServerRecord[] serverRecords;
    public static String TYPE = "HS_SITE";

    public HS_Site(Element element) {
        super(element);
        assert element.getType().equals(TYPE);
    }

    public HS_Site() {
        setType(TYPE);
    }

    @Override
    public void fillFieldsFromValue() {
        ByteBuf toRead = Unpooled.wrappedBuffer(this.getValue());
        version = toRead.readShort();
        protocolVersion = toRead.readShort();
        serialNumber = toRead.readShort();
        primaryMask = toRead.readByte();
        hashOption = toRead.readByte();
        hashFilter = UTF8String.fromByteBuf(toRead);
        attributes = Attribute.listFromByteBuf(toRead);
        numOfServer = toRead.readInt();
        serverRecords = ServerRecord.listFromByteBuf(toRead);
        setType(TYPE);
    }

    @Override
    public byte[] calculateValue() {
        ByteBuf buf = Unpooled.directBuffer();
        buf.writeShort(version);
        buf.writeShort(protocolVersion);
        buf.writeShort(serialNumber);
        buf.writeByte(primaryMask);
        buf.writeByte(hashOption);
        UTF8String.toByteBuf(hashFilter, buf);
        Attribute.toByteBuf(attributes, buf);
        buf.writeInt(numOfServer);
        ServerRecord.toByteBuf(serverRecords, buf);
        byte[] value = ByteBufUtil.getBytes(buf);
        return value;
    }

    public short getVersion() {
        return version;
    }

    public void setVersion(short version) {
        this.version = version;
    }

    public short getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(short protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public short getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(short serialNumber) {
        this.serialNumber = serialNumber;
    }

    public byte getPrimaryMask() {
        return primaryMask;
    }

    public void setPrimaryMask(byte primaryMask) {
        this.primaryMask = primaryMask;
    }

    public byte getHashOption() {
        return hashOption;
    }

    public void setHashOption(byte hashOption) {
        this.hashOption = hashOption;
    }

    public String getHashFilter() {
        return hashFilter;
    }

    public void setHashFilter(String hashFilter) {
        this.hashFilter = hashFilter;
    }

    public Attribute[] getAttributes() {
        return attributes;
    }

    public void setAttributes(Attribute[] attributes) {
        this.attributes = attributes;
    }

    public int getNumOfServer() {
        return numOfServer;
    }

    public void setNumOfServer(int numOfServer) {
        this.numOfServer = numOfServer;
    }

    public ServerRecord[] getServerRecords() {
        return serverRecords;
    }

    public void setServerRecords(ServerRecord[] serverRecords) {
        this.serverRecords = serverRecords;
    }


}
