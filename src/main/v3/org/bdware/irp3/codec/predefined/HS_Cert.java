package org.bdware.irp3.codec.predefined;

import com.nimbusds.jose.JWSObject;
import org.bdware.irp3.codec.Element;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;

/**
 * the payload of jsonWebSignature contains:
 * "iss": the issuer of the certificate, as an element reference (index:identifier pair) or identifier
 * "sub": the subject of the certificate, as an element reference (index:identifier pair) or identifier
 * "exp", "nbf", "iat": expiration, not-before, and issued-at dates as numeric seconds since the epoch
 * "publicKey": the public key of the subject, in JWK format (RFC 7517) [6]
 * "perms": a list of permission objects, where each permission object has the form { "handle": "identifier",
 * "perm": "permission" }
 * with "permission" one of "everything", "thisHandle", "derivedPrefixes", or "handlesUnderThisPrefix"
 * "chain": an optional list of element references (index:identifier pairs) or identifiers, used to build a chain
 * of trust for validating the certificate issuer (in the absence of an explicit chain, the chain can still be built
 * implicitly, as will be discussed)
 */

public class HS_Cert extends Element {
    public static String TYPE = "HS_CERT";
    private JWSObject jsonWebSignature;

    public HS_Cert(Element element) {
        super(element);
        assert element.getType().equals(TYPE);
    }

    public HS_Cert() {
        setType(TYPE);
    }

    @Override
    public void fillFieldsFromValue() {
        try {
            jsonWebSignature = JWSObject.parse(new String(getValue(), StandardCharsets.UTF_8));
            //jsonWebSignature.getPayload().toJSONObject();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public byte[] calculateValue() {
        return jsonWebSignature.serialize().getBytes(StandardCharsets.UTF_8);
    }

    public JWSObject getJsonWebSignature() {
        return jsonWebSignature;
    }

    public void setJsonWebSignature(JWSObject jsonWebSignature) {
        this.jsonWebSignature = jsonWebSignature;
    }
}
