package org.bdware.irp3.codec.predefined;

public interface ElementSerializable {
    void fillFieldsFromValue();

    byte[] calculateValue();
}
