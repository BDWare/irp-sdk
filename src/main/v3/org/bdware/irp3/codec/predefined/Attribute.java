package org.bdware.irp3.codec.predefined;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.codec.Element;
import org.bdware.irp3.codec.UTF8String;

public class Attribute {
    String type;
    String value;

    public static Attribute fromByteBuf(ByteBuf byteBuf) {
        Attribute attribute = new Attribute();
        attribute.type = UTF8String.fromByteBuf(byteBuf);
        attribute.value = UTF8String.fromByteBuf(byteBuf);
        return attribute;
    }
    public void toByteBuf(ByteBuf byteBuf){
        UTF8String.toByteBuf(type,byteBuf);
        UTF8String.toByteBuf(value,byteBuf);
    }
    public static void toByteBuf(Attribute[] attributes, ByteBuf byteBuf) {
        if (attributes == null || attributes.length == 0) {
            byteBuf.writeInt(0);
            return;
        }
        byteBuf.writeInt(attributes.length);
        for (int i = 0; i < attributes.length; i++)
            attributes[i].toByteBuf(byteBuf);
        return;
    }
    public static Attribute[] listFromByteBuf(ByteBuf byteBuf) {
        int len = byteBuf.readInt();
        if (len <= 0) len = 0;
        Attribute[] ret = new Attribute[len];
        for (int i = 0; i < len; i++)
            ret[i] = fromByteBuf(byteBuf);
        return ret;
    }
}
