package org.bdware.irp3.codec.predefined;

import com.nimbusds.jose.JWSObject;
import org.bdware.irp3.codec.Element;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
/**
 * The payload of jsonWebSignature contains:
 • "iss": the issuer of the certificate, as an element reference (index:identifier pair) or identifier
 • "sub": the subject of the certificate, an identifier (the identifier of the record signed)
 • "exp", "nbf", "iat": expiration, not-before, and issued-at dates as numeric seconds since the epoch
 • "digests": a structure indicating the hashed values of the elements of the signed identifier record. It is an object
 with two properties: "alg", a string indicating the digest algorithm (e.g. "SHA-256" [7]); and "digests", a list of
 objects. Each of those objects has two properties: "index", a number indicating the index of an identifier record
 element, and "digest", a string with a Base64-encoding of the digest (or hash) of the element, as described below.
 • "chain": an optional list of element references (index:identifier pairs) or identifiers, used to build a chain
 of trust for validating the signature issuer (in the absence of an explicit chain, the chain can still be built
 implicitly, as will be discussed).
 * */
public class HS_Signature extends Element {
    public static String TYPE = "HS_SIGNATURE";
    private JWSObject jsonWebSignature;

    public HS_Signature(Element element) {
        super(element);
        assert element.getType().equals(TYPE);
    }

    public HS_Signature() {
        setType(TYPE);
    }

    @Override
    public void fillFieldsFromValue() {
        try {
            jsonWebSignature = JWSObject.parse(new String(getValue(), StandardCharsets.UTF_8));
            //jsonWebSignature.getPayload().toJSONObject();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public byte[] calculateValue() {
        return jsonWebSignature.serialize().getBytes(StandardCharsets.UTF_8);
    }

    public JWSObject getJsonWebSignature() {
        return jsonWebSignature;
    }

    public void setJsonWebSignature(JWSObject jsonWebSignature) {
        this.jsonWebSignature = jsonWebSignature;
    }
}
