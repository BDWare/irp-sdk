package org.bdware.irp3.codec.predefined;

public class ElementPermissions {
    public final static short PUBLIC_WRITE = 0x01;
    public final static short PUBLIC_READ = 0x02;
    public final static short ADMIN_WRITE = 0x04;
    public final static short ADMIN_READ = 0x08;
}
