package org.bdware.irp3.codec.predefined;

import org.bdware.irp3.codec.Element;

import java.nio.charset.StandardCharsets;

/**
 * An HS_ALIAS element is one whose type field is HS_ALIAS and whose value field contains a reference to another
 * identifier. An identifier whose record contains an HS_ALIAS element is an alias to the identifier referenced in
 * the HS_ALIAS element. An alias record should not have any additional elements other than HS_ALIAS or HS_ADMIN
 * (for administration) elements. This is necessary to prevent any inconsistency between an identifier and its aliases.
 */
public class HS_Alias extends Element {
    public static String TYPE = "HS_ALIAS";

    public HS_Alias(Element element) {
        super(element);
        assert element.getType().equals(TYPE);
    }

    public HS_Alias() {
        setType(TYPE);
    }

    public String getAliasIdentifier() {
        return new String(getValue(), StandardCharsets.UTF_8);
    }
}
