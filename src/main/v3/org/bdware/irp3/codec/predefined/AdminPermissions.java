package org.bdware.irp3.codec.predefined;

public class AdminPermissions {
    public final static int Add_Identifier = 0x0001;
    public final static int Delete_Identifier = 0x0002;
    public final static int Add_Derived_Prefix = 0x0004;
    public final static int Reserved = 0x0008;
    public final static int Modify_Element = 0x0010;
    public final static int Delete_Element = 0x0020;
    public final static int Add_Element = 0x0040;
    public final static int Modify_Admin = 0x0080;
    public final static int Remove_Admin = 0x0100;
    public final static int Add_Admin = 0x0200;
    public final static int Authorized_Read = 0x0400;
    public final static int List_Identifiers = 0x0800;
    public final static int List_Derived_Prefixes = 0x1000;
}
