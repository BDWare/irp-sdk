package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class IrpMessageCodec extends MessageToMessageCodec<ByteBuf, IrpMessage> {
    //new LengthFieldBasedFrameDecoder(IrpCommon.MAX_MESSAGE_PACKET_LENGTH, 16, 4, 0, 0)
    static Logger LOGGER = LogManager.getLogger(IrpMessageCodec.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, IrpMessage msg, List<Object> out) throws Exception {
        try {
            ByteBuf buff = Unpooled.directBuffer();
            msg.toByteBuf(buff);
            out.add(buff);
            LOGGER.info("SEND:" + msg.prettyPrint());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        IrpMessage irpMessage = IrpMessage.fromByteBuf(msg);
        out.add(irpMessage);
        LOGGER.info("RECV:" + irpMessage.prettyPrint());
        return;
    }
}
