package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;

public class ByteList {
    public static byte[] fromByteBuf(ByteBuf byteBuf) {
        int indexListLength = byteBuf.readInt();
        if (indexListLength > 0) {
            byte[] indexList = new byte[indexListLength];
            byteBuf.readBytes(indexList);
            return indexList;
        } else return new byte[0];
    }

    public static void toByteBuf(byte[] data, ByteBuf buf) {
        if (data == null || data.length == 0)
            buf.writeInt(0);
        else {
            buf.writeInt(data.length);
            buf.writeBytes(data);
        }
    }

    public static int length(byte[] signedData) {
        if (signedData == null) return 4;
        return 4 + signedData.length;
    }
}
