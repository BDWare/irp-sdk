package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;

public class MessageEnvelope {
    public static final int MAJORVERSION = 3;
    public static final int MINORVERSION = 0;

    int compoundHead, sessionId, requestId, sequenceNumber, messageLength;

    public static MessageEnvelope defaultEnvelop() {
        MessageEnvelope ret = new MessageEnvelope();
        ret.compoundHead = 0;
        ret.sessionId = 0;
        ret.requestId = 0;
        ret.sequenceNumber = 0;
        ret.messageLength = 0;
        return ret;
    }

    public int getMajorVersion() {
        return (compoundHead & 0xff000000) >>> 24;
    }

    public int getMinorVersion() {
        return (compoundHead & 0x00ff0000) >>> 16;
    }

    public int getFlag() {
        return (compoundHead & 0x0000e000) >>> 9;
    }

    public int getSuggMajorVersion() {
        return (compoundHead & 0x00001f80) >> 7;
    }

    public int getSuggMinorVersion() {
        return (compoundHead & 0x0000007f);
    }

    public static MessageEnvelope fromByteBuf(ByteBuf byteBuf) {
        MessageEnvelope ret = new MessageEnvelope();
        ret.compoundHead = byteBuf.readInt();
        ret.sessionId = byteBuf.readInt();
        ret.requestId = byteBuf.readInt();
        ret.sequenceNumber = byteBuf.readInt();
        ret.messageLength = byteBuf.readInt();
        return ret;
    }

    public void toByteBuf(ByteBuf byteBuf) {
        byteBuf.writeInt(compoundHead);
        byteBuf.writeInt(sessionId);
        byteBuf.writeInt(requestId);
        byteBuf.writeInt(sequenceNumber);
        byteBuf.writeInt(messageLength);
    }

    public int getRequestID() {
        return requestId;
    }

    public void setRequestID(int requestID) {
        this.requestId = requestID;
    }
}
