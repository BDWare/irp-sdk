package org.bdware.irp3.codec;

import io.netty.buffer.ByteBuf;

public class MessageHeader {
    public int opCode, responseCode, opFlag, siteInfoAndRecursionCount, expirationTime, bodyLength;


    public boolean getAuthoritativeFlag() {
        return (opFlag & 0x80000000) != 0;
    }

    public boolean getCertifiedFlag() {
        return (opFlag & 0x40000000) != 0;
    }

    public boolean getEncryptionFlag() {
        return (opFlag & 0x20000000) != 0;
    }

    public boolean getRecursiveFlag() {
        return (opFlag & 0x10000000) != 0;
    }

    public boolean getCacheAuthenticationFlag() {
        return (opFlag & 0x08000000) != 0;
    }

    public boolean getContinuousFlag() {
        return (opFlag & 0x04000000) != 0;
    }

    public boolean getKeepConnectionFlag() {
        return (opFlag & 0x02000000) != 0;
    }

    public boolean getPublicOnlyFlag() {
        return (opFlag & 0x01000000) != 0;
    }

    public boolean getRequestDigestFlag() {
        return (opFlag & 0x00800000) != 0;
    }

    public boolean getOverwriteWhenExistsFlag() {
        return (opFlag & 0x00400000) != 0;
    }

    public boolean getMintNewSuffixFlag() {
        return (opFlag & 0x00200000) != 0;
    }

    public boolean getDoNotReferFlag() {
        return (opFlag & 0x00100000) != 0;
    }

    public int getSiteInfoSerialNumber() {
        return (siteInfoAndRecursionCount & 0xffff0000) >>> 16;
    }

    public byte getRecursionCount() {
        return (byte) ((siteInfoAndRecursionCount & 0x0000ff00) >>> 8);
    }

    public static MessageHeader fromByteBuf(ByteBuf byteBuf) {
        MessageHeader header = new MessageHeader();
        header.opCode = byteBuf.readInt();
        header.responseCode = byteBuf.readInt();
        header.opFlag = byteBuf.readInt();
        header.siteInfoAndRecursionCount = byteBuf.readInt();
        header.expirationTime = byteBuf.readInt();
        header.bodyLength = byteBuf.readInt();
        return header;
    }
    public void toByteBuf(ByteBuf byteBuf){
        byteBuf.writeInt(opCode);
        byteBuf.writeInt(responseCode);
        byteBuf.writeInt(opFlag);
        byteBuf.writeInt(siteInfoAndRecursionCount);
        byteBuf.writeInt(expirationTime);
        byteBuf.writeInt(bodyLength);
    }
}
