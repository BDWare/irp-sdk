package org.bdware.irp3;

import org.bdware.irp3.body.ErrorResponse;
import org.bdware.irp3.body.QueryRequest;
import org.bdware.irp3.body.QueryResponse;
import org.bdware.irp3.codec.IrpMessage;
import org.bdware.irp3.codec.MessageEnvelope;
import org.bdware.irp3.codec.MessageHeader;

public class IrpMessageFactory {
    public static IrpMessage createQuery(QueryRequest queryRequest) {
        IrpMessage message = new IrpMessage();
        message.envelope = MessageEnvelope.defaultEnvelop();
        message.header = new MessageHeader();
        message.header.opCode = Opcode.OC_RESOLUTION.code;
        message.header.responseCode = 0;
        message.header.opFlag = 0;
        message.header.siteInfoAndRecursionCount = 0;
        message.header.expirationTime = 0;
        message.body = queryRequest;
        message.credential = null;
        return message;
    }

    public static IrpMessage createErrorResponse(IrpMessage input, String errorMessage) {
        IrpMessage message = new IrpMessage();
        message.envelope = MessageEnvelope.defaultEnvelop();
        message.envelope = MessageEnvelope.defaultEnvelop();
        message.header = new MessageHeader();
        message.header.opCode = input.header.opCode;
        message.header.responseCode = ResponseCode.RC_ERRORGeneral.code;
        message.header.opFlag = 0;
        message.header.siteInfoAndRecursionCount = 0;
        message.header.expirationTime = 0;
        message.body = new ErrorResponse(errorMessage);
        message.credential = null;
        return message;
    }


}
