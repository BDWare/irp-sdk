package org.bdware.irp3.handler;

import io.netty.channel.ChannelHandlerContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp3.ResponseCode;
import org.bdware.irp3.body.ErrorResponse;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.server.RequestHandler;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ReflectiveRequestHandler implements RequestHandler {
    static class Pair {
        IrpHandlerBase obj;
        Method method;
    }

    Map<Class, Pair> handlerMap;
    static Logger LOGGER = LogManager.getLogger(ReflectiveRequestHandler.class);
    List<IrpHandlerBase> handlers;


    public void printHandler() {
        if (handlerMap == null)
            return;
        for (Class clz : handlerMap.keySet()) {
            LOGGER.info("handler info: " + clz.getCanonicalName() + " ," + handlerMap.get(clz).method.getName());
        }
    }

    public Map<Class, Pair> getHandlerMap() {
        return handlerMap;
    }

    public void setHandlerMap(Map<Class, Pair> handlerMap) {
        this.handlerMap = handlerMap;
    }

    public List<IrpHandlerBase> getHandlers() {
        return handlers;
    }

    public void setHandlers(List<IrpHandlerBase> handlers) {
        this.handlers = handlers;
    }

    public ReflectiveRequestHandler() {
        handlerMap = new ConcurrentHashMap<>();
        handlers = new ArrayList<>();
    }

    public void addHandler(IrpHandlerBase handler) {

        Class handlerClass = handler.getClass();
        LOGGER.info("===add handler: " + handlerClass.getCanonicalName());
        for (; !handlerClass.equals(Object.class); ) {
            putHandlerMethod(handler, handlerClass);
            Class[] interfaces = handlerClass.getInterfaces();
            for (Class clz : interfaces) {
                putHandlerMethod(handler, clz);
            }
            handlerClass = handlerClass.getSuperclass();
        }
    }

    private void putHandlerMethod(IrpHandlerBase handler, Class handlerClass) {
        Method[] methods = handlerClass.getDeclaredMethods();
        for (Method m : methods) {
            IrpProcessor a = m.getAnnotation(IrpProcessor.class);
            if (a != null) {
                LOGGER.info("===add handler method: " + m.getName());
                Class<?>[] parameters = m.getParameterTypes();
                if (parameters.length != 2) {
                    if (!parameters[0].equals(ChannelHandlerContext.class) || !MessageBody.class.isAssignableFrom(parameters[1])) {
                        LOGGER.error("IrpProcessor parameter error:" + handlerClass.getCanonicalName() + ":" + m.getName());
                        continue;
                    }
                }
                if (!m.getReturnType().equals(Void.TYPE) && !MessageBody.class.isAssignableFrom(m.getReturnType())) {
                    LOGGER.error("IrpProcessor return type error:" + handlerClass.getCanonicalName() + ":" + m.getName());
                    continue;
                }
                putHandler(handler, parameters[1], m);
            }
        }
    }

    private void putHandler(IrpHandlerBase handler, Class<?> parameter, Method m) {
        m.setAccessible(true);
        Pair p = new Pair();
        p.obj = handler;
        p.method = m;
        if (handlerMap.containsKey(parameter)) {
            LOGGER.warn("Duplicated Handler:" + parameter.getCanonicalName());
        }
        handlerMap.put(parameter, p);
    }

    @Override
    public MessageBody onRequest(ChannelHandlerContext ctx, MessageBody msg) {
        Pair pair = handlerMap.get(msg.getClass());
        MessageBody ret = null;
        if (pair != null) {
            try {
                ret = (MessageBody) pair.method.invoke(pair.obj, ctx, msg);
                return ret;
            } catch (Exception e) {
                e.printStackTrace();
                ret = new ErrorResponse(e.getMessage()).setResponseCode(ResponseCode.RC_ERRORGeneral);
                return ret;
            } finally {
                if (ret != null)
                    ret.setSender(msg.getSender());
            }
        } else {
            LOGGER.info("unsupported msg type:" + msg.getClass().getCanonicalName());
            return new ErrorResponse("unsupported message:" + msg.getClass().getCanonicalName());
        }
    }
}
