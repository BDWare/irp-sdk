package org.bdware.irp3.handler;

import io.netty.channel.ChannelHandlerContext;
import org.bdware.irp3.body.*;

public interface PrefixListHandler extends IrpHandlerBase {

    @IrpProcessor
    ListIdentifiersResponse listIdentifier(ChannelHandlerContext ctx, ListIdentifiersRequest queryRequest);

    @IrpProcessor
    ListIdentifiersResponse listDerivedPrefixes(ChannelHandlerContext ctx, ListDerivedPrefixsRequest queryRequest);

}
