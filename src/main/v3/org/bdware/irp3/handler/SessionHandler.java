package org.bdware.irp3.handler;

import io.netty.channel.ChannelHandlerContext;
import org.bdware.irp3.body.SessionSetupRequest;
import org.bdware.irp3.body.SessionSetupResponse;
import org.bdware.irp3.body.SessionTerminationRequest;

public interface SessionHandler extends IrpHandlerBase {
    @IrpProcessor
    SessionSetupResponse sessionSetup(ChannelHandlerContext ctx, SessionSetupRequest queryRequest);

    @IrpProcessor
    void sessionTermination(ChannelHandlerContext ctx, SessionTerminationRequest queryRequest);

}
