package org.bdware.irp3.handler;

import io.netty.channel.ChannelHandlerContext;
import org.bdware.irp3.body.GetSiteInfoRequest;
import org.bdware.irp3.body.GetSiteInfoResponse;

public interface GetSiteInfoHandler extends IrpHandlerBase {

    @IrpProcessor
    GetSiteInfoResponse getSiteInfo(ChannelHandlerContext ctx, GetSiteInfoRequest request);

}
