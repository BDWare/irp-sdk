package org.bdware.irp3.handler;

import io.netty.channel.ChannelHandlerContext;
import org.bdware.irp3.body.HomePrefixRequest;
import org.bdware.irp3.body.ListHomedPrefixRequest;
import org.bdware.irp3.body.ListHomedPrefixResponse;
import org.bdware.irp3.body.UnhomePrefixRequest;
import org.bdware.irp3.codec.MessageBody;

public interface HomeManagementHandler extends IrpHandlerBase {

    @IrpProcessor
    MessageBody homePrefix(ChannelHandlerContext ctx, HomePrefixRequest queryRequest);

    @IrpProcessor
    MessageBody unhomePrefix(ChannelHandlerContext ctx, UnhomePrefixRequest queryRequest);

    @IrpProcessor
    ListHomedPrefixResponse listHomePrefix(ChannelHandlerContext ctx, ListHomedPrefixRequest queryRequest);
}
