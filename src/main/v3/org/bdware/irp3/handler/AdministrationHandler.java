package org.bdware.irp3.handler;

import io.netty.channel.ChannelHandlerContext;
import org.bdware.irp3.body.*;
import org.bdware.irp3.codec.MessageBody;

public interface AdministrationHandler extends IrpHandlerBase {
    @IrpProcessor
    MessageBody addElements(ChannelHandlerContext ctx, AddElementRequest request);

    @IrpProcessor
    MessageBody removeElements(ChannelHandlerContext ctx, RemoveElementRequest request);

    @IrpProcessor
    MessageBody modifyElements(ChannelHandlerContext ctx, ModifyElementRequest request);

    @IrpProcessor
    CreateIdentifierResponse createIdentifier(ChannelHandlerContext ctx, CreateIdentifierRequest request);

    @IrpProcessor
    MessageBody deleteIdentifier(ChannelHandlerContext ctx, DeleteIdentifierRequest request);
}
