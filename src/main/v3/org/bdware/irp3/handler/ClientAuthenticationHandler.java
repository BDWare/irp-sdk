package org.bdware.irp3.handler;

import io.netty.channel.ChannelHandlerContext;
import org.bdware.irp3.body.ChallengeResponseRequest;
import org.bdware.irp3.body.ChallengeResponseResponse;
import org.bdware.irp3.body.VerificationRequest;
import org.bdware.irp3.body.VerificationResponse;

//In implementation clientAuth should wait for onServerVerification?
//When implement DO-IRPServer compound with Verification
//Just Implement ClientAuthenticationHandler.clientAuth and verifiyRequest
public interface ClientAuthenticationHandler extends IrpHandlerBase {
    //method for server receiving client request
    @IrpProcessor
    ChallengeResponseResponse clientAuth(ChannelHandlerContext ctx, ChallengeResponseRequest request);

    //Required when server is not the key server!
    @IrpProcessor
    void onServerVerification(ChannelHandlerContext ctx, VerificationResponse response);

    //method for DO-IRPServer( the key server)
    @IrpProcessor
    VerificationResponse verifyRequest(ChannelHandlerContext ctx, VerificationRequest request);

}
