package org.bdware.irp3.handler;

import io.netty.channel.ChannelHandlerContext;
import org.bdware.irp3.body.QueryRequest;
import org.bdware.irp3.body.QueryResponse;

public interface QueryHandler extends IrpHandlerBase {
    @IrpProcessor
    QueryResponse query(ChannelHandlerContext ctx, QueryRequest request);

}
