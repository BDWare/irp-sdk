package org.bdware.irp3.server;

public interface BootstrapInitializer<T> {
    public T init(MessageHandler handler);
}
