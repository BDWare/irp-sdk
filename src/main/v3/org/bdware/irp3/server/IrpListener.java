package org.bdware.irp3.server;

import io.netty.bootstrap.AbstractBootstrap;
import io.netty.channel.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IrpListener {
    static Logger LOGGER = LogManager.getLogger(IrpListener.class);
    private final BootstrapInitializer<? extends AbstractBootstrap> initializer;
    private Channel ch;
    public RequestHandler requestHandler;

    public IrpListener(RequestHandler requestHandler, BootstrapInitializer<? extends AbstractBootstrap> initializer) {
        this.requestHandler = requestHandler;
        this.initializer = initializer;
    }

    public void start() {
        MessageHandler messageHandler = new MessageHandler(requestHandler);
        try {

            AbstractBootstrap bootstrap = initializer.init(messageHandler);
            ch = bootstrap.bind().syncUninterruptibly().channel();
            ch.closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        ch.close();
    }
}
