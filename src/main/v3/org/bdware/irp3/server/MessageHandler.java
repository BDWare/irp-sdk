package org.bdware.irp3.server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp3.codec.MessageBody;

@ChannelHandler.Sharable
public class MessageHandler extends SimpleChannelInboundHandler<MessageBody> {
    RequestHandler requestHandler;
    static Logger LOGGER = LogManager.getLogger(MessageHandler.class);


    public MessageHandler(RequestHandler requestHandler) {
        this.requestHandler = requestHandler;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageBody msg) throws Exception {
        MessageBody message = requestHandler.onRequest(ctx, msg);
        message.setRequestID(msg.getRequestID());
        ctx.writeAndFlush(message);
    }
}
