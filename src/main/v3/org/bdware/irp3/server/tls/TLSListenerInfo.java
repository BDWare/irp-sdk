package org.bdware.irp3.server.tls;

import org.bdware.irp.irpserver.IrpListenerInfo;

import java.io.File;

public class TLSListenerInfo extends IrpListenerInfo {
    private transient final File chainKeyFile;
    private transient final File keyFile;

    public TLSListenerInfo(String host, int port, String protocolType, File chainKeyFile, File keyFile) {
        super(host, port, protocolType);
        this.chainKeyFile = chainKeyFile;
        this.keyFile = keyFile;
    }

    public File getChainKeyFile() {
        return chainKeyFile;
    }

    public File getKeyFile() {
        return keyFile;
    }
}
