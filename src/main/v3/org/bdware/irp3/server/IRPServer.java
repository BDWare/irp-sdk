package org.bdware.irp3.server;


import org.bdware.irp.irpserver.IrpListenerInfo;
import org.bdware.irp.irpserver.IrpServerInfo;
import org.bdware.irp3.handler.IrpHandlerBase;
import org.bdware.irp3.handler.ReflectiveRequestHandler;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class IRPServer {
    private final IrpServerInfo serverInfo;
    private final ArrayList<BootstrapInitializer> initializers;
    ReflectiveRequestHandler requestCallback;

    public IRPServer(IrpServerInfo info) {
        this.serverInfo = info;
        requestCallback = new ReflectiveRequestHandler();
        initializers = new ArrayList<>();
        for (IrpListenerInfo listenerInfo : info.listenerInfos) {
            BootstrapInitializer listener = BootstrapInitializerFactory.createServerBootstrapInitializer(listenerInfo);
            initializers.add(listener);
        }
    }

    public void addHandler(IrpHandlerBase handlerBase) {
        requestCallback.addHandler(handlerBase);
    }

    public void start() {
        ExecutorService pool = Executors.newFixedThreadPool(initializers.size());
        for (BootstrapInitializer initializer : initializers){
            pool.execute(new Runnable() {
                @Override
                public void run() {
                    new IrpListener(requestCallback, initializer).start();
                }
            });
        }
    }
}
