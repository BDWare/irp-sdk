package org.bdware.irp3.server;

import io.netty.channel.ChannelHandlerContext;
import org.bdware.irp3.codec.IrpMessage;
import org.bdware.irp3.codec.MessageBody;

public interface RequestHandler {
    MessageBody onRequest(ChannelHandlerContext ctx, MessageBody msg);
}
