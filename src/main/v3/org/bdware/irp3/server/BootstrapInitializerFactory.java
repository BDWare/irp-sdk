package org.bdware.irp3.server;

import io.netty.bootstrap.AbstractBootstrap;
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.WriteBufferWaterMark;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramChannel;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp.irpserver.IrpListenerInfo;
import org.bdware.irp3.codec.DatagramPacketToIrpMessageCodec;
import org.bdware.irp3.codec.IrpMessageCodec;
import org.bdware.irp3.codec.RequestCodec;
import org.bdware.irp3.codec.WebSocketFrameToByteBufCodec;
import org.bdware.irp3.server.tls.TLSListenerInfo;

import javax.net.ssl.SSLEngine;
import java.io.File;

public class BootstrapInitializerFactory {
    static Logger LOGGER = LogManager.getLogger(BootstrapInitializerFactory.class);

    public static BootstrapInitializer<? extends AbstractBootstrap> createServerBootstrapInitializer(IrpListenerInfo listenerInfo) {
        switch (listenerInfo.protocolType.toLowerCase()) {
            case "tcp":
                return new TcpInitializer(listenerInfo);
            case "udp":
                return new UdpInitializer(listenerInfo);
            case "tls":
                return new TLSInitializer((TLSListenerInfo) listenerInfo);
            case "ws":
                return new WSInitializer(listenerInfo);
            case "http":
        }
        return null;
    }


    static class TcpInitializer implements BootstrapInitializer<ServerBootstrap> {
        IrpListenerInfo info;

        public TcpInitializer(IrpListenerInfo listenerInfo) {
            this.info = listenerInfo;
        }

        @Override
        public ServerBootstrap init(MessageHandler handler) {
            ServerBootstrap b = new ServerBootstrap();
            EventLoopGroup bossGroup = new NioEventLoopGroup(1);
            EventLoopGroup workerGroup = new NioEventLoopGroup();
            int maxFrame = 5 * 1024 * 1024;
            b.option(ChannelOption.WRITE_BUFFER_WATER_MARK,
                    new WriteBufferWaterMark(2 * maxFrame, 10 * maxFrame));
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(info.port);
            b.childHandler(
                    new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline()
                                    .addLast(new LengthFieldBasedFrameDecoder(maxFrame, 16, 4, 0, 0))
                                    .addLast(new IrpMessageCodec())
                                    .addLast(new RequestCodec())
                                    .addLast(handler);
                        }
                    });
            LOGGER.info("TCP Listen at " + info.port);
            return b;
        }
    }

    static class UdpInitializer implements BootstrapInitializer<Bootstrap> {
        IrpListenerInfo info;


        public UdpInitializer(IrpListenerInfo listenerInfo) {
            this.info = listenerInfo;
        }

        @Override
        public Bootstrap init(MessageHandler handler) {
            EventLoopGroup bossGroup = new NioEventLoopGroup(1);
            EventLoopGroup workerGroup = new NioEventLoopGroup();
            Bootstrap b = new Bootstrap();
            int maxFrame = 5 * 1024 * 1024;
            b.group(workerGroup)
                    .channel(NioDatagramChannel.class)
                    .localAddress(info.port)
                    .option(ChannelOption.SO_BROADCAST, true);
            b.option(ChannelOption.WRITE_BUFFER_WATER_MARK,
                    new WriteBufferWaterMark(0, 100));
            b.handler(
                    new ChannelInitializer<DatagramChannel>() {
                        @Override
                        protected void initChannel(DatagramChannel ch) throws Exception {

                            ch.pipeline()
                                    .addLast(new DatagramPacketToIrpMessageCodec())
                                    .addLast(new RequestCodec())
                                    .addLast(handler);
                        }
                    });
            LOGGER.info("UDP Listen at " + info.port);
            return b;
        }
    }

    static class TLSInitializer implements BootstrapInitializer<ServerBootstrap> {
        private final SslContext sslContext;
        TLSListenerInfo info;

        public TLSInitializer(TLSListenerInfo listenerInfo) {
            this.info = listenerInfo;
            sslContext = getSSLContext(listenerInfo.getChainKeyFile(), listenerInfo.getKeyFile());

        }

        protected SslContext getSSLContext(File chainFile, File keyFile) throws IllegalStateException {
            try {
                SslContext sslContext =
                        SslContextBuilder.forServer(chainFile, keyFile)
                                .ciphers(
                                        null,
                                        (ciphers, defaultCiphers, supportedCiphers) ->
                                                defaultCiphers.stream()
                                                        .filter(x -> null != x && !x.contains("RC4"))
                                                        .toArray(String[]::new))
                                .build();
                return sslContext;
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }

        }

        @Override
        public ServerBootstrap init(MessageHandler handler) {
            ServerBootstrap b = new ServerBootstrap();
            EventLoopGroup bossGroup = new NioEventLoopGroup(1);
            EventLoopGroup workerGroup = new NioEventLoopGroup();
            int maxFrame = 5 * 1024 * 1024;
            b.option(ChannelOption.WRITE_BUFFER_WATER_MARK,
                    new WriteBufferWaterMark(2 * maxFrame, 10 * maxFrame));
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(info.port);
            b.childHandler(
                    new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            SSLEngine engine = sslContext.newEngine(ch.alloc());
                            engine.setUseClientMode(false);
                            engine.setNeedClientAuth(false);
                            ch.pipeline()
                                    .addFirst("ssl", new SslHandler(engine))
                                    .addLast(new LengthFieldBasedFrameDecoder(maxFrame, 16, 4, 0, 0))
                                    .addLast(new IrpMessageCodec())
                                    .addLast(new RequestCodec())
                                    .addLast(handler);
                        }
                    });
            LOGGER.info("TLS Listen at " + info.port);
            return b;
        }
    }

    static class WSInitializer implements BootstrapInitializer<ServerBootstrap> {
        IrpListenerInfo info;

        public WSInitializer(IrpListenerInfo listenerInfo) {
            this.info = listenerInfo;
        }

        @Override
        public ServerBootstrap init(MessageHandler handler) {
            ServerBootstrap b = new ServerBootstrap();
            EventLoopGroup bossGroup = new NioEventLoopGroup(1);
            EventLoopGroup workerGroup = new NioEventLoopGroup();
            int maxFrame = 5 * 1024 * 1024;
            b.option(ChannelOption.WRITE_BUFFER_WATER_MARK,
                    new WriteBufferWaterMark(2 * maxFrame, 10 * maxFrame));
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(info.port);
            b.childHandler(
                    new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline()
                                    .addLast(new HttpServerCodec())
                                    .addLast(new HttpObjectAggregator(maxFrame))
                                    .addLast(new WebSocketServerProtocolHandler("/WS", null, true))
                                    .addLast(new WebSocketFrameToByteBufCodec())
                                    .addLast(new LengthFieldBasedFrameDecoder(maxFrame, 16, 4, 0, 0))
                                    .addLast(new IrpMessageCodec())
                                    .addLast(new RequestCodec())
                                    .addLast(handler);
                        }
                    });
            LOGGER.info("WS Listen at " + info.port);
            return b;
        }
    }


}
