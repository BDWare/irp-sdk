package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.*;

public class SessionSetupRequest extends MessageBody {
    short mode;
    int timeout;
    String identityIdentifier;
    int identityIndex;
    byte[] publicKey;

    public static SessionSetupRequest fromByteBuf(ByteBuf byteBuf) {
        SessionSetupRequest request = new SessionSetupRequest();
        request.mode = byteBuf.readShort();
        request.timeout = byteBuf.readInt();
        request.identityIdentifier = UTF8String.fromByteBuf(byteBuf);
        request.identityIndex = byteBuf.readInt();
        request.publicKey = ByteList.fromByteBuf(byteBuf);
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        byteBuf.writeShort(mode);
        byteBuf.writeInt(timeout);
        UTF8String.toByteBuf(identityIdentifier, byteBuf);
        byteBuf.writeInt(identityIndex);
        ByteList.toByteBuf(publicKey, byteBuf);
        return;
    }
    public Opcode getOpcode() {
        return Opcode.OC_SESSION_SETUP;
    }
}
