package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class DeleteIdentifierRequest extends MessageBody {
    String identifier;

    public DeleteIdentifierRequest(String identifier) {
        this.identifier = identifier;
    }

    public static DeleteIdentifierRequest fromByteBuf(ByteBuf byteBuf) {
        DeleteIdentifierRequest request = new DeleteIdentifierRequest(UTF8String.fromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(identifier, byteBuf);
    }

    public Opcode getOpcode() {
        return Opcode.OC_DELETE_ID;
    }
}
