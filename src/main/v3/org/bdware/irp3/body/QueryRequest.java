package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.*;

public class QueryRequest extends MessageBody {
    String identifier;
    int[] indexList;
    String[] typeList;

    public QueryRequest(String identifier, int[] indexList, String[] typeList) {
        super();
        this.identifier = identifier;
        this.indexList = indexList;
        this.typeList = typeList;
    }

    public QueryRequest() {
    }

    public QueryRequest setIdentifier(String identifier) {
        this.identifier = identifier;
        return this;
    }

    public QueryRequest setIndexList(int[] indexList) {
        this.indexList = indexList;
        return this;
    }

    public QueryRequest setIdentifier(String[] typeList) {
        this.typeList = typeList;
        return this;
    }

    public String getIdentifier() {
        return identifier;
    }

    public int[] getIndexList() {
        return indexList;
    }

    public String[] getTypeList() {
        return typeList;
    }

    public static QueryRequest fromByteBuf(ByteBuf byteBuf) {
        QueryRequest request = new QueryRequest();
        request.identifier = UTF8String.fromByteBuf(byteBuf);
        request.indexList = IntegerList.fromByteBuf(byteBuf);
        request.typeList = StringList.fromByteBuf(byteBuf);
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(identifier, byteBuf);
        IntegerList.toByteBuf(indexList, byteBuf);
        StringList.toByteBuf(typeList, byteBuf);
    }
    public Opcode getOpcode() {
        return Opcode.OC_RESOLUTION;
    }
}
