package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;

public class GeneralMessage extends MessageBody {

    public static GeneralMessage fromByteBuf(ByteBuf byteBuf) {
        GeneralMessage request = new GeneralMessage();
        request.toRead = byteBuf.copy();
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
    }
    public Opcode getOpcode() {
        return Opcode.OC_RESERVED;
    }
}
