package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.RequestDigest;

public class ClientAuthResponse extends MessageBody {
    private RequestDigest requestDigest;
    private int nonce;

    public static ClientAuthResponse fromByteBuf(ByteBuf byteBuf) {
        ClientAuthResponse response = new ClientAuthResponse();
        response.setRequestDigest(RequestDigest.fromByteBuf(byteBuf));
        response.setNonce(byteBuf.readInt());
        return response;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        getRequestDigest().toByteBuf(byteBuf);
        byteBuf.writeInt(getNonce());
    }
    public Opcode getOpcode() {
        return Opcode.OC_CHALLENGE_RESPONSE;
    }

    public RequestDigest getRequestDigest() {
        return requestDigest;
    }

    public void setRequestDigest(RequestDigest requestDigest) {
        this.requestDigest = requestDigest;
    }

    public int getNonce() {
        return nonce;
    }

    public void setNonce(int nonce) {
        this.nonce = nonce;
    }
}
