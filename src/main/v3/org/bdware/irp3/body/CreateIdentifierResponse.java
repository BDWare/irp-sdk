package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.RequestDigest;
import org.bdware.irp3.codec.UTF8String;

public class CreateIdentifierResponse extends MessageBody {
    private RequestDigest requestDigest;
    private String identifier;

    public static CreateIdentifierResponse fromByteBuf(ByteBuf byteBuf) {
        CreateIdentifierResponse request = new CreateIdentifierResponse();
        request.setRequestDigest(RequestDigest.fromByteBuf(byteBuf));
        request.setIdentifier(UTF8String.fromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        getRequestDigest().toByteBuf(byteBuf);
        UTF8String.toByteBuf(getIdentifier(), byteBuf);
    }
    public Opcode getOpcode() {
        return Opcode.OC_CREATE_ID;
    }

    public RequestDigest getRequestDigest() {
        return requestDigest;
    }

    public void setRequestDigest(RequestDigest requestDigest) {
        this.requestDigest = requestDigest;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
