package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.RequestDigest;

public class VerificationResponse extends MessageBody {
    RequestDigest requestDigest;
    int nonce;

    public VerificationResponse(RequestDigest digest, int nonce) {
        super();
        this.requestDigest = digest;
        this.nonce = nonce;
    }

    public static VerificationResponse fromByteBuf(ByteBuf byteBuf) {
        VerificationResponse response = new VerificationResponse(RequestDigest.fromByteBuf(byteBuf),
                byteBuf.readInt());
        return response;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        requestDigest.toByteBuf(byteBuf);
        byteBuf.writeInt(nonce);
    }

    public Opcode getOpcode() {
        return Opcode.OC_VERIFY_RESPONSE;
    }
}
