package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class ListIdentifiersRequest extends MessageBody {
    String prefixIdentifier;

    public ListIdentifiersRequest(String prefixIdentifier) {
        super();
        this.prefixIdentifier = prefixIdentifier;
    }

    public static ListIdentifiersRequest fromByteBuf(ByteBuf byteBuf) {
        ListIdentifiersRequest request = new ListIdentifiersRequest(UTF8String.fromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(prefixIdentifier, byteBuf);
    }

    public Opcode getOpcode() {
        return Opcode.OC_LIST_IDS;
    }
}
