package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.IntegerList;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class RemoveElementRequest extends MessageBody {
    String identifier;
    int[] elements;

    public RemoveElementRequest(String identifier, int[] elements) {
        super();
        this.identifier = identifier;
        this.elements = elements;
    }

    public static RemoveElementRequest fromByteBuf(ByteBuf byteBuf) {
        RemoveElementRequest request = new RemoveElementRequest(UTF8String.fromByteBuf(byteBuf), IntegerList.fromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(identifier, byteBuf);
        IntegerList.toByteBuf(elements, byteBuf);
    }

    public Opcode getOpcode() {
        return Opcode.OC_REMOVE_ELEMENT;
    }
}
