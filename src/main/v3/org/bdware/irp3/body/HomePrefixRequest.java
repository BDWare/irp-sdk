package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class HomePrefixRequest extends MessageBody {
    String prefixIdentifiers;

    public HomePrefixRequest(String prefixIdentifiers) {
        super();
        this.prefixIdentifiers = prefixIdentifiers;
    }

    public static HomePrefixRequest fromByteBuf(ByteBuf byteBuf) {
        HomePrefixRequest request = new HomePrefixRequest(UTF8String.fromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(prefixIdentifiers, byteBuf);
    }
    public Opcode getOpcode() {
        return Opcode.OC_HOME_PREFIX;
    }
}
