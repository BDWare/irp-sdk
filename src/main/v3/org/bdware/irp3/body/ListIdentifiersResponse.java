package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class ListIdentifiersResponse extends MessageBody {
    private String[] identifiers;

    public ListIdentifiersResponse(String[] identifiers) {
        this.identifiers = identifiers;
    }

    public static ListIdentifiersResponse fromByteBuf(ByteBuf byteBuf) {
        ListIdentifiersResponse request = new ListIdentifiersResponse(UTF8String.listFromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(identifiers, byteBuf);
    }

    public Opcode getOpcode() {
        return Opcode.OC_LIST_IDS;
    }

    public String[] getIdentifiers() {
        return identifiers;
    }
}
