package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class ListDerivedPrefixsRequest extends MessageBody {
    String prefixIdentifier;

    public ListDerivedPrefixsRequest(String prefixIdentifier) {
        super();
        this.prefixIdentifier = prefixIdentifier;
    }

    public static ListDerivedPrefixsRequest fromByteBuf(ByteBuf byteBuf) {
        ListDerivedPrefixsRequest request = new ListDerivedPrefixsRequest(UTF8String.fromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(prefixIdentifier, byteBuf);
    }

    public Opcode getOpcode() {
        return Opcode.OC_LIST_DERIVED_PREFIXES;
    }
}
