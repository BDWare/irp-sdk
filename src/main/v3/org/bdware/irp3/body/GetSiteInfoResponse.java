package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.Element;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.predefined.HS_Site;

public class GetSiteInfoResponse extends MessageBody {
    private HS_Site siteInfo;

    public GetSiteInfoResponse(HS_Site siteInfo) {
        this.siteInfo = siteInfo;
    }

    public static GetSiteInfoResponse fromByteBuf(ByteBuf byteBuf) {
        GetSiteInfoResponse request = new GetSiteInfoResponse(new HS_Site(Element.fromByteBuf(byteBuf)));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        siteInfo.toByteBuf(byteBuf);
        return;
    }

    public Opcode getOpcode() {
        return Opcode.OC_GET_SITE_INFO;
    }

    public HS_Site getSiteInfo() {
        return siteInfo;
    }
}
