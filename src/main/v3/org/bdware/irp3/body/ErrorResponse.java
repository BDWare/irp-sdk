package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.IntegerList;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.RequestDigest;
import org.bdware.irp3.codec.UTF8String;

public class ErrorResponse extends MessageBody {
    RequestDigest requestDigest;
    String errorMessage;
    int[] indexList;

    public ErrorResponse() {
    }

    public ErrorResponse(String errorMessage) {
        super();
        this.errorMessage = errorMessage;
    }

    public static ErrorResponse fromByteBuf(ByteBuf byteBuf) {
        ErrorResponse response = new ErrorResponse();
        response.errorMessage = UTF8String.fromByteBuf(byteBuf);
        response.indexList = IntegerList.fromByteBuf(byteBuf);
        return response;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(errorMessage, byteBuf);
        IntegerList.toByteBuf(indexList, byteBuf);
    }
    public Opcode getOpcode() {
        return Opcode.OC_RESERVED;
    }
}
