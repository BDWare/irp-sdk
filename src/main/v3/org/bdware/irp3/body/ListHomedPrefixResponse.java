package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class ListHomedPrefixResponse extends MessageBody {

    private String[] identifiers;

    public ListHomedPrefixResponse(String[] identifiers) {
        super();
        this.identifiers = identifiers;
    }

    public static ListHomedPrefixResponse fromByteBuf(ByteBuf byteBuf) {
        ListHomedPrefixResponse request = new ListHomedPrefixResponse(UTF8String.listFromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(identifiers, byteBuf);
    }

    public Opcode getOpcode() {
        return Opcode.OC_LIST_HOMED_PREFIXES;
    }

    public String[] getIdentifiers() {
        return identifiers;
    }
}
