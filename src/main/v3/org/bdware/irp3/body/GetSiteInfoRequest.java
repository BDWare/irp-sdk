package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class GetSiteInfoRequest extends MessageBody {
    private String ignoredIdentifier;

    public GetSiteInfoRequest(String ignoredIdentifier) {
        super();
        this.ignoredIdentifier = ignoredIdentifier;
    }

    public static GetSiteInfoRequest fromByteBuf(ByteBuf byteBuf) {
        GetSiteInfoRequest request = new GetSiteInfoRequest(UTF8String.fromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(ignoredIdentifier, byteBuf);
        return;
    }

    public Opcode getOpcode() {
        return Opcode.OC_GET_SITE_INFO;
    }

    public String getIgnoredIdentifier() {
        return ignoredIdentifier;
    }
}
