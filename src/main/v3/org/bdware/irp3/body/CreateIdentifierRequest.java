package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.Element;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class CreateIdentifierRequest extends MessageBody {
    String identifier;
    Element[] elements;

    public CreateIdentifierRequest(String identifier, Element[] elements) {
        super();
        this.identifier = identifier;
        this.elements = elements;
    }

    public static CreateIdentifierRequest fromByteBuf(ByteBuf byteBuf) {
        CreateIdentifierRequest request = new CreateIdentifierRequest(UTF8String.fromByteBuf(byteBuf), Element.listFromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(identifier, byteBuf);
        Element.toByteBuf(elements, byteBuf);
    }

    public Opcode getOpcode() {
        return Opcode.OC_CREATE_ID;
    }
}
