package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.Element;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.RequestDigest;
import org.bdware.irp3.codec.UTF8String;

public class ReferralResponse extends MessageBody {
    RequestDigest requestDigest;
    String referralIdentifier;
    Element[] elements;

    public static ReferralResponse fromByteBuf(ByteBuf byteBuf) {
        ReferralResponse response = new ReferralResponse();
        response.referralIdentifier = UTF8String.fromByteBuf(byteBuf);
        response.elements = Element.listFromByteBuf(byteBuf);
        return response;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(referralIdentifier, byteBuf);
        Element.toByteBuf(elements, byteBuf);
    }
    public Opcode getOpcode() {
        return Opcode.OC_RESERVED;
    }
}
