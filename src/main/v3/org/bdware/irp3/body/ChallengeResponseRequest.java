package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.ByteList;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class ChallengeResponseRequest extends MessageBody {
    private String authenticationType;
    private String keyIdentifier;
    private int keyIndex;
    private byte[] challengeResponse;

    public ChallengeResponseRequest(String authenticationType, String keyIdentifier, int keyIndex, byte[] challengeResponse) {
        super();
        this.authenticationType = authenticationType;
        this.keyIdentifier = keyIdentifier;
        this.keyIndex = keyIndex;
        this.challengeResponse = challengeResponse;
    }

    public static ChallengeResponseRequest fromByteBuf(ByteBuf byteBuf) {
        ChallengeResponseRequest request = new ChallengeResponseRequest(UTF8String.fromByteBuf(byteBuf),
                UTF8String.fromByteBuf(byteBuf), byteBuf.readInt(), ByteList.fromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(getAuthenticationType(), byteBuf);
        UTF8String.toByteBuf(getKeyIdentifier(), byteBuf);
        byteBuf.writeInt(getKeyIndex());
        ByteList.toByteBuf(getChallengeResponse(), byteBuf);
        return;
    }

    public Opcode getOpcode() {
        return Opcode.OC_CHALLENGE_RESPONSE;
    }

    public String getAuthenticationType() {
        return authenticationType;
    }

    public String getKeyIdentifier() {
        return keyIdentifier;
    }

    public int getKeyIndex() {
        return keyIndex;
    }

    public byte[] getChallengeResponse() {
        return challengeResponse;
    }
}
