package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.ByteList;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.RequestDigest;

public class SessionSetupResponse extends MessageBody {
    private RequestDigest requestDigest;
    private short mode;

    byte[] data;

    public SessionSetupResponse(RequestDigest digest, short mode, byte[] data) {
        this.requestDigest = digest;
        this.mode = mode;
        this.data = data;
    }

    public static SessionSetupResponse fromByteBuf(ByteBuf byteBuf) {
        SessionSetupResponse request = new SessionSetupResponse(RequestDigest.fromByteBuf(byteBuf),
                byteBuf.readShort(),ByteList.fromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        requestDigest.toByteBuf(byteBuf);
        byteBuf.writeShort(mode);
        ByteList.toByteBuf(data, byteBuf);
        return;
    }

    public Opcode getOpcode() {
        return Opcode.OC_SESSION_SETUP;
    }

    public RequestDigest getRequestDigest() {
        return requestDigest;
    }

    public short getMode() {
        return mode;
    }
}
