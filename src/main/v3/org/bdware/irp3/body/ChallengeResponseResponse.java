package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.RequestDigest;

public class ChallengeResponseResponse extends MessageBody {
    RequestDigest requestDigest;
    byte verificationResult;

    public ChallengeResponseResponse(RequestDigest digest, byte result) {
        this.requestDigest = digest;
        this.verificationResult = result;
    }

    public static ChallengeResponseResponse fromByteBuf(ByteBuf byteBuf) {
        ChallengeResponseResponse request = new ChallengeResponseResponse(RequestDigest.fromByteBuf(byteBuf),
                byteBuf.readByte());
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        requestDigest.toByteBuf(byteBuf);
        byteBuf.writeByte(verificationResult);
        return;
    }

    public Opcode getOpcode() {
        return Opcode.OC_CHALLENGE_RESPONSE;
    }
}
