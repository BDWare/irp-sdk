package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;


//No such request
public class ClientAuthRequest extends MessageBody {

    @Override
    public void toByteBuf(ByteBuf byteBuf) {

    }
    public Opcode getOpcode() {
        return Opcode.OC_RESERVED;
    }
}
