package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;

public class SessionTerminationRequest extends MessageBody {
    public static SessionTerminationRequest fromByteBuf(ByteBuf byteBuf) {
        return new SessionTerminationRequest();
    }
    @Override
    public void toByteBuf(ByteBuf byteBuf) {

    }
    public Opcode getOpcode() {
        return Opcode.OC_SESSION_TERMINATE;
    }
}
