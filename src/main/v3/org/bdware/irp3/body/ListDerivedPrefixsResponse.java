package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class ListDerivedPrefixsResponse extends MessageBody {
    String[] identifierList;

    public static ListDerivedPrefixsResponse fromByteBuf(ByteBuf byteBuf) {
        ListDerivedPrefixsResponse request = new ListDerivedPrefixsResponse();
        request.identifierList = UTF8String.listFromByteBuf(byteBuf);
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(identifierList, byteBuf);
    }
    public Opcode getOpcode() {
        return Opcode.OC_LIST_DERIVED_PREFIXES;
    }
}
