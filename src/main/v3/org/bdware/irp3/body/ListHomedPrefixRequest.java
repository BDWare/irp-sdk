package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.UTF8String;

public class ListHomedPrefixRequest extends MessageBody {
    String ignoredIdentifier;

    public ListHomedPrefixRequest(String ignoredIdentifier) {
        super();
        this.ignoredIdentifier = ignoredIdentifier;
    }

    public static ListHomedPrefixRequest fromByteBuf(ByteBuf byteBuf) {
        ListHomedPrefixRequest request = new ListHomedPrefixRequest(UTF8String.fromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(ignoredIdentifier, byteBuf);
    }

    public Opcode getOpcode() {
        return Opcode.OC_LIST_HOMED_PREFIXES;
    }
}
