package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.Element;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.RequestDigest;
import org.bdware.irp3.codec.UTF8String;

public class QueryResponse extends MessageBody {
    RequestDigest requestDigest;
    String identifier;
    Element[] elementList;

    public RequestDigest getRequestDigest() {
        return requestDigest;
    }

    public void setRequestDigest(RequestDigest requestDigest) {
        this.requestDigest = requestDigest;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Element[] getElementList() {
        return elementList;
    }

    public void setElementList(Element[] elementList) {
        this.elementList = elementList;
    }

    public QueryResponse() {

    }

    public static QueryResponse fromByteBuf(ByteBuf byteBuf) {
        QueryResponse response = new QueryResponse();
        response.identifier = UTF8String.fromByteBuf(byteBuf);
        response.elementList = Element.listFromByteBuf(byteBuf);
        return response;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(identifier, byteBuf);
        Element.toByteBuf(elementList, byteBuf);
    }

    public String getIdentifier() {
        return identifier;
    }
    public Opcode getOpcode() {
        return Opcode.OC_RESOLUTION;
    }
}
