package org.bdware.irp3.body;

import io.netty.buffer.ByteBuf;
import org.bdware.irp3.Opcode;
import org.bdware.irp3.codec.ByteList;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.RequestDigest;
import org.bdware.irp3.codec.UTF8String;

public class VerificationRequest extends MessageBody {
    private String keyIdentifier;
    private int keyIndex;
    private int nonce;
    private RequestDigest serverChallengeReqDigest;
    private byte[] challengeResponse;

    public VerificationRequest(String keyIdentifier, int keyIndex, int nonce, RequestDigest requestDigest, byte[] challengeResponse) {
        super();
        this.keyIdentifier = keyIdentifier;
        this.keyIndex = keyIndex;
        this.nonce = nonce;
        this.serverChallengeReqDigest = requestDigest;
        this.challengeResponse = challengeResponse;
    }

    public static VerificationRequest fromByteBuf(ByteBuf byteBuf) {
        VerificationRequest request = new VerificationRequest(UTF8String.fromByteBuf(byteBuf),
                byteBuf.readInt(), byteBuf.readInt(), RequestDigest.fromByteBuf(byteBuf),
                ByteList.fromByteBuf(byteBuf));
        return request;
    }

    @Override
    public void toByteBuf(ByteBuf byteBuf) {
        UTF8String.toByteBuf(keyIdentifier, byteBuf);
        byteBuf.writeInt(keyIndex);
        byteBuf.writeInt(nonce);
        serverChallengeReqDigest.toByteBuf(byteBuf);
        ByteList.toByteBuf(challengeResponse,byteBuf);
        return;
    }

    public Opcode getOpcode() {
        return Opcode.OC_VERIFY_RESPONSE;
    }

    public String getKeyIdentifier() {
        return keyIdentifier;
    }

    public int getKeyIndex() {
        return keyIndex;
    }

    public int getNonce() {
        return nonce;
    }

    public RequestDigest getServerChallengeReqDigest() {
        return serverChallengeReqDigest;
    }

    public byte[] getChallengeResponse() {
        return challengeResponse;
    }
}
