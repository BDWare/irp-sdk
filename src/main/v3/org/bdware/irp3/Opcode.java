package org.bdware.irp3;

import org.bdware.irp3.codec.MessageBody;

public enum Opcode {
    OC_RESERVED(0),
    OC_RESOLUTION(1),
    OC_GET_SITE_INFO(2),
    OC_CREATE_ID(100),
    OC_DELETE_ID(101),
    OC_ADD_ELEMENT(102),
    OC_REMOVE_ELEMENT(103),
    OC_MODIFY_ELEMENT(104),
    OC_LIST_IDS(105),
    OC_LIST_DERIVED_PREFIXES(106),
    OC_CHALLENGE_RESPONSE(200),
    OC_VERIFY_RESPONSE(201),
    OC_HOME_PREFIX(300),
    OC_UNHOME_PREFIX(301),
    OC_LIST_HOMED_PREFIXES(302),
    OC_SESSION_SETUP(400),
    OC_SESSION_TERMINATE(401);
    public int code;

    Opcode(int code) {
        this.code = code;
    }

    public static Opcode fromInt(int i) {
        switch (i) {
            case 0:
                return OC_RESERVED;
            case 1:
                return OC_RESOLUTION;
            case 2:
                return OC_GET_SITE_INFO;
            case 100:
                return OC_CREATE_ID;
            case 101:
                return OC_DELETE_ID;
            case 102:
                return OC_ADD_ELEMENT;
            case 103:
                return OC_REMOVE_ELEMENT;
            case 104:
                return OC_MODIFY_ELEMENT;
            case 105:
                return OC_LIST_IDS;
            case 106:
                return OC_LIST_DERIVED_PREFIXES;
            case 200:
                return OC_CHALLENGE_RESPONSE;
            case 201:
                return OC_VERIFY_RESPONSE;
            case 300:
                return OC_HOME_PREFIX;
            case 301:
                return OC_UNHOME_PREFIX;
            case 302:
                return OC_LIST_HOMED_PREFIXES;
            case 400:
                return OC_SESSION_SETUP;
            case 402:
                return OC_SESSION_TERMINATE;
        }
        return null;
    }
}
