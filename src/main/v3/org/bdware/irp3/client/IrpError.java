package org.bdware.irp3.client;

import org.bdware.irp3.body.ErrorResponse;

public class IrpError extends Exception {
    ErrorResponse response;

    public IrpError(ErrorResponse response) {
        this.response = response;

    }
}
