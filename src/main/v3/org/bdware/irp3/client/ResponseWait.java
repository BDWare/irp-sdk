/*
 *    Copyright (c) [2021] [Peking University]
 *    [BDWare DOIP SDK] is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *             http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package org.bdware.irp3.client;

import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.TimerTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp3.codec.Message;

import java.util.Map;
import java.util.concurrent.*;


public class ResponseWait<T extends Message> {
    static Logger LOGGER = LogManager.getLogger(ResponseWait.class);
    public static final HashedWheelTimer HASHED_WHEEL_TIMER = new HashedWheelTimer(
            new ThreadFactory() {
                @Override
                public Thread newThread(Runnable r) {
                    Thread t = new Thread(r);
                    t.setDaemon(true);
                    return t;
                }
            }, 5, TimeUnit.MILLISECONDS, 2);

    // use static map to ensure requestid is !UNIC!
    // in a client(With multiple connection)
    final Map<Integer, ResultCallback<T>> waitObj = new ConcurrentHashMap<>();
    private final TimoutMessageFactory<T> factory;

    public ResponseWait(TimoutMessageFactory<T> factory) {
        this.factory = factory;
    }

    public void wakeUpAndRemove(int requestID, T result) {
        ResultCallback ob = getAndRemove(requestID);
        wakeup(requestID, ob, result);
    }

    //DO NOT use synchronized like "private synchronized ..."
    //Because the waitObj is static
    private ResultCallback<T> getAndRemove(int requestID) {
        synchronized (waitObj) {
            ResultCallback ob = waitObj.get(requestID);
            waitObj.remove(requestID);
            return ob;
        }
    }

    public boolean waitResponse(final int requestID, ResultCallback<T> cb) {
        return waitResponse(requestID, cb, 5);
    }

    public boolean waitResponse(int requestID, ResultCallback<T> cb, int seconds) {
        if (waitObj.size() > 1000) {
            return false;
        }
        synchronized (waitObj) {
            if (!waitObj.containsKey(requestID)) {
                waitObj.put(requestID, cb);
                TimerTask tt = new TimerTask() {
                    @Override
                    public void run(Timeout timeout) throws Exception {
                        T timeOutMessage = factory.creatTimeOutResponse(requestID);
                        wakeUpAndRemove(requestID, timeOutMessage);
                    }
                };
                HASHED_WHEEL_TIMER.newTimeout(tt, seconds, TimeUnit.SECONDS);
                return true;
            }
            LOGGER.info("Return false, try again!");
            return false;
        }
    }

    //TODO just support for stream??
    // schedule timeout cleaner?
    public void wakeup(int requestID, T result) {
        ResultCallback<T> ob = waitObj.get(requestID);
        wakeup(requestID, ob, result);
    }

    public void wakeup(int requestID, ResultCallback<T> ob, T result) {
        if (ob == null) {
            //  LOGGER.info("cancel the timeout task!!" + requestID + ", please Check server status??");
            //  @TODO actually we can stop the timeout runner.
        }
        if (ob != null) ob.onResult(result);
    }
}
