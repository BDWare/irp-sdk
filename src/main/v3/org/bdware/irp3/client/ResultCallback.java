package org.bdware.irp3.client;

import org.bdware.irp3.codec.IrpMessage;
import org.bdware.irp3.codec.Message;

public interface ResultCallback<T extends Message> {
    void onResult(T message);
}
