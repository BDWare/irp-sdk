package org.bdware.irp3.client;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp3.body.ClientAuthResponse;
import org.bdware.irp3.codec.MessageBody;

import java.util.Random;

public class ClientHandler extends SimpleChannelInboundHandler<MessageBody> {
    public Channel channel;
    static Logger LOGGER = LogManager.getLogger(ClientHandler.class);
    ResponseWait<MessageBody> sync = new ResponseWait<MessageBody>(new IrpMessageBodyTimeoutCreator());
    Random random = new Random();
    private IrpClient client;

    public void setChannel(Channel channel) {
        this.channel = channel;
    }


    public void sendMessage(MessageBody request, ResultCallback<MessageBody> callback, int timeoutInSeconds) {
        if (callback == null) {
            LOGGER.error("ResultCallback is null, please check!");
        }
        if (request.getRequestID() == 0) {
            request.setRequestID(random.nextInt());
            while (!sync.waitResponse(request.getRequestID(), callback, timeoutInSeconds)) {
                request.setRequestID(random.nextInt());
            }
        } else {
            if (!sync.waitResponse(request.getRequestID(), callback, timeoutInSeconds)) {
                LOGGER.debug("duplicate requestID, please check");
            }
        }
        channel.writeAndFlush(request);
    }


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageBody msg) throws Exception {
        LOGGER.info("ClientHandler Recv：" + msg.toString());
        if (msg instanceof ClientAuthResponse) {
            client.onChallengeRequest((ClientAuthResponse) msg);
        } else {
            sync.wakeUpAndRemove(msg.getRequestID(), msg);
        }
    }

    public void setClient(IrpClient irpClient) {
        this.client = irpClient;
    }
}
