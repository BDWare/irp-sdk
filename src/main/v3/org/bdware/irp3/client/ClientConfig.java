package org.bdware.irp3.client;

import org.bdware.irp.irplib.core.IrpMessageSigner;

import java.net.URI;

public class ClientConfig {
    public URI uri;

    public ClientConfig(URI uri) {
        this.uri = uri;
    }

    public String protocolVersion;
    public int timeoutSeconds = 5;
    private boolean debugPrint;
    private transient IrpMessageSigner signer;
}
