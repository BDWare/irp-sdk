package org.bdware.irp3.client;

import io.netty.bootstrap.Bootstrap;

public interface ClientBootstrapInitializer {
    public void init(Bootstrap bootstrap, ClientHandler handler);
}
