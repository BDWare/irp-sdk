package org.bdware.irp3.client;

import org.bdware.irp3.codec.Message;

public interface TimoutMessageFactory<T extends Message> {
    T creatTimeOutResponse(int requestID);
}
