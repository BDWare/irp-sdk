package org.bdware.irp3.client;

import org.bdware.irp3.body.*;
import org.bdware.irp3.codec.Element;
import org.bdware.irp3.codec.MessageBody;

import java.net.URISyntaxException;

public class IrpClient {
    ClientChannel channel;

    public IrpClient(ClientConfig config) throws URISyntaxException {
        channel = new ClientChannel(config);
        channel.connect(config.uri, this);
    }

    //---------QueryHandler------
    public QueryResponse query(String identifier, int[] indexList, String[] typeList) throws IrpError {
        QueryRequest request = new QueryRequest(identifier, indexList, typeList);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        QueryResponse queryResponse = (QueryResponse) response;
        return queryResponse;
    }

    //---------AdministrationHandler------
    public GeneralMessage addElement(String identifier, Element[] elements) throws IrpError {
        AddElementRequest request = new AddElementRequest(identifier, elements);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        GeneralMessage generalResponse = (GeneralMessage) response;
        return generalResponse;
    }

    public GeneralMessage removeElements(String identifier, int[] indexs) throws IrpError {
        RemoveElementRequest request = new RemoveElementRequest(identifier, indexs);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        GeneralMessage generalResponse = (GeneralMessage) response;
        return generalResponse;
    }

    public GeneralMessage modifyElements(String identifier, Element[] elements) throws IrpError {
        ModifyElementRequest request = new ModifyElementRequest(identifier, elements);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        GeneralMessage generalResponse = (GeneralMessage) response;
        return generalResponse;
    }

    public CreateIdentifierResponse createIdentifier(String identifier, Element[] elements) throws IrpError {
        CreateIdentifierRequest request = new CreateIdentifierRequest(identifier, elements);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        CreateIdentifierResponse createIdentifierResponse = (CreateIdentifierResponse) response;
        return createIdentifierResponse;
    }

    public GeneralMessage deleteIdentifier(String identifier) throws IrpError {
        DeleteIdentifierRequest request = new DeleteIdentifierRequest(identifier);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        GeneralMessage generalResponse = (GeneralMessage) response;
        return generalResponse;
    }

    //---------ClientAuthenticationHandler------
    protected void onChallengeRequest(ClientAuthResponse clientAuthResponse) {
        //TODO send ChallengeResponseRequest msg to server
        // by invoking clientAuth
    }
    public ChallengeResponseResponse clientAuth(ChallengeResponseRequest request) throws IrpError {
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        ChallengeResponseResponse generalResponse = (ChallengeResponseResponse) response;
        return generalResponse;
    }

    //this method only used in server that send verify request to the key servers
    public VerificationResponse verifyRequest(VerificationRequest request) throws IrpError {
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        VerificationResponse generalResponse = (VerificationResponse) response;
        return generalResponse;
    }

    //---------GetSiteInfoHandler------
    public GetSiteInfoResponse getSiteInfo(String ignoredIdentifier) throws IrpError {
        GetSiteInfoRequest request = new GetSiteInfoRequest(ignoredIdentifier);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        GetSiteInfoResponse generalResponse = (GetSiteInfoResponse) response;
        return generalResponse;
    }

    //---------HomeManagementHandler------
    public GeneralMessage homePrefix(String prefixIdentifiers) throws IrpError {
        HomePrefixRequest request = new HomePrefixRequest(prefixIdentifiers);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        GeneralMessage generalResponse = (GeneralMessage) response;
        return generalResponse;
    }

    public GeneralMessage unhomePrefix(String prefixIdentifiers) throws IrpError {
        UnhomePrefixRequest request = new UnhomePrefixRequest(prefixIdentifiers);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        GeneralMessage generalResponse = (GeneralMessage) response;
        return generalResponse;
    }

    public ListHomedPrefixResponse listHomePrefix(String ignoredIdentifier) throws IrpError {
        ListHomedPrefixRequest request = new ListHomedPrefixRequest(ignoredIdentifier);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        ListHomedPrefixResponse generalResponse = (ListHomedPrefixResponse) response;
        return generalResponse;
    }

    //---------PrefixAdminHandler------
    public ListIdentifiersResponse listIdentifier(String prefixIdentifier) throws IrpError {
        ListIdentifiersRequest request = new ListIdentifiersRequest(prefixIdentifier);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        ListIdentifiersResponse generalResponse = (ListIdentifiersResponse) response;
        return generalResponse;
    }

    public ListIdentifiersResponse listDerivedPrefixes(String prefixIdentifier) throws IrpError {
        ListDerivedPrefixsRequest request = new ListDerivedPrefixsRequest(prefixIdentifier);
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        ListIdentifiersResponse generalResponse = (ListIdentifiersResponse) response;
        return generalResponse;
    }

    //---------SessionHandler------
    public SessionSetupResponse setupSession(SessionSetupRequest request) throws IrpError {
        MessageBody response = channel.sendMessageSync(request);
        if (response instanceof ErrorResponse) {
            throw new IrpError((ErrorResponse) response);
        }
        SessionSetupResponse generalResponse = (SessionSetupResponse) response;
        return generalResponse;
    }

    public void terminateSession() {
        SessionTerminationRequest queryRequest = new SessionTerminationRequest();
        channel.sendMessage(queryRequest, new ResultCallback<MessageBody>() {
            @Override
            public void onResult(MessageBody message) {
                //always timeout/error?
            }
        });
        //channel.disconnect();
    }

}
