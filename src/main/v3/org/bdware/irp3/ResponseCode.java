package org.bdware.irp3;

public enum ResponseCode {
    RC_RESERVED(0),
    RC_SUCCESS(1),
    RC_ERRORGeneral(2),
    RC_SERVER_BUSY(3),
    RC_PROTOCOL_ERROR(4),
    RC_OPERATION_DENIED(5),
    RC_RECUR_LIMIT_EXCEEDED(6),
    RC_SERVER_BACKUP(7),
    RC_ID_NOT_FOUND(100),
    RC_ID_ALREADY_EXIST(101),
    RC_INVALID_ID(102),
    RC_ELEMENT_NOT_FOUND(200),
    RC_ELEMENT(201),
    RC_ELEMENT_INVALID(202),
    RC_EXPIRED_SITE_INFO(300),
    RC_SERVER_NOT_RESP(301),
    RC_SERVICE_REFERRAL(302),
    RC_PREFIX_REFERRAL(303),
    RC_INVALID_ADMIN(400),
    RC_ACCESS_DENIED(401),
    RC_AUTHEN_NEEDED(402),
    RC_AUTHEN_FAILED(403),
    RC_INVALID_CREDENTIAL(404),
    RC_AUTHEN_TIMEOUT(405),
    RC_UNABLE_TO_AUTHEN(406),
    RC_SESSION_TIMEOUT(500),
    RC_SESSION_FAILED(501),
    RC_SESSION_KEY_INVALID(502),
    RC_SESSION_MSG_REJECTED(505),
    RC_UNKNOWN(999);


    public int code;

    ResponseCode(int code) {
        this.code = code;
    }

    public static ResponseCode fromInt(int responseCode) {
        switch (responseCode) {
            case 0:
                return RC_RESERVED;
            case 1:
                return RC_SUCCESS;
            case 2:
                return RC_ERRORGeneral;
            case 3:
                return RC_SERVER_BUSY;
            case 4:
                return RC_PROTOCOL_ERROR;
            case 5:
                return RC_OPERATION_DENIED;
            case 6:
                return RC_RECUR_LIMIT_EXCEEDED;
            case 7:
                return RC_SERVER_BACKUP;
            case 100:
                return RC_ID_NOT_FOUND;
            case 101:
                return RC_ID_ALREADY_EXIST;
            case 102:
                return RC_INVALID_ID;
            case 200:
                return RC_ELEMENT_NOT_FOUND;
            case 201:
                return RC_ELEMENT;
            case 202:
                return RC_ELEMENT_INVALID;
            case 300:
                return RC_EXPIRED_SITE_INFO;
            case 301:
                return RC_SERVER_NOT_RESP;
            case 302:
                return RC_SERVICE_REFERRAL;
            case 303:
                return RC_PREFIX_REFERRAL;
            case 400:
                return RC_INVALID_ADMIN;
            case 401:
                return RC_ACCESS_DENIED;
            case 402:
                return RC_AUTHEN_NEEDED;
            case 403:
                return RC_AUTHEN_FAILED;
            case 404:
                return RC_INVALID_CREDENTIAL;
            case 405:
                return RC_AUTHEN_TIMEOUT;
            case 406:
                return RC_UNABLE_TO_AUTHEN;
            case 500:
                return RC_SESSION_TIMEOUT;
            case 501:
                return RC_SESSION_FAILED;
            case 502:
                return RC_SESSION_KEY_INVALID;
            case 505:
                return RC_SESSION_MSG_REJECTED;
            default:
                return RC_UNKNOWN;
        }
    }
}
