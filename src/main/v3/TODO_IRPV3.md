# Client实现情况

| Handler                     | 接口数量 | 已实现 | 测试用例 |
|-----------------------------|------|-----|------|
| AdministrationHandler       | 5 | 5   | 5    |
| ClientAuthenticationHandler | 3 | 3   | 3    |
| GetSiteInfoHandler          | 1 | 1   | 1    |
| HomeManagementHandler       | 3 | 3   | 3    |
| PrefixListHandler           | 2 | 2   | 2    |
| QueryHandler                | 1 | 1   | 1    |
| SessionHandler              | 2 | 2   | 2    |

进度：16/16

# 预定义元素实现情况

| 序号 | 元素 | 是否实现 |
|----|------------|------|
| 1  | AdminPermissions | 1    |
| 2  | Attribute | 1    |
| 3  | ElementPermissions | 1    |
| 4  | HS_Admin | 1    |
| 5  | HS_Alias | 1    |
| 6  | HS_Cert | 1    |
| 7  | HS_Pubkey | 1    |
| 8  | HS_Seckey | 1    |
| 9  | HS_Serv | 1    |
| 10 | HS_Signature | 1    |
| 11 | HS_Site | 1    |
| 12 | HS_Vlist | 1    |
| 13 | ServerRecord | 1    |
| 14 | ServiceInterface | 1    |

进度：14/14

# 传输协议实现情况

| 序号  | 协议 | 是否实现 | 测试用例 |
|-----|----|------|------|
| 1   | TCP | 1    | 1    |
| 2   | UDP | 1    | 1    |
| 3   | TLS | 1    | 0    |
| 4   | WS | 1    | 0 |
| 5   | HTTP | 0    | 0 |

进度: 4/5

# 与已有的 IRPLib 兼容

进度：0/1

# 标识解析系统实现情况

| Handler | 接口数量 | 已实现  |
|-----------------------|------|------|
| AdministrationHandler | 5 | 0    |
| ClientAuthenticationHandler | 2 | 0    |
| GetSiteInfoHandler | 1 | 0    |
| HomeManagementHandler | 3 | 0    |
| PrefixListHandler | 2 | 0    |
| QueryHandler | 1 | 0    |
| SessionHandler | 2 | 0    |

进度：0/16