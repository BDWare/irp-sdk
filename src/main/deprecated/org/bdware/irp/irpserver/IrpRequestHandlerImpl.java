package org.bdware.irp.irpserver;

import org.apache.log4j.Logger;
import org.bdware.irp.irplib.core.IrpMessage;
import org.bdware.irp.irplib.core.IrpMessageCode;
import org.bdware.irp.irplib.core.IrpResponse;

public class IrpRequestHandlerImpl implements IrpRequestHandler{
    static Logger logger = Logger.getLogger(NettyIrpListener.class);
    IrsHandler irsHandler;
    GrsHandler grsHandler;
    RouterHandler routerHandler;

    public IrpRequestHandlerImpl(IrsHandler irsHandler){
        this.irsHandler = irsHandler;
    }

    public IrpRequestHandlerImpl(GrsHandler grsHandler){
        this.grsHandler = grsHandler;
    }

    public IrpRequestHandlerImpl(RouterHandler routerHandler){
        this.routerHandler = routerHandler;
    }

    @Override
    public IrpMessage onRequest(IrpMessage msg) {
        int opCode = msg.header.opCode;
        IrpMessage res;
        logger.debug("[Call operation] name: " + opCode);
        switch (opCode) {
            case IrpMessageCode.OC_RESOLUTION_DOID:
                res = irsHandler.doidResolve(msg);
                break;
            case IrpMessageCode.OC_CREATE_DOID:
                res = irsHandler.createDOID(msg);
                break;
            case IrpMessageCode.OC_DELETE_DOID:
                res = irsHandler.deleteDOID(msg);
                break;
            case IrpMessageCode.OC_UPDATE_DOID:
                res = irsHandler.updateDOID(msg);
                break;
            case IrpMessageCode.OC_BATCH_CREATE_DOID:
                res = irsHandler.batchCreateDOID(msg);
                break;
            case IrpMessageCode.OC_VERIFY_IRS:
                res = grsHandler.verifyIRS(msg);
                break;
            case IrpMessageCode.OC_CREATE_ORG_GRS:
                res = grsHandler.createOrg(msg);
                break;
            case IrpMessageCode.OC_RESOLVE_GRS:
                res = grsHandler.doidResolve(msg);
                break;
            case IrpMessageCode.OC_VERIFY_ROUTER:
                res = routerHandler.verifyChildRouter(msg);
                break;
            case IrpMessageCode.OC_RESOLVE_ROUTER:
                res = routerHandler.resolve(msg);
                break;
            default:
                logger.debug("Unknown opcode to handler");
                res = IrpResponse.newErrorResponse(opCode, IrpMessageCode.RC_ERROR, "Unknown opcode to handler");
        }
        return res;
    }
}
