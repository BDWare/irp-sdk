package org.bdware.irp.irpserver;

import org.bdware.irp.irplib.core.IrpMessage;

public interface IrpRequestHandler {
    IrpMessage onRequest(IrpMessage msg);
}
