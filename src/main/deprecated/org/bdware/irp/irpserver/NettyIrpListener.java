package org.bdware.irp.irpserver;

import org.bdware.irp.irplib.util.IrpCommon;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

public abstract class NettyIrpListener implements IrpListener{

    static Logger logger = Logger.getLogger(NettyIrpListener.class);
    NettyServerHandler handler;

    public static NettyIrpListener CreateIrpListener(IrpListenerInfo listenerInfo) {
        try {
            switch (listenerInfo.protocolType) {
                case IrpCommon.IRP_SERVER_LISTENER_TCP:
                    return new NettyIrpListenerTCP(listenerInfo);
                case IrpCommon.IRP_SERVER_LISTENER_UDP:
                    //return new NettyIrpListenerUDP(listenerInfo);
                default:
                    logger.error("[Create irp server error] Unsupported Listener: " + new Gson().toJson(listenerInfo));
                    return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void setRequestHandler(IrpRequestHandler handler) {
        this.handler = new NettyServerHandler(handler);
    }

    public abstract void start();
    public abstract void stop();

}
