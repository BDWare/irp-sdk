package org.bdware.irp.irpserver;

import org.bdware.irp.irplib.util.IrpCommon;

public class IrpListenerInfo {
    public String host;
    public int port;
    public String protocolType;

    public IrpListenerInfo(String protocolType){
        this.host = IrpCommon.DEAULT_HOST_ADDRESS;
        this.port = IrpCommon.DEAULT_HOST_PORT;
        this.protocolType = protocolType;
    }

    public IrpListenerInfo(int port, String protocolType){
        this.host = IrpCommon.DEAULT_HOST_ADDRESS;
        this.port = port;
        this.protocolType = protocolType;
    }

    public IrpListenerInfo(String host, int port, String protocolType){
        this.host = host;
        this.port = port;
        this.protocolType = protocolType;
    }
}
