package org.bdware.irp.irpserver;

import org.bdware.irp.irplib.core.IrpMessage;

//Grs服务接口，支持标识解析等
public interface IrsHandler {

    IrpMessage doidResolve(IrpMessage request);

    IrpMessage createDOID(IrpMessage request);

    IrpMessage batchCreateDOID(IrpMessage request);

    IrpMessage updateDOID(IrpMessage request);

    IrpMessage deleteDOID(IrpMessage request);
}
