package org.bdware.irp.irpserver;

import com.google.gson.Gson;
import java.util.List;

public class IrpServerInfo {
    public String serverId;
    public String serverDescription;
    public String ipAddress;
    public int port;
    public String admin;
    public String publicKey;

    public String protocol;
    public String protocolVersion;
    public List<IrpListenerInfo> listenerInfos;

    public IrpServerInfo(int port, List<IrpListenerInfo> listenerInfos){
        this.serverId = "1";
        this.serverDescription = "test";
        this.admin = "admin";
        this.listenerInfos = listenerInfos;
        this.ipAddress = "localhost";
        this.port = port;
    }

    public IrpServerInfo(String serverId, String serverDes, String admin, String ipAddress, int port, List<IrpListenerInfo> listenerInfos){
        this.serverId = serverId;
        this.serverDescription = serverDes;
        this.admin = admin;
        this.listenerInfos = listenerInfos;
        this.ipAddress = ipAddress;
        this.port = port;
        //this.publicKey = GlobalCertifications.getGlobalJWK().toPublicJWK().toString();
    }

    public String toJson(){
        return new Gson().toJson(this);
    }

    public byte[] toBytes(){
        return this.toJson().getBytes();
    }

}
