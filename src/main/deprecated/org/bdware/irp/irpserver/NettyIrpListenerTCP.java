package org.bdware.irp.irpserver;

import org.bdware.irp.irplib.util.IrpCommon;
import org.bdware.irp.irplib.crypto.NettyTCPCodeC;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import org.apache.log4j.Logger;

public class NettyIrpListenerTCP extends NettyIrpListener {

    private final int port;
    static Logger logger = Logger.getLogger(NettyIrpListenerTCP.class);
    private Channel ch;

    protected NettyIrpListenerTCP(IrpListenerInfo listenerInfo){
        this.port = listenerInfo.port;
    }

    @Override
    public void start() {
        if(handler == null){
            logger.error("Handler not init yet! set handler first");
            return;
        }
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(port);
            b.childHandler(
                    new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline()
                                    .addLast(new LengthFieldBasedFrameDecoder(IrpCommon.MAX_MESSAGE_PACKET_LENGTH,16,4,0,0))
                                    .addLast(new NettyTCPCodeC())
                                    .addLast(handler);
                        }
                    });

            ch = b.bind().syncUninterruptibly().channel();
            logger.info("TCP DOIP listener start at:" + port);
            ch.closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    @Override
    public void stop() {
        if(ch != null)
            ch.close();
    }

}
