package org.bdware.irp.irpserver;

import org.bdware.irp.irplib.core.IrpMessage;

//Grs服务接口，支持irs节点验证、标识解析、机构信息管理
public interface GrsHandler {
    IrpMessage verifyIRS(IrpMessage request);
    IrpMessage doidResolve(IrpMessage request);
    IrpMessage createOrg(IrpMessage request);
    IrpMessage updateOrg(IrpMessage request);
    IrpMessage deleteOrg(IrpMessage request);
}
