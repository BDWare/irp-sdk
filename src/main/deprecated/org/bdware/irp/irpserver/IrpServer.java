package org.bdware.irp.irpserver;

public interface IrpServer {
    void start();
    void stop();
    void setIrsServerHandler(IrsHandler handler);
    void setGrsServerHandler(GrsHandler handler);
    void setRouterServerHandler(RouterHandler handler);
}
