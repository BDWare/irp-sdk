package org.bdware.irp.stateinfo;

public class DoStateInfo extends StateInfoBase {

    public DoStateInfo(String owner, String repository){
        super();
        setOwner(owner);
        setRepository(repository);
    }

    public DoStateInfo(StateInfoBase hrBase){
        identifier = hrBase.identifier;
        handleValues = hrBase.handleValues;
    }

    public static DoStateInfo fromStateInfoBase(StateInfoBase base){
        return new DoStateInfo(base);
    }

    public String getOwner(){
        return getValues("owner");
    }
    public String getRepository(){
        return getValues("repository");
    }

    public void setOwner(String owner){
        handleValues.addProperty("owner",owner);
    }
    public void setRepository(String repository){
        handleValues.addProperty("repository",repository);
    }

}
