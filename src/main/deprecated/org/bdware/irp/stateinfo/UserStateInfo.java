package org.bdware.irp.stateinfo;

public class UserStateInfo extends StateInfoBase {

    public UserStateInfo(String pubkey, String desc){
        super();
        handleValues.addProperty("pubkey",pubkey);
        handleValues.addProperty("desc",desc);
    }

    public UserStateInfo(StateInfoBase hrBase){
        identifier = hrBase.identifier;
        handleValues = hrBase.handleValues;
    }

    public static UserStateInfo fromStateInfoBase(StateInfoBase hrBase){
        return new UserStateInfo(hrBase);
    }

    public void setPubkey(String pubkey){
        handleValues.addProperty("pubkey",pubkey);
    }

    public void setDesc(String desc){
        handleValues.addProperty("desc",desc);
    }

    public String getPubkey(){
        return getValues("pubkey");
    }

    public String getDesc(){
        return getValues("desc");
    }
}
