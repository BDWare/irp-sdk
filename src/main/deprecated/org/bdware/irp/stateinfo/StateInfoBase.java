package org.bdware.irp.stateinfo;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.UUID;

public class StateInfoBase {
    public String identifier;
    public JsonObject handleValues;
    static final String UUID_KEY = "UUID";

    public StateInfoBase(){
        handleValues = new JsonObject();
    }


    public StateInfoBase(JsonObject handleValues){
        if(handleValues == null)
            this.handleValues = new JsonObject();
        else
            this.handleValues = handleValues;
    }

    public StateInfoBase(String identifier, JsonObject handleValues){
        this.identifier = identifier;
        if(handleValues == null)
            this.handleValues = new JsonObject();
        else
            this.handleValues = handleValues;
    }

//    public <T extends HandleRecordBase> T toChildClass(Class<T> classOfT){
////        T hr = new T();
//        return classOfT.cast(this);
//    }

    public UUID getUUID(){
        return UUID.fromString(getValues(UUID_KEY));
    }

    public void setUUID(UUID uuid){
        this.handleValues.addProperty(UUID_KEY,uuid.toString());
    }

    public static StateInfoBase fromJson(String json){
        return new Gson().fromJson(json, StateInfoBase.class);
    }

    public String getIdentifier(){
        return identifier;
    }

    public JsonObject getHandleValues() {
        return handleValues;
    }

    public void setHandleValues(JsonObject handleValues) {
        this.handleValues = handleValues;
    }

    public void setIdentifier(String identifier){
        this.identifier = identifier;
    }

    public String getValues(String key){
        return handleValues.has(key)?handleValues.get(key).getAsString():null;
    }

}
