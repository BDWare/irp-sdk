package org.bdware.irp.irplib.core;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.bdware.irp.irplib.util.GlobalUtils;
public class IrpForRouterRequest extends IrpMessage {
    private static final Logger LOGGER = LogManager.getLogger(IrpForRouterRequest.class);
    public String pubkey = null;
    public String name = null;
    public String signInfo = null;

    public byte[] doid;

    /**
     * Creates an router irp request.
     */
    public IrpForRouterRequest(int opCode) {
        super(opCode, IrpMessageCode.RC_RESERVED);
    }

    public static IrpForRouterRequest newVerifyRouterAuthRequest(String pubkey, String name, String signInfo) {
        IrpForRouterRequest req = new IrpForRouterRequest(IrpMessageCode.OC_VERIFY_ROUTER);
        req.pubkey = pubkey;
        req.name = name;
        req.signInfo = signInfo;
        LOGGER.info("req.pubkey:" + req.pubkey);
        LOGGER.info("req.name:" + req.name);
        LOGGER.info("req.signInfo:" + req.signInfo);
        return req;
    }

    public static IrpForRouterRequest newRouterResolveRequest(String doid) {
        IrpForRouterRequest req = new IrpForRouterRequest(IrpMessageCode.OC_RESOLVE_ROUTER);
        req.doid = GlobalUtils.encodeString(doid);
        return req;
    }

    public String getDoid() {
        return GlobalUtils.decodeString(this.doid);
    }
}
