package org.bdware.irp.irplib.core;

public class IrpForRouterResponse extends IrpResponse{
    public String prefix;
    public int port;
    public String routerInfo;
    public String result = "success";
    /**
     * Creates an IrpResponse.
     */
    public IrpForRouterResponse(int opCode, int resCode) {
        super(opCode, resCode);
    }

    public static IrpForRouterResponse newVerifyRouterAuthResponse(String prefix, int port, String routerInfo) {
        IrpForRouterResponse res = new IrpForRouterResponse(IrpMessageCode.OC_VERIFY_ROUTER, IrpMessageCode.RC_SUCCESS);
        res.prefix = prefix;
        res.port = port;
        res.routerInfo = routerInfo;
        return res;
    }
}
