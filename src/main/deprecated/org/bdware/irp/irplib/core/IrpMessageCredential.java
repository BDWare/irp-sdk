package org.bdware.irp.irplib.core;

import org.bdware.irp.irplib.util.IrpCommon;
/**
 *
 * Description:
 * <p>
 *  Message Credential implementation
 * </p>
 * Time:2021-07<br>
 *
 * @author：pku<br>
 * @version:$Revision: 1.0 $<br>
 * @since 1.0
 */
public class IrpMessageCredential {
    public int credentialLength;
    public byte version;
    public byte reserved;   //An octet that must be set to zero
    public short options;   //reserved for various cryptography options, unused

    public byte[] handle;   //handle system auth info, unused
    public int handleIdx;   //handle system auth info, unused

    public byte[] signerDoid;   //used for bdware irs auth

    public byte[] signedInfoType;
    public int signedInfoLength;
    public byte[] signedInfoDigestAlgorithm;
    public byte[] signature;

    /**
     * IrpMessageCredential Constructor
     */
    public IrpMessageCredential(){
        this.credentialLength = 0;
        this.version = IrpCommon.DEFAULT_CREDENTIAL_VERSION;
        this.reserved = 0;
        this.options = 0;
        this.signedInfoType = IrpCommon.DEFAULT_CREDENTIAL_SIGNEDINFO_TYPE;
        this.signedInfoLength = 0;
        this.signedInfoDigestAlgorithm = IrpCommon.DEFAULT_CREDENTIAL_SIGNEDINFO_DIGEST_ALG;
        this.signature = null;
    }

}
