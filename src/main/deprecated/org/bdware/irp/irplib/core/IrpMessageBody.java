package org.bdware.irp.irplib.core;

import org.bdware.irp.irplib.util.IrpCommon;
/**
 *
 * Description:
 * <p>
 *  Message Body implementation
 * </p>
 * Time:2021-07<br>
 *
 * @author: pku<br>
 * @version: $Revision: 1.0 $<br>
 * @since 1.0
 */
public class IrpMessageBody {
    public byte[] requestDigest;
    public byte[] messageBody;
    public byte digestType;

    /**
     * IrpMessageBody Constructor
     */
    public IrpMessageBody(){
        this.requestDigest = null;
        this.messageBody = null;
        this.digestType = IrpCommon.DIGEST_TYPE_SHA1;
    }

    public int getMessageBodyLength(){
        return requestDigest.length + messageBody.length;
    }

}
