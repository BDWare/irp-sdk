package org.bdware.irp.irplib.core;

/**
 *
 * Description:
 * <p>
 *  All of the standard identifiers for the irp message code
 * </p>
 * Time:2021-07<br>
 *
 * @author：pku<br>
 * @version:$Revision: 1.0 $<br>
 * @since 1.0
 */
public class IrpMessageCode {
    //opCode list(Message Header)
    public static final int OC_RESERVED = 0;
    public static final int OC_RESOLUTION = 1;
    public static final int OC_GET_SITEINFO = 2;

    //handle system opCode
    public static final int OC_CREATE_HANDLE = 100;
    public static final int OC_DELETE_HANDLE = 101;
    public static final int OC_ADD_VALUE = 102;
    public static final int OC_REMOVE_VALUE = 103;
    public static final int OC_MODIFY_VALUE = 104;
    public static final int OC_LIST_HANDLE = 105;
    public static final int OC_LIST_NA = 106;

    //irs opCode
    public static final int OC_RESOLUTION_DOID = 11;
    public static final int OC_CREATE_DOID = 110;
    public static final int OC_DELETE_DOID = 111;
    public static final int OC_UPDATE_DOID = 117;
    public static final int OC_BATCH_CREATE_DOID = 120;

    public static final int OC_GET_USERS = 118;
    public static final int OC_GET_REPOS = 119;

    public static final int OC_VERIFY_IRS = 211;
    public static final int OC_RESOLVE_GRS = 212;
    //同步节点信息到GRS
    public static final int OC_CREATE_ORG_GRS = 213;
    public static final int OC_UPDATE_ORG_GRS = 214;
    public static final int OC_DELETE_ORG_GRS = 215;
    //同步节点信息到父节点
    public static final int OC_SYNC_ORG_IRS_PARENT = 216;
    public static final int OC_UPDATE_ORG_IRS_PARENT = 217;
    public static final int OC_DELETE_ORG_IRS_PARENT = 218;

    public static final int OC_CHALLENGE_RESPONSE = 200;
    public static final int OC_VERIFY_CHALLENGE = 201;

    public static final int OC_SESSION_SETUP = 400;
    public static final int OC_SESSION_TERMINATE = 401;
    public static final int OC_SESSION_EXCHANGEKEY = 402;

    //router opCode
    public static final int OC_VERIFY_ROUTER = 800;
    public static final int OC_RESOLVE_ROUTER = 801;

    //ResponseCode list(Message Header)
    public static final int RC_RESERVED = 0;
    public static final int RC_SUCCESS = 1;
    public static final int RC_ERROR = 2;
    public static final int RC_SERVER_BUSY = 3;
    public static final int RC_PROTOCOL_ERROR = 4;

    public static final int RC_OPERATION_DENIED = 5;
    public static final int RC_RECUR_LIMIT_EXCEEDED = 6;

    //handle system response code
    public static final int RC_HANDLE_NOT_FOUND = 100;
    public static final int RC_HANDLE_ALREADY_EXIST = 101;
    public static final int RC_INVALID_HANDLE = 102;

    //irs response code
    public static final int RC_DOID_NOT_FOUND = 110;
    public static final int RC_DOID_ALREADY_EXIST = 111;
    public static final int RC_INVALID_DOID = 112;

    public static final int RC_VALUE_NOT_FOUND = 200;
    public static final int RC_VALUE_ALREADY_EXIST = 201;
    public static final int RC_INVALID_VALUE = 202;

    public static final int RC_EXPIRED_SITE_INFO = 300;
    public static final int RC_SERVER_NOT_RESP = 301;
    public static final int RC_SERVICE_REFERRAL = 302;
    public static final int RC_NA_DELEGATE = 303;

    public static final int RC_NOT_AUTHORIZED = 400;
    public static final int RC_ACCESS_DENIED = 401;
    public static final int RC_AUTHEN_NEEDED = 402;
    public static final int RC_AUTHEN_FAILED = 403;
    public static final int RC_INVALID_CREDENTIAL = 404;
    public static final int RC_AUTHEN_TIMEOUT = 405;
    public static final int RC_UNABLE_TO_AUTHEN = 406;

    public static final int RC_SESSION_TIMEOUT = 500;
    public static final int RC_SESSION_FAILED = 501;
    public static final int RC_NO_SESSION_KEY = 502;
    public static final int RC_SESSION_NO_SUPPORT = 503;
    public static final int RC_SESSION_KEY_INVALID = 504;

    public static final int RC_TRYING = 900;
    public static final int RC_FORWARDED = 901;

    public static final int RC_QUEUED = 902;

    //
    public static final String getOperationCodeMessage(int opCode) {
        switch (opCode) {
            case OC_RESERVED:
                return "Reserved";
            case OC_RESOLUTION:
                return "resolution query";
            case OC_GET_SITEINFO:
                return "Get SITE_INFO values";
            case OC_CREATE_HANDLE:
                return "Create new Handle";
            case OC_DELETE_HANDLE:
                return "Delete existing Handle";
            case OC_ADD_VALUE:
                return "Add Handle value(s)";
            case OC_REMOVE_VALUE:
                return "Remove Handle value(s)";
            case OC_MODIFY_VALUE:
                return "Modify Handle value(s)";
            case OC_LIST_HANDLE:
                return "List Handles";
            case OC_LIST_NA:
                return "List sub-naming authorities";
            case OC_CHALLENGE_RESPONSE:
                return "Response to challenge";
            case OC_VERIFY_CHALLENGE:
                return "Verify challenge response";
            case OC_SESSION_SETUP:
                return "Session setup request";
            case OC_SESSION_TERMINATE:
                return "Session termination request";
            case OC_SESSION_EXCHANGEKEY:
                return "Session key exchange";
            case OC_CREATE_DOID:
                return "Create new DOID";
            case OC_DELETE_DOID:
                return "Delete existing DOID";
            case OC_UPDATE_DOID:
                return "Update existing DOID";
            default:
                return "Unexpected Errors";
        }
    }

    public static final String getResponseCodeInfo(int responseCode) {
        switch (responseCode) {
            case RC_RESERVED:
                return "Reserved for request";
            case RC_SUCCESS:
                return "Success response";
            case RC_ERROR:
                return "General error";
            case RC_SERVER_BUSY:
                return "Server too busy to respond";
            case RC_PROTOCOL_ERROR:
                return "Corrupted or unrecognizable message";
            case RC_OPERATION_DENIED:
                return "Unsupported operation";
            case RC_RECUR_LIMIT_EXCEEDED:
                return "Too many recursions for the request";
            case RC_HANDLE_NOT_FOUND:
                return "Handle not found";
            case RC_HANDLE_ALREADY_EXIST:
                return "Handle already exists";
            case RC_INVALID_HANDLE:
                return "Encoding (or syntax) error";
            case RC_VALUE_NOT_FOUND:
                return "Value not found";
            case RC_VALUE_ALREADY_EXIST:
                return "Value already exists";
            case RC_INVALID_VALUE:
                return "Invalid Handle value";
            case RC_EXPIRED_SITE_INFO:
                return "SITE_INFO out of date";
            case RC_SERVER_NOT_RESP:
                return "Server not responsible";
            case RC_SERVICE_REFERRAL:
                return "Server referral";
            case RC_NA_DELEGATE:
                return "Naming authority delegation takes place";
            case RC_NOT_AUTHORIZED:
                return "Not authorized/permitted";
            case RC_ACCESS_DENIED:
                return "No access to data";
            case RC_AUTHEN_NEEDED:
                return "Authentication required";
            case RC_AUTHEN_FAILED:
                return "Failed to authenticate";
            case RC_INVALID_CREDENTIAL:
                return "Invalid credential";
            case RC_AUTHEN_TIMEOUT:
                return "Authentication timed out";
            case RC_UNABLE_TO_AUTHEN:
                return "Unable to authenticate";
            case RC_SESSION_TIMEOUT:
                return "Session expired";
            case RC_SESSION_FAILED:
                return "Unable to establish session";
            case RC_NO_SESSION_KEY:
                return "No session yet available";
            case RC_SESSION_NO_SUPPORT:
                return "Session not supported";
            case RC_SESSION_KEY_INVALID:
                return "Invalid session key";
            case RC_TRYING:
                return "Request under processing";
            case RC_FORWARDED:
                return "Request forwarded to another server";
            case RC_QUEUED:
                return "Request queued for later processing";
            case RC_DOID_NOT_FOUND:
                return "DOID not found";
            case RC_DOID_ALREADY_EXIST:
                return "DOID already exists";
            case RC_INVALID_DOID:
                return "Encoding (or syntax) error";
            default:
                return "Unexpected Errors";
        }
    }
}
