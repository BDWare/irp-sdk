package org.bdware.irp.irplib.exception;

public class IrpMessageCredentialException extends Exception{

    public IrpMessageCredentialException(){
        super();
    }
    public IrpMessageCredentialException(String message){
        super(message);
    }

    @Override
    public String toString() {
        String msg = getMessage();
        if (msg == null) msg = "";
        return "IrpMessageCredentialException: " + msg;
    }

}
