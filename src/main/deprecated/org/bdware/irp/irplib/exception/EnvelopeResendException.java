package org.bdware.irp.irplib.exception;

public class EnvelopeResendException extends Exception{
    public EnvelopeResendException(String e){
        super(e);
    }
}
