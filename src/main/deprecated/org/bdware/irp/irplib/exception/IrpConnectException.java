package org.bdware.irp.irplib.exception;

public class IrpConnectException extends Exception{
    public IrpConnectException(String s){
        super(s);
    }
}
