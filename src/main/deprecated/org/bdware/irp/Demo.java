package org.bdware.irp;

import org.bdware.irp.client.IrpClient;
import org.bdware.irp.client.IrpClientImpl;
import org.bdware.irp.irplib.util.GlobalUtils;
import org.bdware.irp.stateinfo.DoStateInfo;
import org.bdware.irp.stateinfo.StateInfoBase;
import com.nimbusds.jose.jwk.JWK;

public class Demo {


    public static void main(String args[]) throws Exception{
        String url = "tcp://121.89.195.14:2641";
        JWK jwk = GlobalUtils.loadKeysFromJWKFile("./keys/jwk/dou.TEST.jwk");
        IrpClientImpl client = new IrpClientImpl(jwk, "clientTest", url);
        client.connect(url);
        StateInfoBase result = client.resolve("aibd/do.d53e4391-a8d3-443d-a8a8-275e90928b4c");
        System.out.println(result.getHandleValues());
        client.close();
        System.out.println("close the client!");
/*        Thread.sleep(5000);
        ThreadGroup currentGroup = Thread.currentThread().getThreadGroup();
        int noThreads = currentGroup.activeCount();
        Thread[] lstThreads = new Thread[noThreads];
        currentGroup.enumerate(lstThreads);

        for (int i = 0; i < noThreads; i++){
            System.out.println("Thread No:" + i + " = " + lstThreads[i].getName());
        }

        Thread.sleep(3000);
        lstThreads[0].interrupt();

        ThreadGroup currentGroup2 = Thread.currentThread().getThreadGroup();
        int noThreads2 = currentGroup2.activeCount();
        Thread[] lstThreads2 = new Thread[noThreads2];
        currentGroup2.enumerate(lstThreads2);

        for (int i = 0; i < noThreads; i++){
            System.out.println("Thread No:" + i + " = " + lstThreads2[i].getName());
        }*/
    }

}


