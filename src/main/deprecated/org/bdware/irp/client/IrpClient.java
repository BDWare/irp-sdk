package org.bdware.irp.client;

import org.bdware.irp.exception.IrpClientException;
import org.bdware.irp.stateinfo.StateInfoBase;

import java.util.List;

public interface IrpClient {

    StateInfoBase resolve(String handle) throws IrpClientException;

    String register(StateInfoBase hr) throws IrpClientException;

    String reRegister(StateInfoBase hr) throws IrpClientException;

    String unRegister(String handle);

    List<String> batchRegister(StateInfoBase hr, int count) throws IrpClientException;

}
