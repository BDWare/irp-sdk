package org.bdware.irp.client;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nimbusds.jose.jwk.JWK;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.bdware.irp.exception.IrpClientException;
import org.bdware.irp.stateinfo.StateInfoBase;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InternalIrpClient implements IrpClient{

    JWK clientKeyPair;
    String LHSUrl;
    static Logger logger = Logger.getLogger(InternalIrpClient.class);
    String ClientID;

    public InternalIrpClient(JWK kp, String clientID, String LHSUrl){
        this.clientKeyPair = kp;
        this.ClientID = clientID;
        this.LHSUrl = LHSUrl;
    }

    @Override
    public StateInfoBase resolve(String handle) throws IrpClientException {
        String response = resolveFromLHS(handle);
        JsonObject resp = new Gson().fromJson(response,JsonObject.class);
        StateInfoBase hr = new StateInfoBase();
        if(resp == null ||!resp.has("identifier")
                || (resp.has("status"))){
            throw new IrpClientException(response);
        }
        hr.identifier = resp.get("identifier").getAsString();
        if(resp.has("handleValues"))
            hr.handleValues = resp.getAsJsonObject("handleValues");
        else
            hr.handleValues = resp;
        return hr;
    }

    @Override
    public String register(StateInfoBase hr) throws IrpClientException {
        Map<String, String> map = hrToMap(hr);
        map.remove("identifier");
        map.put("action","register");
        String response = signAndSend(map);
        JsonObject resp = new Gson().fromJson(response,JsonObject.class);
        if(resp == null || !resp.has("identifier")){
            throw new IrpClientException(response);
        }
        return resp.get("identifier").getAsString();
    }

    @Override
    public List<String> batchRegister(StateInfoBase hr, int count) throws IrpClientException {
        Map<String, String> map = hrToMap(hr);
        map.remove("identifier");
        map.put("action","register");
        map.put("count",String.valueOf(count));
        String response = signAndSend(map);
        JsonObject resp = new Gson().fromJson(response,JsonObject.class);
        List<String> idList = new Gson().fromJson(resp.get("identifier").getAsString(),new TypeToken<List<String>>(){}.getType());
        if(idList == null){
            throw new IrpClientException(response);
        }
        return idList;
    }

    @Override
    public String reRegister(StateInfoBase hr) throws IrpClientException {
        if(hr.identifier == null){
            logger.warn("handle need to be set.");
        }
        Map<String, String> map = hrToMap(hr);
        map.put("identifier",hr.identifier);
        map.put("action","reregister");
        String response = signAndSend(map);
        JsonObject resp = new Gson().fromJson(response,JsonObject.class);
        if(resp == null ||!resp.has("identifier")){
            throw new IrpClientException(response);
        }
        return resp.get("identifier").getAsString();
    }

    @Override
    public String unRegister(String handle) {
        Map<String, String> map = new HashMap<>();
        map.put("identifier",handle);
        map.put("action","unregister");
        return signAndSend(map);
    }



    private String signAndSend(Map<String, String> map) {
//        map.put("dou",this.ClientID);
//        String reqMapStr = MapToString(map);
//        if(clientKeyPair != null){
//            try {
//                byte[] signature = CertUtils.Sign(reqMapStr.getBytes(), clientKeyPair.getPrivate());
//                map.put("signature",new String(signature));
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }
        return sendPost(map, LHSUrl + getSubUrl());
    }

    public String sendPost(Map<String, String> reqMap, String url){
        String result;
        HttpPost post = new HttpPost(url);
        try{
            CloseableHttpClient httpClient = HttpClients.createDefault();
            List<BasicNameValuePair> params = new ArrayList<>();
            for(String keys:reqMap.keySet()){
                params.add(new BasicNameValuePair(keys,reqMap.get(keys)));
            }
            UrlEncodedFormEntity postForm = new UrlEncodedFormEntity(params,"utf-8");
            post.setEntity(postForm);
            HttpResponse response = httpClient.execute(post);

            InputStream in = response.getEntity().getContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(in, "utf-8"));
            StringBuilder strber= new StringBuilder();
            String line = null;
            while((line = br.readLine())!=null){
                strber.append(line+'\n');
            }
            br.close();
            in.close();
            result = strber.toString();
            if(response.getStatusLine().getStatusCode()!= HttpStatus.SC_OK){
                result = "服务器异常";
            }
        } catch (Exception e){
            System.out.println("请求异常");
            throw new RuntimeException(e);
        } finally{
            post.abort();
        }
        return result;
    }

    public String resolveFromLHS(String id){
        String result;
        String resolveUrl = LHSUrl + "resolve?identifier=" + id;
        HttpGet httpGet = new HttpGet(resolveUrl);
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;
        InputStream in = null;
        BufferedReader br = null;
        try{
            httpClient = HttpClients.createDefault();
            response = httpClient.execute(httpGet);
            in = response.getEntity().getContent();
            br = new BufferedReader(new InputStreamReader(in, "utf-8"));
            StringBuilder strber= new StringBuilder();
            String line = null;
            while((line = br.readLine())!=null){
                strber.append(line+'\n');
            }
            result = strber.toString();
            if(response.getStatusLine().getStatusCode()!= HttpStatus.SC_OK){
                result = "服务器异常";
            }
        } catch (Exception e){
            System.out.println("请求异常");
            throw new RuntimeException(e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (response != null) {
                try {
                    response.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (httpClient != null) {
                try {
                    httpClient.close();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    private HashMap<String,String> hrToMap(StateInfoBase handleRecord){
        HashMap<String,String> hrMap = new HashMap<>();
        for(String keys:handleRecord.handleValues.keySet()){
            hrMap.put(keys,handleRecord.handleValues.get(keys).getAsString());
        }
        String className = handleRecord.getClass().getSimpleName();
        switch (className){
            case "DoStateInfo":
                hrMap.put("hrType","do");
                break;
            case "DoipServiceStateInfo":
                hrMap.put("hrType", "org/bdware/doip");
                break;
            case "UserStateInfo":
                hrMap.put("hrType","dou");
                break;
            default:
                logger.error("use specific handle record class instead.");
                break;
        }
        return hrMap;
    }

    private String getSubUrl(){
        return "local/";
    }

}
