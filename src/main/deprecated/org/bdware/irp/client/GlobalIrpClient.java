package org.bdware.irp.client;

import com.nimbusds.jose.jwk.JWK;
import org.apache.log4j.Logger;
import org.bdware.irp.exception.IrpClientException;

import java.security.KeyPair;

public class GlobalIrpClient {
    static Logger logger = Logger.getLogger(GlobalIrpClient.class);
    /**
     * Global IrpClient
     */
    public static IrpClient globalIrpClient;

    public static IrpClient getGlobalClient() throws IrpClientException {
        if(globalIrpClient == null){
            logger.error("globalIrpClient not initialized yet! ");
            throw new IrpClientException("globalIrpClient not initialized yet!");
        }
        return globalIrpClient;
    }

    public static void useTcpIrpClient(JWK kp, String clientID, String LHSUrl){
        globalIrpClient = new IrpClientImpl(kp, clientID, LHSUrl);
    }

    public static void useCdiIrpClient(JWK kp, String clientID, String LHSUrl){
        globalIrpClient = new CdiIrpClient(kp, clientID, LHSUrl);
    }

    public static void useGeneralIrpClient(JWK kp, String clientID, String LHSUrl){
        globalIrpClient = new GeneralIrpClient(kp, clientID, LHSUrl);
    }

    public static void useInternalIrpClient(JWK kp, String clientID, String LHSUrl){
        globalIrpClient = new InternalIrpClient(kp, clientID, LHSUrl);
    }


}
