package org.bdware.irp.client;


import com.nimbusds.jose.jwk.JWK;
import org.bdware.irp.exception.IrpClientException;
import org.bdware.irp.stateinfo.StateInfoBase;

import java.security.KeyPair;
import java.util.List;

public class GeneralIrpClient implements IrpClient{

    CdiIrpClient cdiIrpClient;
    InternalIrpClient internalIrpClient;

    public GeneralIrpClient(JWK kp, String clientID, String LHSUrl){
        cdiIrpClient = new CdiIrpClient(kp,clientID,LHSUrl);
        internalIrpClient = new InternalIrpClient(kp,clientID,LHSUrl);
    }

    @Override
    public StateInfoBase resolve(String handle) throws IrpClientException {
        return internalIrpClient.resolve(handle);
    }

    @Override
    public String register(StateInfoBase hr) throws IrpClientException {
        if(hr.identifier !=null && !hr.identifier.contains("_bdw")) return cdiIrpClient.register(hr);
        return internalIrpClient.register(hr);
    }

    @Override
    public String reRegister(StateInfoBase hr) throws IrpClientException {
        if(hr.identifier !=null && !hr.identifier.contains("_bdw")) return cdiIrpClient.reRegister(hr);
        return internalIrpClient.reRegister(hr);
    }

    @Override
    public String unRegister(String handle) {
        if(handle!=null && !handle.contains("_bdw")) return cdiIrpClient.unRegister(handle);
        return internalIrpClient.unRegister(handle);
    }

    @Override
    public List<String> batchRegister(StateInfoBase hr, int count) throws IrpClientException {
        if(hr.identifier !=null && !hr.identifier.contains("_bdw")) return cdiIrpClient.batchRegister(hr,count);
        return internalIrpClient.batchRegister(hr,count);
    }
}
