package org.bdware.irp.irpclient;

import org.apache.log4j.Logger;

import java.net.URI;
import java.util.HashMap;

public class IrpClientChannelGenerator {
    static Logger logger = Logger.getLogger(IrpClientChannelGenerator.class);
    static HashMap<String, IrpClientChannel> adhocClient = new HashMap<>();

    public static IrpClientChannel createIrpClientChannel(String url) {
        try {
            URI uri = new URI(url);
            logger.debug("[URI Parse]scheme:" + uri.getScheme() + " host: " + uri.getHost() + "  port: " + uri.getPort());
            switch (uri.getScheme()) {
                case "tcp":
                case "TCP":
                case "udp":
                case "UDP":
                    return NettyIrpClientChannel.createByServerUrl(url);
                default:
                    if(adhocClient.get(uri.getScheme())!= null){
                        return adhocClient.get(uri.getScheme());
                    }
                    logger.error("[Create server error] Unsupported URL: " + url);
                    return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void addClient(String schema, IrpClientChannel channel){
        adhocClient.put(schema,channel);
    }
}
