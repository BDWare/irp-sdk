package org.bdware.irp.irpclient;

import org.bdware.irp.irplib.core.IrpMessage;

import java.net.URISyntaxException;

public interface IrpClientChannel {
    void sendMessage(IrpMessage request, IrpMessageCallback callback);
    void close();
    void connect(String url) throws URISyntaxException;
    boolean isConnected();
}
