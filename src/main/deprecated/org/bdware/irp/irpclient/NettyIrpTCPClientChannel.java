package org.bdware.irp.irpclient;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import org.apache.log4j.Logger;
import org.bdware.irp.irplib.crypto.NettyTCPCodeC;
import org.bdware.irp.irplib.util.IrpCommon;

import java.net.URI;
import java.net.URISyntaxException;

public class NettyIrpTCPClientChannel extends NettyIrpClientChannel {
    static Logger logger = Logger.getLogger(NettyIrpTCPClientChannel.class);
    final Bootstrap b = new Bootstrap();
    static EventLoopGroup group = new NioEventLoopGroup();

    public NettyIrpTCPClientChannel() {
        b.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000);
        b.group(group);
        handler = new NettyIrpClientHandler();
        b.channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(
                        new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel(SocketChannel ch) {
                                ChannelPipeline p = ch.pipeline();
                                p.addLast(new LengthFieldBasedFrameDecoder(IrpCommon.MAX_MESSAGE_PACKET_LENGTH, 16, 4, 0, 0))
                                        .addLast(new NettyTCPCodeC())
                                        .addLast(handler);
                            }
                        });
    }

    @Override
    public void close() {
        if (handler != null) handler.close();
        isConnected = false;
    }

    @Override
    public void connect(String targetUrl) throws URISyntaxException {
        URI uri = new URI(targetUrl);
        logger.debug("[URI Parse]scheme:" + uri.getScheme() + "  host: " + uri.getHost() + "  port: " + uri.getPort());
        try {
            channel = b.connect(uri.getHost(), uri.getPort()).sync().channel();
            handler.setChannel(channel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        isConnected = true;
    }

}
