package org.bdware.irp.irpclient;

import org.bdware.irp.irplib.core.IrpMessage;

public interface IrpMessageCallback {
    void onResult(IrpMessage msg);
}
