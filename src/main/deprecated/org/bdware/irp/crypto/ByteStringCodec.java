package org.bdware.irp.crypto;

public interface ByteStringCodec {
    String encodeToString(byte[] bytes);
    byte[] decodeFromString(String str);
}
