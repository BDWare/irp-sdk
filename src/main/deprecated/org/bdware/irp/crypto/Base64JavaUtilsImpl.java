package org.bdware.irp.crypto;

import java.util.Base64;

public class Base64JavaUtilsImpl implements ByteStringCodec {

    static final Base64.Encoder encoder = Base64.getEncoder();
    static final Base64.Decoder decoder = Base64.getDecoder();

    @Override
    public String encodeToString(byte[] bytes) {
        return encoder.encodeToString(bytes);
    }

    @Override
    public byte[] decodeFromString(String str) {
        return decoder.decode(str);
    }

}
