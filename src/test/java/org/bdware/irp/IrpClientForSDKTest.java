package org.bdware.irp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp.client.IrpClientImpl;
import org.bdware.irp.exception.IrpClientException;
import org.bdware.irp.stateinfo.DoStateInfo;
import org.bdware.irp.stateinfo.StateInfoBase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IrpClientForSDKTest {

    Logger logger = LogManager.getLogger(IrpClientForSDKTest.class);
    public IrpClientImpl client;
    boolean waitForResp = true;

    @Before
    public void getIrpClient() throws Exception {
        try {
            logger.info("load the config finish");
            client = new IrpClientImpl("clientTest", "tcp://127.0.0.1:16161", null);
            client.setAddress("tcp://127.0.0.1:16161");
            client.connect("tcp://127.0.0.1:16161");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @After
    public void waitForResp() throws InterruptedException {
        while (waitForResp) {
            Thread.sleep(1000);
        }
        client.close();
    }

    @Test
    public void testCreate() {
        StateInfoBase digitalObject = createStateInfo();
        try {
            String result = client.register(digitalObject);
            logger.info("----------------" + result);
        } catch (IrpClientException e) {
            e.printStackTrace();
        }
        waitForResp = false;
    }

    @Test
    public void unRegister() throws IrpClientException {
        long start = System.currentTimeMillis();
        String ret = client.unRegister("doidForCreate");
        logger.info("takes:" + (System.currentTimeMillis() - start) + " " + (ret));
    }

    StateInfoBase createStateInfo() {
        DoStateInfo doStateInfo = new DoStateInfo("admin_test", "for test");
        return doStateInfo;
    }

    @Test
    public void resolveDO() {
//        client.retrieve("20.5000.123.TEST/service",null,true,pcb);
//        client.retrieve("86.5000.470/do.hello",null,false,pcb);
        try {
            StateInfoBase stateInfo = client.resolve("bdwaretest.loccall/fffe1bb2-76de-4244-a8a1-55d6017e45f1");
            if (stateInfo == null) {
                logger.info("Resolve failed!");
                waitForResp = false;
                return;
            }
            logger.info(stateInfo.getIdentifier());
            logger.info(stateInfo.getHandleValues());
        } catch (IrpClientException e) {
            e.printStackTrace();
        }
        waitForResp = false;
    }
}
