package org.bdware.irp;

import com.google.gson.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp.irplib.core.IrpMessage;
import org.bdware.irp.irplib.core.IrpRequest;
import org.bdware.irp.irplib.core.IrpResponse;
import org.bdware.irp.irplib.util.EncoderUtils;
import org.bdware.irp.irpserver.IrpHandler;
import org.bdware.irp.irpserver.IrpServerInfo;
import org.bdware.irp.stateinfo.StateInfoBase;

import java.util.UUID;

//TODO we do not support such handler
public class TestIrsHandler implements IrpHandler {
    Logger logger = LogManager.getLogger(IrpClientTest.class);
    IrpServerInfo serverInfo;

    public TestIrsHandler(IrpServerInfo info) {
        serverInfo = info;
    }

    @Override
    public IrpMessage resolveDOID(IrpMessage request) {
        logger.info(request);
        IrpRequest req = (IrpRequest) request;
        String doi = req.getDoid();
        JsonObject arg = new JsonObject();
        arg.addProperty("abc", "def");
        return IrpResponse.newIrsResolveResponse("hellloo", arg);
    }


    @Override
    public IrpMessage batchCreateDOID(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage createDOID(IrpMessage request) {
        IrpRequest req = (IrpRequest) request;
        StateInfoBase stateInfoBase = request2StateInfo("newID/" + UUID.randomUUID().toString(), req.getDoidValues());
        IrpResponse resp = IrpResponse.newIrsCreateDoidResponse(stateInfoBase.identifier);
        resp.responseMessage = EncoderUtils.encodeString("success");
        resp.result = "success";
        return resp;

    }


    @Override
    public IrpMessage updateDOID(IrpMessage request) {
        return null;
    }


    @Override
    public IrpMessage deleteDOID(IrpMessage request) {
        return null;
    }

    public static StateInfoBase request2StateInfo(String identifier, JsonObject doidValues) {
        StateInfoBase stateInfoBase = new StateInfoBase();
        stateInfoBase.setIdentifier(identifier);
        stateInfoBase.setHandleValues(doidValues);
        return stateInfoBase;
    }

    @Override
    public IrpMessage verifyIrs(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage resolvePrefix(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage createPrefix(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage updatePrefix(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage deletePrefix(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage verifyChildRouter(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage queryIdentifierByOffset(IrpMessage request) {
        return null;
    }
}
