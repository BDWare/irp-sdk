package org.bdware.irp;

import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp.stateinfo.DoStateInfo;
import org.bdware.irp.stateinfo.StateInfoBase;
import org.junit.Test;

public class StateInfoTest {
    Logger logger = LogManager.getLogger(IrpClientTest.class);
    @Test
    public void testSerialization(){
        DoStateInfo doHR = new DoStateInfo("111","222");
        doHR.identifier = "111";

        logger.debug(new Gson().toJson(doHR));
        logger.debug(new Gson().toJson(getHrBase(doHR)));
    }
    public StateInfoBase getHrBase(StateInfoBase base){
        return base;
    }

    @Test
    public void testTypeCast(){
        StateInfoBase hr = new StateInfoBase();
        hr.identifier = "111";
        hr.handleValues.addProperty("owner","111");
        hr.handleValues.addProperty("repository","222");
        DoStateInfo doHr = new DoStateInfo(hr);

        logger.debug("repository: " + doHr.getRepository());

    }
}
