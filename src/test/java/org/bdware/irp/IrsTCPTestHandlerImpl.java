package org.bdware.irp;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.bdware.irp.irplib.core.IrpMessage;
import org.bdware.irp.irplib.core.IrpRequest;
import org.bdware.irp.irplib.core.IrpResponse;
import org.bdware.irp.irpserver.IrpHandler;
import org.bdware.irp.irpserver.IrpServerInfo;
import org.bdware.irp.stateinfo.DoStateInfo;
import org.bdware.irp.stateinfo.StateInfoBase;

import java.util.Map;

public class IrsTCPTestHandlerImpl implements IrpHandler {
    IrpServerInfo serverInfo;

    public IrsTCPTestHandlerImpl(IrpServerInfo info) {
        serverInfo = info;
    }

    @Override
    public IrpMessage resolveDOID(IrpMessage request) {
        IrpRequest req = (IrpRequest) request;
        StateInfoBase stateInfoBase = new DoStateInfo("test", "test");
        String doi = req.getDoid();
        IrpMessage res;
        res = IrpResponse.newIrsResolveResponse(doi, stateInfoBase.getHandleValues());
        return res;
    }

    @Override
    public IrpMessage createDOID(IrpMessage request) {
        IrpRequest req = (IrpRequest) request;
        StateInfoBase stateInfoBase = request2StateInfo(req.getDoid(), req.getDoidValues());
        String doType = req.getDoidValues().get("hrType").getAsString();
        if (doType.equals("org/bdware/doip"))
            doType = "doip";
        IrpMessage res = IrpResponse.newIrsCreateDoidResponse("test/test.test");
        return res;
    }

    @Override
    public IrpMessage batchCreateDOID(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage updateDOID(IrpMessage request) {
        IrpMessage res = IrpResponse.newIrsUpdateDoidResponse("test");
        return res;
    }

    @Override
    public IrpMessage deleteDOID(IrpMessage request) {
        IrpRequest req = (IrpRequest) request;
        IrpMessage res = IrpResponse.newIrsDeleteDoidResponse(req.getDoid());
        return res;
    }

    public static StateInfoBase request2StateInfo(String identifier, JsonObject doidValues) {
        StateInfoBase stateInfoBase = new StateInfoBase();
        stateInfoBase.setIdentifier(identifier);
        stateInfoBase.setHandleValues(doidValues);
        return stateInfoBase;
    }

    @Override
    public IrpMessage verifyIrs(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage resolvePrefix(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage createPrefix(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage updatePrefix(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage deletePrefix(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage verifyChildRouter(IrpMessage request) {
        return null;
    }

    @Override
    public IrpMessage queryIdentifierByOffset(IrpMessage request) {
        return null;
    }
}
