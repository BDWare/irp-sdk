package org.bdware.irp;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp.client.IrpClientImpl;
import org.bdware.irp.exception.IrpClientException;
import org.bdware.irp.stateinfo.DoStateInfo;
import org.bdware.irp.stateinfo.StateInfoBase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileReader;
import java.util.List;

public class IrpClientTest {

    Logger logger = LogManager.getLogger(IrpClientTest.class);
    public IrpClientImpl client;
    boolean waitForResp = true;
    IrpServerTest.Config config;

    @Before
    public void getIrpClient() throws Exception {
        try {
            config = new Gson().fromJson(new FileReader("./input/irptest.json"), IrpServerTest.Config.class);
            logger.info("load the config finish");
            client = new IrpClientImpl("clientTest", config.irsAddress, null);
            client.setAddress(config.irsAddress);
            client.connect(config.irsAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @After
    public void waitForResp() throws InterruptedException {
        while (waitForResp) {
            Thread.sleep(1000);
        }
        client.close();
    }

    @Test
    public void testCreate() {
        StateInfoBase digitalObject = createStateInfo();
        try {
            String result = client.register(digitalObject);
            logger.info("----------------" + result);
        } catch (IrpClientException e) {
            e.printStackTrace();
        }
        waitForResp = false;
    }

    @Test
    public void unRegister() throws IrpClientException {
        long start = System.currentTimeMillis();
        String ret = client.unRegister("doidForCreate");
        logger.info("takes:" + (System.currentTimeMillis() - start) + " " + (ret));
    }

    StateInfoBase createStateInfo() {
        DoStateInfo doStateInfo = new DoStateInfo("admin_test", "for test");
        return doStateInfo;
    }


    @Test
    public void batchResolve() {
        long start = System.currentTimeMillis();
        try {
            for (int i = 0; i < config.batchResolveSize; i++) {
                StateInfoBase stateInfo = client.resolve("bdw.r1");
                if (stateInfo == null) {
                    logger.info("Resolve failed!");
                } else {
                    logger.info(stateInfo.getIdentifier());
                    logger.info(stateInfo.getHandleValues());
                }
            }
        } catch (IrpClientException e) {
            e.printStackTrace();
        }
        logger.info("takes:" + (System.currentTimeMillis() - start));
        waitForResp = false;
    }

    @Test
    public void batchRegister() {
        long start = System.currentTimeMillis();
        try {
            StateInfoBase base = new StateInfoBase();
            base.handleValues = new JsonObject();
            base.handleValues.add("repoId", JsonParser.parseString("bdtest5.r1/aibdtestrepo"));
            base.handleValues.addProperty("selfDefinedProp", System.currentTimeMillis());
            List<String> ret = client.batchRegister(base, 118);
            logger.info("takes:" + (System.currentTimeMillis() - start) + new Gson().toJson(ret));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void resolveRoutes() {
//        client.retrieve("20.5000.123.TEST/service",null,true,pcb);
//        client.retrieve("86.5000.470/do.hello",null,false,pcb);
        try {
            StateInfoBase stateInfo = client.resolve("bdwaretest.loccall");
            if (stateInfo == null) {
                logger.info("Resolve failed!");
                waitForResp = false;
                return;
            }
            logger.info(stateInfo.getIdentifier());
            logger.info(stateInfo.getHandleValues());
        } catch (IrpClientException e) {
            e.printStackTrace();
        }
        waitForResp = false;
    }

    @Test
    public void resolveRepos() {
//        client.retrieve("20.5000.123.TEST/service",null,true,pcb);
//        client.retrieve("86.5000.470/do.hello",null,false,pcb);
        try {
            StateInfoBase stateInfo = client.resolve("bdwaretest.loccall/Repo1");
            if (stateInfo == null) {
                logger.info("Resolve failed!");
                waitForResp = false;
                return;
            }
            logger.info(stateInfo.getIdentifier());
            logger.info(stateInfo.getHandleValues());
        } catch (IrpClientException e) {
            e.printStackTrace();
        }
        waitForResp = false;
    }

    @Test
    public void resolveDO() {
//        client.retrieve("20.5000.123.TEST/service",null,true,pcb);
//        client.retrieve("86.5000.470/do.hello",null,false,pcb);
        try {
            StateInfoBase stateInfo = client.resolve("bdwaretest.loccall/fffe1bb2-76de-4244-a8a1-55d6017e45f1");
            if (stateInfo == null) {
                logger.info("Resolve failed!");
                waitForResp = false;
                return;
            }
            logger.info(stateInfo.getIdentifier());
            logger.info(stateInfo.getHandleValues());
        } catch (IrpClientException e) {
            e.printStackTrace();
        }
        waitForResp = false;
    }
}
