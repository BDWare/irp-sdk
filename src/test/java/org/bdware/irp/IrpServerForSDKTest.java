package org.bdware.irp;

import org.bdware.irp.irpserver.*;


import java.util.ArrayList;
import java.util.List;

public class IrpServerForSDKTest {

    static IrpHandler irsHandler;

    public static void main(String[] args) {

        IrpListenerInfo listenerInfo = new IrpListenerInfo(16161, "TCP");
        List<IrpListenerInfo> listenerInfos = new ArrayList<IrpListenerInfo>();
        listenerInfos.add(listenerInfo);
        IrpServerInfo info = new IrpServerInfo("test", "irs server", listenerInfos);
        irsHandler = new IrsTCPTestHandlerImpl(info);
        IrpServer server = IrpServerImpl.createIrpServer(info);
        server.setIrpServerHandler(irsHandler);
        server.start();
    }
}



