package org.bdware.irp;

import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp.irpserver.IrpListenerInfo;
import org.bdware.irp.irpserver.IrpServer;
import org.bdware.irp.irpserver.IrpServerImpl;
import org.bdware.irp.irpserver.IrpServerInfo;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class IrpServerTest {

    Logger logger = LogManager.getLogger(IrpClientTest.class);

    public static class Config {
        String irsAddress;
        int batchResolveSize;
        String prefix;

    }

    public static void main(String[] args) throws FileNotFoundException, URISyntaxException {
        Config config = new Gson().fromJson(new FileReader("./input/irptest.json"), Config.class);
        URI uri = new URI(config.irsAddress);
        IrpListenerInfo listenerInfo = new IrpListenerInfo(uri.getHost(), uri.getPort(), "TCP");
        List<IrpListenerInfo> listenerInfos = new ArrayList<IrpListenerInfo>();
        listenerInfos.add(listenerInfo);
        IrpServerInfo info = new IrpServerInfo(config.prefix, "没有描述", listenerInfos);
        IrpServer server = IrpServerImpl.createIrpServer(info);
        server.setIrpServerHandler(new TestIrsHandler(info));
        server.start();

    }
}
