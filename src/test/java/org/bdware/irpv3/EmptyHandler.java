package org.bdware.irpv3;

import io.netty.channel.ChannelHandlerContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp3.ResponseCode;
import org.bdware.irp3.body.*;
import org.bdware.irp3.codec.MessageBody;
import org.bdware.irp3.codec.RequestDigest;
import org.bdware.irp3.codec.predefined.HS_Site;
import org.bdware.irp3.handler.*;

public class EmptyHandler implements QueryHandler, AdministrationHandler, ClientAuthenticationHandler, GetSiteInfoHandler, HomeManagementHandler, PrefixListHandler, SessionHandler {
    static Logger LOGGER = LogManager.getLogger(EmptyHandler.class);

    @Override
    public QueryResponse query(ChannelHandlerContext ctx, QueryRequest queryRequest) {
        LOGGER.info("query id: " + queryRequest.getIdentifier());
        QueryResponse queryResponse = new QueryResponse();
        queryResponse.setResponseCode(ResponseCode.RC_SUCCESS);
        queryResponse.setIdentifier(queryRequest.getIdentifier());
        return queryResponse;
    }

    @Override
    public MessageBody addElements(ChannelHandlerContext ctx, AddElementRequest queryRequest) {
        GeneralMessage ret = new GeneralMessage();
        ret.setResponseCode(ResponseCode.RC_INVALID_ADMIN);
        return ret;
    }

    @Override
    public MessageBody removeElements(ChannelHandlerContext ctx, RemoveElementRequest queryRequest) {
        GeneralMessage ret = new GeneralMessage();
        ret.setResponseCode(ResponseCode.RC_INVALID_ADMIN);
        return ret;
    }

    @Override
    public MessageBody modifyElements(ChannelHandlerContext ctx, ModifyElementRequest queryRequest) {
        GeneralMessage ret = new GeneralMessage();
        ret.setResponseCode(ResponseCode.RC_INVALID_ADMIN);
        return ret;
    }

    @Override
    public CreateIdentifierResponse createIdentifier(ChannelHandlerContext ctx, CreateIdentifierRequest queryRequest) {
        CreateIdentifierResponse ret = new CreateIdentifierResponse();
        ret.setIdentifier("abc");
        ret.setRequestDigest(new RequestDigest((byte) 1, new byte[16]));
        return ret;
    }

    @Override
    public MessageBody deleteIdentifier(ChannelHandlerContext ctx, DeleteIdentifierRequest queryRequest) {
        GeneralMessage ret = new GeneralMessage();
        ret.setResponseCode(ResponseCode.RC_INVALID_ADMIN);
        return ret;
    }

    @Override
    public ChallengeResponseResponse clientAuth(ChannelHandlerContext ctx, ChallengeResponseRequest request) {
        RequestDigest digest = new RequestDigest((byte) 1, new byte[16]);
        ChallengeResponseResponse ret = new ChallengeResponseResponse(digest, (byte) 1);
        if (request.getAuthenticationType().equals("sm2") && request.getChallengeResponse()[1] == 1) {
            ret.setResponseCode(ResponseCode.RC_SUCCESS);
        } else
            ret.setResponseCode(ResponseCode.RC_AUTHEN_FAILED);
        return ret;
    }

    @Override
    public void onServerVerification(ChannelHandlerContext ctx, VerificationResponse queryRequest) {
        //just ignore
        return;
    }

    @Override
    public VerificationResponse verifyRequest(ChannelHandlerContext ctx, VerificationRequest request) {
        RequestDigest digest = new RequestDigest((byte) 1, new byte[16]);
        int nonce = 1;
        VerificationResponse resp = new VerificationResponse(digest, nonce);
        if (request.getChallengeResponse()[1] == 1)
            resp.setResponseCode(ResponseCode.RC_SUCCESS);
        else
            resp.setResponseCode(ResponseCode.RC_AUTHEN_FAILED);
        return resp;
    }

    @Override
    public GetSiteInfoResponse getSiteInfo(ChannelHandlerContext ctx, GetSiteInfoRequest request) {
        HS_Site siteInfo = new HS_Site();
        siteInfo.setVersion((short) 456);
        GetSiteInfoResponse response = new GetSiteInfoResponse(siteInfo);
        if (request.getIgnoredIdentifier().equals("abc"))
            siteInfo.setVersion((short) 123);
        response.setResponseCode(ResponseCode.RC_SUCCESS);
        return response;
    }

    @Override
    public MessageBody homePrefix(ChannelHandlerContext ctx, HomePrefixRequest queryRequest) {
        GeneralMessage ret = new GeneralMessage();
        ret.setResponseCode(ResponseCode.RC_ACCESS_DENIED);
        return ret;
    }

    @Override
    public MessageBody unhomePrefix(ChannelHandlerContext ctx, UnhomePrefixRequest queryRequest) {
        GeneralMessage ret = new GeneralMessage();
        ret.setResponseCode(ResponseCode.RC_INVALID_ADMIN);
        return ret;
    }

    @Override
    public ListHomedPrefixResponse listHomePrefix(ChannelHandlerContext ctx, ListHomedPrefixRequest queryRequest) {
        ListHomedPrefixResponse response = new ListHomedPrefixResponse(new String[]{"abc"});
        return response;
    }

    @Override
    public ListIdentifiersResponse listIdentifier(ChannelHandlerContext ctx, ListIdentifiersRequest queryRequest) {
        ListIdentifiersResponse response = new ListIdentifiersResponse(new String[]{"abc"});
        return response;
    }

    @Override
    public ListIdentifiersResponse listDerivedPrefixes(ChannelHandlerContext ctx, ListDerivedPrefixsRequest queryRequest) {
        ListIdentifiersResponse response = new ListIdentifiersResponse(new String[]{"def"});
        return response;
    }

    @Override
    public SessionSetupResponse sessionSetup(ChannelHandlerContext ctx, SessionSetupRequest queryRequest) {
        RequestDigest digest = new RequestDigest((byte) 1, new byte[16]);
        SessionSetupResponse response = new SessionSetupResponse(digest, (short) 1, new byte[]{1, 2, 3});
        return response;
    }

    @Override
    public void sessionTermination(ChannelHandlerContext ctx, SessionTerminationRequest queryRequest) {
        ctx.close();
    }
}
