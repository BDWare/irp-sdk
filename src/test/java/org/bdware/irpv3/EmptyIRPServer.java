package org.bdware.irpv3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp.irpserver.IrpListenerInfo;
import org.bdware.irp.irpserver.IrpServerInfo;
import org.bdware.irp3.server.IRPServer;

import java.util.ArrayList;
import java.util.List;

public class EmptyIRPServer {
    public static int port = 21047;
    static Logger LOGGER = LogManager.getLogger(EmptyIRPServer.class);

    public static void main(String[] args) {
        String serverId = "bdtest";
        String serverDes = "测试";
        List<IrpListenerInfo> listenerInfos = new ArrayList<>();
        listenerInfos.add(new IrpListenerInfo("127.0.0.1", getPortByProtocol("tcp"), "tcp"));
        listenerInfos.add(new IrpListenerInfo("127.0.0.1", getPortByProtocol("udp"), "udp"));
        // listenerInfos.add(new IrpListenerInfo("127.0.0.1", getPortByProtocol("tls"), "tls"));
        listenerInfos.add(new IrpListenerInfo("127.0.0.1", getPortByProtocol("ws"), "ws"));
        IrpServerInfo info = new IrpServerInfo(serverId, serverDes, listenerInfos);
        IRPServer server = new IRPServer(info);
        server.addHandler(new EmptyHandler());
        server.start();
    }

    public static int getPortByProtocol(String protocol) {
        switch (protocol) {
            case "tcp":
            case "udp":
                return port;
            case "tls":
                return port + 1;
            case "ws":
                return port + 2;
            default:
                return -1;
        }
    }
}
