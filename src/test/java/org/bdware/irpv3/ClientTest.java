package org.bdware.irpv3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.irp3.ResponseCode;
import org.bdware.irp3.body.*;
import org.bdware.irp3.client.ClientConfig;
import org.bdware.irp3.client.IrpClient;
import org.bdware.irp3.client.IrpError;
import org.bdware.irp3.codec.Element;
import org.bdware.irp3.codec.RequestDigest;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.nio.charset.StandardCharsets;

public class ClientTest {
    static Logger LOGGER = LogManager.getLogger(ClientTest.class);
    private IrpClient client;
    static String protocol = "tcp";

    @Before
    public void init() throws Exception {
        ClientConfig config = new ClientConfig(new URI(protocol + "://127.0.0.1:" + EmptyIRPServer.getPortByProtocol(protocol) + "/WS"));
        client = new IrpClient(config);
    }

    @Test
    public void testQuery() throws Exception {
        QueryResponse response = client.query("abc", new int[]{1}, new String[]{"."});
        assert response.getIdentifier().equals("abc");
        LOGGER.info(response.getIdentifier());
    }

    @Test
    public void testAdministrationHandler() throws Exception {
        Element e = new Element();
        e.setIndex(1);
        e.setValue("abc".getBytes(StandardCharsets.UTF_8));
        Element[] eles = new Element[]{e};
        GeneralMessage generalResp = client.addElement("abc", eles);
        assert generalResp.getResponseCode() == ResponseCode.RC_INVALID_ADMIN;
        LOGGER.info("addElement:" + generalResp.getResponseCode());
        generalResp = client.removeElements("abc", new int[]{1});
        assert generalResp.getResponseCode() == ResponseCode.RC_INVALID_ADMIN;
        LOGGER.info("removeElements:" + generalResp.getResponseCode());
        generalResp = client.modifyElements("abc", eles);
        assert generalResp.getResponseCode() == ResponseCode.RC_INVALID_ADMIN;
        LOGGER.info("modifiyElements:" + generalResp.getResponseCode());
        CreateIdentifierResponse createResp = client.createIdentifier("abc", eles);
        assert createResp.getIdentifier().equals("abc");
        LOGGER.info("createIdentifier:" + createResp.getIdentifier());
        generalResp = client.deleteIdentifier("abc");
        assert generalResp.getResponseCode() == ResponseCode.RC_INVALID_ADMIN;
        LOGGER.info("generalResp:" + generalResp.getResponseCode());
    }

    @Test
    public void testClientAuthentication() throws Exception {
        String authenticationType = "sm2";
        String keyIdentifier = "abc.key";
        int keyIndex = 1;
        byte[] challengeResponse = new byte[]{0, 1, 2, 3, 4};
        ChallengeResponseResponse result = client.clientAuth(new ChallengeResponseRequest(authenticationType, keyIdentifier, keyIndex, challengeResponse));
        assert result.getResponseCode() == ResponseCode.RC_SUCCESS;
        challengeResponse = new byte[]{1, 2, 3, 4};
        result = client.clientAuth(new ChallengeResponseRequest(authenticationType, keyIdentifier, keyIndex, challengeResponse));
        assert result.getResponseCode() == ResponseCode.RC_AUTHEN_FAILED;
        int nonce = 1;
        RequestDigest requestDigest = new RequestDigest((byte) 1, new byte[16]);

        challengeResponse = new byte[]{0, 1, 2, 3, 4};
        VerificationRequest request = new VerificationRequest(keyIdentifier, keyIndex, nonce, requestDigest, challengeResponse);
        VerificationResponse response = client.verifyRequest(request);
        assert response.getResponseCode() == ResponseCode.RC_SUCCESS;
        challengeResponse = new byte[]{1, 2, 3, 4};
        request = new VerificationRequest(keyIdentifier, keyIndex, nonce, requestDigest, challengeResponse);
        response = client.verifyRequest(request);
        assert response.getResponseCode() == ResponseCode.RC_AUTHEN_FAILED;
    }

    @Test
    public void testGetSiteInfo() throws IrpError {
        GetSiteInfoResponse result = client.getSiteInfo("abc");
        assert result.getResponseCode() == ResponseCode.RC_SUCCESS;
        assert result.getSiteInfo().getVersion() == 123;
        result = client.getSiteInfo("abcdef");
        assert result.getResponseCode() == ResponseCode.RC_SUCCESS;
        assert result.getSiteInfo().getVersion() == 456;
    }

    @Test
    public void testHomeManagement() throws IrpError {
        GeneralMessage result = client.homePrefix("abc");
        assert result.getResponseCode() == ResponseCode.RC_ACCESS_DENIED;
        result = client.unhomePrefix("abc");
        assert result.getResponseCode() == ResponseCode.RC_INVALID_ADMIN;
        ListHomedPrefixResponse response = client.listHomePrefix("abc");
        assert response.getIdentifiers()[0].equals("abc");
    }

    @Test
    public void testPrefixAdmin() throws IrpError {
        ListIdentifiersResponse result = client.listIdentifier("abc");
        assert result.getIdentifiers()[0].equals("abc");
        ListIdentifiersResponse result2 = client.listDerivedPrefixes("abc");
        assert result2.getIdentifiers()[0].equals("def");
    }

    @Test
    public void testSession() throws IrpError {
        SessionSetupRequest request = new SessionSetupRequest();
        SessionSetupResponse response = client.setupSession(request);
        assert response.getMode() == 1;
        client.terminateSession();
    }
}
