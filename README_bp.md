# irp-sdk
本sdk 主要包括IRP协议的编解码器。并实现了递归解析的IRP客户端。

# 示例1：Irp协议客户端(IrpClientImpl)
参考测试用例：`src/test/java/org/bdware/irp/IrpClientTest.java`
相关java类：`IrpClientImpl`
需增加一个配置文件 `input/irptest.json`

## 构造函数

只有1个构造函数。参考：`IrpClientTest.java`中的`getIrpClient`测试用例。

### 参数
第一个参数为`String`类型，表示client的id。

第二个参数，为`String`类型，表示server的url。

第三个参数，为`IrpMessageSigner`类型，表示client的身份信息。

## resolve
解析doId以获取相关配置信息。参考：`IrpClientTest.java`中的`resolve`测试用例。

### 参数
只有一个参数，类型为`String`，表示client的doId。

### 返回结果
类型为`StateInfoBase`类型，表示client的相关配置信息。

## register
注册一个标识。
参考：`IrpClientTest.java` 中的`testCreate`测试用例。

### 参数
通过JsonObject初始化StateInfoBase

```java
StateInfoBase info=new StateInfoBase();
info.handleValue=JsonParser.parse("{...}").getAsJsonObject();
LOGGER.info(client.register());
```

### 返回结果
格式为：prefix/repo/id
```
bdtest5.r1/aibdtestrepo/106d0d49-8831-46a9-bf19-a6a5e71a0ecb
```

## reRegister

更新标识。
参考：`IrpClientTest.java` 中的`testCreate`用例。

### 参数

只有一个`StateInfoBase`类型的参数。

### 返回结果
格式为：prefix/repo/id
```
bdtest5.r1/aibdtestrepo/106d0d49-8831-46a9-bf19-a6a5e71a0ecb
```


## unRegister

删除标识。
参考：`IrpClientTest.java` 中的`unRegister`测试用例。

## batchRegister

批量注册。
参考：`IrpClientTest.java` 中的`batchRegister`

### 参数

第一个参数为StateInfoBase，同register方法。 第二个参数为count，<127。

### 返回结果

```//List<String>
[
    "bdwaretest.loccall/Repo1/d5c08fc4-4fa5-4389-928d-728beefd0db6",
    "bdwaretest.loccall/Repo1/2e938a34-ec1f-45a6-bcfb-947616c89ae5",
    "bdwaretest.loccall/Repo1/ce424245-b572-4b6b-a22b-543b4e30914c",
    "bdwaretest.loccall/Repo1/f96e7e3c-d959-4d7a-bf11-5311f31a64ce",
    "bdwaretest.loccall/Repo1/ee3d6261-bda4-42cc-8123-6d430b67ee22"
]
```

## resolve

解析标识。
参考：`IrpClientTest.java` 中的`resolveRoutes`、`resolveRepos`、`resolveDO`三个测试用例。

### 参数

1个参数，表示doId。

### 返回结果

```
//解析DO的返回结果
{
    "repoId":"bdwaretest.loccall/Repo1",
    "selfDefinedProp":"1642926242001",
    "doId":"bdwaretest.loccall/Repo1/e321016c-560a-47d6-bded-cdd7a1fb1eef",
    "repoInfo":"{\"date\":\"2022-1-23\",\"name\":\"Repo1\",\"doId\":\"bdwaretest.loccall/Repo1\",\"address\":\"tcp://127.0.0.1:2054\",\"status\":\"已审核\",\"protocol\":\"DOIP\",\"pubKey\":\"04d1924329f72ced148f6f333fb985ccbaa31b1e3aacf10be5f43d4a4ff5ad88899a005e79e37fc06993e1d66ada8cf8b711cb36f59538bb7d3e39e70fa9360ddd\",\"version\":\"2.1\"}"
}
//解析Repo的返回结果
{
    "date":"2022-1-23",
    "protocol":"DOIP",
    "address":"tcp://127.0.0.1:2055",
    "name":"Repo1","doId":"bdwaretest.loccall/Repo1",
    "version":"2.1","status":"已审核",
    "pubKey":"04d1924329f72ced148f6f333fb985ccbaa31b1e3aacf10be5f43d4a4ff5ad88899a005e79e37fc06993e1d66ada8cf8b711cb36f59538bb7d3e39e70fa9360ddd"
}
//解析Router的返回结果
{
    "date":"2022-1-7",
    "protocol":"IRP",
    "address":"127.0.0.1",
    "port":"2641",
    "name":"测试根",
    "doId":"bdwaretest.loccall",
    "version":"2.1",
    "status":"已审核",
    "pubKey":"04d1924329f72ced148f6f333fb985ccbaa31b1e3aacf10be5f43d4a4ff5ad88899a005e79e37fc06993e1d66ada8cf8b711cb36f59538bb7d3e39e70fa9360ddd"
}
```

# 示例2：Irp协议服务端(IrpServerImpl)

## start
启动Irp服务器。
参考：`IrpServerTest.java`中的main方法。

## stop
关闭Irp服务器。

## setIrsServerHandler
设置Irs服务器处理器。
参考：`IrpServerTest.java`中的main方法。

### 参数
类型为`IrsHandler`的处理器。

## setGrsServerHandler
设置Grs服务器处理器。

### 参数
类型为`GrsHandler`的处理器。

## setRouterServerHandler
设置Router服务器处理器。

### 参数
类型为`RouterHandler`的处理器。
