## 一、项目背景

#### 1.DOA

![](https://gitee.com/BDWare/BDWare/raw/master/_static/imgs/DOAArchitecture.png)

数字对象架构（Digital Object Architecture, DOA）是现有Internet体系结构的一个逻辑扩展，它支持不同系统之间的数据互操作需求，而不仅仅是将数字形式的信息从Internet的一个位置传送到另一个位置。数字对象体系结构中的基本元素是数字对象（Digital Object），是对任意信息系统数据资源的统一抽象。数字对象分为四个部分：标识、元数据、状态数据以及实体。其中，标识永久且唯一的指向一个数字对象，是数字对象永久不变的属性；元数据是数字对象的描述信息，用于数字对象的检索和发现；状态数据包含数字对象当前位置、访问入口、访问方式等信息，用于数字对象的定位和访问；实体则是数字对象所包含的实际内容，可以是任何比特序列或一系列比特序列的集合。标识和状态数据的分离也使得数字对象的标识不在与数字对象的访问入口紧耦合，使得无论是否在互联网环境中，数字对象都可以被共享、访问，进而实现普适环境下的信息系统互操作。

DO是对互联网资源的抽象，DO代表互联网中有意义或者是有价值的资源。DO的表现形式可以是一个比特序列，或者是多个比特序列的集合；也可以是一个对外提供接口的程序。就像互联网中每一个Host都有一个IP地址作为标识一样，在DOA中每个DO同样有一个标识，用于对这个DO进行唯一的识别，这个标识叫做DOID。

DOA中有两个基本协议：标识解析协议IRP(Identifier Resolution Protocol)和数字对象接口协议DOIP(Digital Object Interface Protocol)。DOA中有三个基本的组成部分，分别是标识/解析系统(Identifier/Resolution System)负责DO的标识和解析、仓库系统(Repository System)负责DO的存储和访问、以及注册系统负责DO元数据的注册和DO的搜索(Registry System)。

#### 2.DO-IRP
DO-IRP是一个用户安全创建、更新、维护和访问数字对象标识以及标识记录的协议，该协议基于数字对象架构，被设计用于访问受保护的标识信息，以实现在诸如互联网的网络上进行标识互操作和资源共享。数字对象架构的标识解析构件实现最早是由美国国家研究创新机构（CNRI）在IETF RFCs 3650，3651和3652中明确的。这几个早期的RFC的重要组成部分是描述标识解析协议，即在RFC3652中介绍的Handle System Protocol（2.1版本）。2022年6月30日，DONA基金会发布了最新的DO-IRP 3.0协议，该版本协议吸收了Handle System Protocol来处理基本的问题，并能够大部分向后兼容，但是是一个独立的规范协议，不依赖于特定的实现。本项目参考了Handle System Protocol，目前正在进行基于DO-IRP 3.0协议的开发，后续将进行开源。
## 二、项目结构

![](https://gitee.com/BDWare/BDWare/raw/master/_static/imgs/IRPSDKModule.png)

本项目是IRP协议的软件开发套件SDK，可以基于本SDK实现IRP Client和IRP Server。
项目整体功能模块如图所示，主要包括核心层和端点层两部分。

#### 1.核心层
协议的编解码器：
负责对数字对象进行序列化编解码，以及对在通信协议中传输的IrpMessage进行编、解码操作。

#### 2.端点层
协议端点的开发框架录：
提供服务器和客户端的开发接口，可以方便的实现基于不同传输协议下的服务器和客户端软件。
当前SDK支持使用tcp等传输协议，具体实现可以参考main/java/org.bdware.irp.client.jwk.IrpJWKClientImpl, test/java/org.bdware.irp.IrpServerForSDKTest

## 三、使用前的准备
#### 1.gradle.properties
项目编译前，需要在根目录下新建一个配置文件，文件名为gradle.properties，复制以下内容到文件内：
```gradle
NEXUS_USERNAME=abc
NEXUS_PASSWORD=def
signing.keyId=ghi
signing.password=jkl
signing.secretKeyRingFile=mno
```
#### 2.生成JWK密钥
IRP Client和Server基于公私钥来进行身份验证和加密通讯，但并不限定公私钥的生成机制以及公钥的交换方式。
IRP SDK默认采用JWX作为公私钥标准，JWK序列化密钥、JWS签名、JWE加解密。

有关JWX的相关介绍可以参考
- JWK格式简介 https://blog.csdn.net/JosephThatwho/article/details/114876345
- JWK在线生成 https://mkjwk.org/
- jwt官网 https://jwt.io/
- jwt反解 http://jwt.calebb.net/

通常情况下，用户的公私钥信息会作为用户的标识记录保存在标识解析系统中，在启用签名验证或加密时需要先将用户信息注册至IRS:



## 四、基于SDK开发标识解析系统，提供IRP服务：

数字对象标识解析系统包含了对标识记录存储、管理等能力，需要结合具体的场景具体处理。
因此，IRP SDK本身并不包含一个完整实现的数字对象标识解析系统，但提供了相应的接口，开发者可以便捷的开发出符合自己需求的数字对象标识解析系统。

### 1.实现服务器Handler
Handler是服务器处理请求的具体实现，不同的服务器应用之间的主要区别就在于使用了不同的Handler实现方式。
Handler每个方法的输入为接受到的IRP请求消息，返回值为对应的IRP响应消息。

在接收到IRP消息时，会根据IrpMessage.IrpMessageHeader.opCode判断其操作码调用相应的操作。
项目中对该接口的实现参考test/org.bdware.irp.IrsTCPTestHandlerImpl，主要继承了IrsHandler接口，里面定义了标识注册、更新、删除等操作。

缺省情况下，标识注册与解析系统的Handler应该包含如下的基本操作，在SDK中表现为IrsHandler接口：
```java
public interface IrsHandler {

    IrpMessage doidResolve(IrpMessage request);

    IrpMessage createDOID(IrpMessage request);

    IrpMessage batchCreateDOID(IrpMessage request);

    IrpMessage updateDOID(IrpMessage request);

    IrpMessage deleteDOID(IrpMessage request);
}

```
如需实现路由等功能，则需要实现诸如GrsHandler等接口，进行标识前缀的管理


### 2.配置服务器信息
服务器配置信息IrpServerInfo主要包含以下参数：
```java
class IrpServerInfo{
    String serverId
    String serverDescription;
    List<IrpListenerInfo> listenerInfos;
}
```
其中ListenerInfo描述IRP服务入口，包含host，port，protocolType。其中host指明了服务地址、port指明了服务端口、protocolType指明了服务支持的协议。
```java
class IrpListenerInfo{
    public String host;
    int port
    public String protocolType;
}
```

### 3.生成服务器实例并启动
SDK为开发者提供了IrpServerImpl类作为开发服务器的模板类，开发者只需要使用IrpServerInfo类型的配置信息,配置相应的handler即可生成服务器：
```java
IrpListenerInfo listenerInfo = new IrpListenerInfo(16161, "TCP");
List<IrpListenerInfo> listenerInfos = new ArrayList<IrpListenerInfo>();
listenerInfos.add(listenerInfo);
IrpServerInfo info = new IrpServerInfo("test", "irs server", listenerInfos);
irsHandler = new IrsTCPTestHandlerImpl(info);
IrpServer server = IrpServerImpl.createIrpServer(info);
server.setIrsServerHandler(irsHandler);
server.start();
```

服务器会根据IrpListenerInfo，启动一个或多个监听，接受IRP请求，请求会交由相应的Handler方法处理，并返回响应消息。

## 五、基于IRP SDK开发IRP客户端：
IRP SDK提供了以及IrpClient接口以及缺省接口实现IrpClientImpl，访问目标标识服务的IRP服务，主要定义了客户端发送标识注册、解析、更新、删除的操作，已经链接相关接口。

```java
public interface IrpClient {

    StateInfoBase resolve(String handle) throws IrpClientException;

    String register(StateInfoBase hr) throws IrpClientException;

    String reRegister(StateInfoBase hr) throws IrpClientException;

    String unRegister(String handle) throws IrpClientException;

    List<String> batchRegister(StateInfoBase hr, int count) throws IrpClientException;

    void close();

    void connect(String url);

    void connect(String clientID, String LHSUrl, IrpMessageSigner signer);

    void reconnect() throws IrpConnectException;

    public boolean isConnected();

}
```

一个简单的访问示例入下所示,方法调用IrpClient.retrieve接口，向目标服务器发送了解析标识的请求（Resolve）：
```java
public static void resolve(String DOID) throws InterruptedException {
    String irsAddress = "tcp://172.22.163.35:2641";

    //生成秘钥信息
    String[] keyPair = CertUtils.keyGenerator(UUID.randomUUID().toString());
    String privateKey = keyPair[0];
    String publicKey = keyPair[1];
    JWK jwk = GlobalUtils.loadKeysFromJWKStr(privateKey);
    //初始化client
    IrpClientImpl client = new IrpClientImpl(jwk, "clientID", irsAddress);;
    client.connect(irsAddress);
    //发送标识解析请求
    StateInfoBase stateInfo = client.resolve("bdwaretest.test");
}
```

## 六、相关链接
- [Handle](http://www.handle.net/)
- [DO-IRP 3.0](https://www.dona.net/sites/default/files/2022-06/DO-IRPV3.0--2022-06-30.pdf)