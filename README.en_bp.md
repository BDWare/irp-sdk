# irp-sdk
This SDK mainly includes the codec of the IRP protocol and implements IRP client with recursive parsing.

# Example 1: Client with Irp protocol-IrpClientImpl
Refer to test case `src/test/java/org/bdware/irp/IrpClientTest.java`.
Related java classes contain `IrpClientImpl`.
You need to add configuration file `input/irptest.json` to run.

## Constructor
Only one constructor is provided.
Refer to test case `getIrpClient` in `IrpClientTest.java`.

### Parameter
There are three parameters.
The first one is the `String` type, representing the id of the client.
The second one is also the `String` type, representing the url of the server.
The last one is the `IrpMessageSigner` type, representing the identity of the client.

## resolve
Resolve doId to get related configuration information.
Refer to test case `resolve` in  `IrpClientTest.java`.

## Parameter
There is only one parameter whose type is `String`, representing the doId of the client.

## Return result
The result is of type `StateInfoBase`, and represents the related configuration information of the client.

## register
Register a identifier.
Refer to test case `testCreate` in `IrpClientTest.java`.

### Parameter
Initialize `StateInfoBase` via `JsonObject`.

```java
StateInfoBase info=new StateInfoBase();
info.handleValue=JsonParser.parse("{...}").getAsJsonObject();
LOGGER.info(client.register());
```

### Return result
The format is `prefix/repo/id`
```
bdtest5.r1/aibdtestrepo/106d0d49-8831-46a9-bf19-a6a5e71a0ecb
```

## reRegister

Update the identifier.
Refer to test case `testCreate` in `IrpClientTest.java`.

### Parameter
There is only one parameter whose type is `StateInfoBase`.

### Return result
The format is `prefix/repo/id`.
```
bdtest5.r1/aibdtestrepo/106d0d49-8831-46a9-bf19-a6a5e71a0ecb
```


## unRegister

Delete the identifier.
Refer to test case `unRegister` in `IrpClientTest.java`.

## batchRegister
For batch registration, please refer to test case `batchRegister` in `IrpClientTest.java`.

### Parameter

The first one is of type `StateInfoBase`, which acts as in the `registrer` method.
The second one is count，less than 127.

### Return result

```//List<String>
[
    "bdwaretest.loccall/Repo1/d5c08fc4-4fa5-4389-928d-728beefd0db6",
    "bdwaretest.loccall/Repo1/2e938a34-ec1f-45a6-bcfb-947616c89ae5",
    "bdwaretest.loccall/Repo1/ce424245-b572-4b6b-a22b-543b4e30914c",
    "bdwaretest.loccall/Repo1/f96e7e3c-d959-4d7a-bf11-5311f31a64ce",
    "bdwaretest.loccall/Repo1/ee3d6261-bda4-42cc-8123-6d430b67ee22"
]
```

## resolve

Resolve the identifier.
Refer to the three test cases of `resolveRoutes`, `resolveRepos`, and `resolveDO` in `IrpClientTest.java`.

### Parameter

There is only one parameter representing `doId`.

### Return result

```
//解析DO的返回结果
{
    "repoId":"bdwaretest.loccall/Repo1",
    "selfDefinedProp":"1642926242001",
    "doId":"bdwaretest.loccall/Repo1/e321016c-560a-47d6-bded-cdd7a1fb1eef",
    "repoInfo":"{\"date\":\"2022-1-23\",\"name\":\"Repo1\",\"doId\":\"bdwaretest.loccall/Repo1\",\"address\":\"tcp://127.0.0.1:2054\",\"status\":\"已审核\",\"protocol\":\"DOIP\",\"pubKey\":\"04d1924329f72ced148f6f333fb985ccbaa31b1e3aacf10be5f43d4a4ff5ad88899a005e79e37fc06993e1d66ada8cf8b711cb36f59538bb7d3e39e70fa9360ddd\",\"version\":\"2.1\"}"
}
//解析Repo的返回结果
{
    "date":"2022-1-23",
    "protocol":"DOIP",
    "address":"tcp://127.0.0.1:2055",
    "name":"Repo1","doId":"bdwaretest.loccall/Repo1",
    "version":"2.1","status":"已审核",
    "pubKey":"04d1924329f72ced148f6f333fb985ccbaa31b1e3aacf10be5f43d4a4ff5ad88899a005e79e37fc06993e1d66ada8cf8b711cb36f59538bb7d3e39e70fa9360ddd"
}
//解析Router的返回结果
{
    "date":"2022-1-7",
    "protocol":"IRP",
    "address":"127.0.0.1",
    "port":"2641",
    "name":"测试根",
    "doId":"bdwaretest.loccall",
    "version":"2.1",
    "status":"已审核",
    "pubKey":"04d1924329f72ced148f6f333fb985ccbaa31b1e3aacf10be5f43d4a4ff5ad88899a005e79e37fc06993e1d66ada8cf8b711cb36f59538bb7d3e39e70fa9360ddd"
}
```

# 示例2：Server with Irp protocol-IrpServerImpl

## start
Start the Irp server.
Refer to `main` method in `IrpServerTest.java`.

## stop
Close Irp server.

## setIrsServerHandler
Set the handler to handle oprations for Irs server.
Refer to `main` method in `IrpServerTest.java`.

### Parameter
There is only one handler whose type is `IrsHandler`.

## setGrsServerHandler
Set the handler to handle oprations for Grs server.

### Parameter
There is only one handler whose type is `GrsHandler`.

## setRouterServerHandler
Set the handler to handle oprations for Router server.
### Parameter
There is only one handler whose type is `RouterHandler`.